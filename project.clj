(defproject yahont "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/data.zip "0.1.1"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-jetty-adapter "1.3.2"]
                 [compojure "1.3.1"]
                 [hiccup "1.0.5"]
                 [com.taoensso/tower "3.0.2"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [postgresql/postgresql "8.4-702.jdbc4"]
                 [clojure.jdbc/clojure.jdbc-c3p0 "0.3.2"]
                 [inflections "0.9.13"]
                 [honeysql "0.4.3"]
                 [org.clojure/data.xml "0.0.8"]]
  :source-paths ["src" "src/main/clojure"]
  :target-path "target/%s/"
  :main yahont.core)
