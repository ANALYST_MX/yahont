(ns yahont.map.convention.name
  (:use inflections.core)
  (:require [clojure.string :as string]))

(defn keyword-entry [name]
  (singular (keyword (string/replace name "-" "_"))))

(defn keyword-entries [name]
  (plural (keyword (string/replace name "-" "_"))))
