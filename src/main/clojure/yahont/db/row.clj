(ns yahont.db.row
  (:require [yahont.map.convention.name :as naming]))

(defn select-columns [columnseq]
  (fn [row-map] (select-keys row-map columnseq)))
