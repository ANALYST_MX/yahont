(ns yahont.db.marshal
  (:require [yahont.map.convention.name :as naming]))

(defn entry->map [k entry]
  (assoc {} (naming/keyword-entry k) (first entry)))

(defn entries->map [k entries]
  (assoc {} (naming/keyword-entries k) (map #(entry->map k (list %)) entries)))

(defn ->map [k e]
  (cond
    (empty? e) (entries->map k e)
    (nil? e) (entry->map k e)
    (> (count e) 1) (entries->map k e)
    (= (count e) 1) (entry->map k e)))
