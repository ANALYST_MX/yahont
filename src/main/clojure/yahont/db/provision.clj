(ns yahont.db.provision)

(defmacro row-specs
  [r]
  `(into [~r] (:specs (meta (var ~r)))))
