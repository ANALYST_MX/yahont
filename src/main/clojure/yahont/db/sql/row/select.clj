(ns yahont.db.sql.row.select
  (:require [clojure.java.jdbc :as sql]
            [yahont.db :as db]))

(defn select-by-id [table-name id]
  (sql/query db/spec
             [(str "SELECT * FROM " table-name " WHERE id = ?") id]))

(defn select-all [table-name]
  (sql/query db/spec
             [(str "SELECT * FROM " table-name)]))

(defn select [sql-clause]
  (sql/query db/spec sql-clause))
