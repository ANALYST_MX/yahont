(ns yahont.db.sql.row.delete
  (:require [clojure.java.jdbc :as sql]
            [yahont.db :as db]))

(defn delete-by-id [table-name id]
  (sql/delete! db/spec (keyword table-name)
               ["id = ?" id]))

(defn delete [sql-clause]
  (sql/execute! db/spec sql-clause))
