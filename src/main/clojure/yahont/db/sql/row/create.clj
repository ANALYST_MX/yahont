(ns yahont.db.sql.row.create
  (:require [clojure.java.jdbc :as sql]
            [yahont.db :as db]))

(defn create [table-name row-map]
  (sql/insert! db/spec (keyword table-name)
               row-map))
