(ns yahont.db.sql.row.update
  (:require [clojure.java.jdbc :as sql]
            [yahont.db :as db]))

(defn update-by-id [table-name id set-map]
  (sql/update! db/spec (keyword table-name)
               set-map
               ["id = ?" id]))
