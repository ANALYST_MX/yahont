(ns yahont.db.sql.table.create
  (:require [clojure.java.jdbc :as sql]
            [yahont.db :as db]))

(defn created? [table]
  (-> (sql/query db/spec
                 [(str "select count(1) from information_schema.tables "
                       "where table_name='" (name table) "';")])
      first :count pos?))

(defn create [table-name & options]
  (when (not (created? table-name))
    (sql/db-do-commands db/spec
                        (apply sql/create-table-ddl
                               (keyword table-name)
                               options))))
