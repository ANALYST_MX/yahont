(ns yahont.db.convention.name
  (:use [clojure.string :refer [join]])
  (:require [inflections.core :refer [singular plural]]))

(def ^:const delimiter "_")

(defn table [& names]
  (plural (join delimiter names)))

(defn primary-key [table] :id)

(defn foreign-key
  ([table] (foreign-key table (primary-key table)))
  ([table field] (keyword (str (singular (name table)) delimiter (name field)))))

(defn full-name [& names]
  (join "." (map name names)))
