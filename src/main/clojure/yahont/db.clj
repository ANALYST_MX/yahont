(ns yahont.db
  (:require [clojure.java.jdbc :as sql]
            [jdbc.pool.c3p0 :as pool]
            [yahont.config :refer [database-adapter database-host database-port
                                   database-name database-user database-password]]))

(def database-conf
  {:db-adapter database-adapter
   :db-host database-host
   :db-port database-port
   :db-name database-name
   :db-user database-user
   :db-pass database-password})

(defn make-db [{:keys [db-adapter db-host db-port db-name db-user db-pass]}]
  {:subprotocol db-adapter
   :subname (str "//" db-host ":" db-port "/" db-name)
   :user db-user
   :password db-pass})

(defonce db (make-db database-conf))

(def spec (pool/make-datasource-spec (merge db {:initial-pool-size 3})))
