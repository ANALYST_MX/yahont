(ns yahont.routes.get
  (:require [yahont.modules.application.controllers.home]
            [yahont.modules.dictionaries.controllers.home]
            [yahont.modules.versions.controllers.home]
            [yahont.modules.subdivisions.controllers.home]
            [yahont.modules.dictionaries.register.controllers.home]
            [yahont.modules.dictionaries.common.controllers.home]
            [yahont.modules.dictionaries.funding.controllers.home]
            [yahont.modules.register.controllers.home]
            [yahont.modules.application.controllers.info]
            [yahont.modules.status_code.controllers.four_oh_four]
            [yahont.modules.register.controllers.register]
            [yahont.modules.register.controllers.register.show]
            [yahont.modules.register.controllers.register.edit]
            [yahont.modules.register.controllers.register.all]
            [yahont.modules.register.controllers.register.delete]
            [yahont.modules.register.controllers.register.upload]
            [yahont.modules.versions.register.controllers.register]
            [yahont.modules.versions.register.controllers.register.all]
            [yahont.modules.versions.register.controllers.register.create]
            [yahont.modules.versions.register.controllers.register.edit]
            [yahont.modules.versions.register.controllers.register.show]
            [yahont.modules.versions.register.controllers.register.delete]
            [yahont.modules.services.controllers.home]
            [yahont.modules.services.paid.account.controllers.contract]
            [yahont.modules.services.paid.account.controllers.contract.all]
            [yahont.modules.services.paid.account.controllers.contract.create]
            [yahont.modules.services.paid.account.controllers.contract.edit]
            [yahont.modules.services.paid.account.controllers.contract.show]
            [yahont.modules.services.paid.account.controllers.contract.find]
            [yahont.modules.services.paid.account.controllers.contract.delete]
            [yahont.modules.dictionaries.register.controllers.institution]
            [yahont.modules.dictionaries.register.controllers.institution.all]
            [yahont.modules.dictionaries.register.controllers.institution.create]
            [yahont.modules.dictionaries.register.controllers.institution.edit]
            [yahont.modules.dictionaries.register.controllers.institution.show]
            [yahont.modules.dictionaries.register.controllers.institution.delete]
            [yahont.modules.dictionaries.common.controllers.yes_no]
            [yahont.modules.dictionaries.common.controllers.yes_no.all]
            [yahont.modules.dictionaries.common.controllers.yes_no.create]
            [yahont.modules.dictionaries.common.controllers.yes_no.edit]
            [yahont.modules.dictionaries.common.controllers.yes_no.show]
            [yahont.modules.dictionaries.common.controllers.yes_no.delete]
            [yahont.modules.dictionaries.funding.controllers.source]
            [yahont.modules.dictionaries.funding.controllers.source.all]
            [yahont.modules.dictionaries.funding.controllers.source.create]
            [yahont.modules.dictionaries.funding.controllers.source.edit]
            [yahont.modules.dictionaries.funding.controllers.source.show]
            [yahont.modules.dictionaries.funding.controllers.source.delete]
            [yahont.modules.register.controllers.document]
            [yahont.modules.dictionaries.register.controllers.subdivision]
            [yahont.modules.dictionaries.register.controllers.subdivision.all]
            [yahont.modules.dictionaries.register.controllers.subdivision.create]
            [yahont.modules.dictionaries.register.controllers.subdivision.edit]
            [yahont.modules.dictionaries.register.controllers.subdivision.show]
            [yahont.modules.dictionaries.register.controllers.subdivision.delete]
            [yahont.modules.register.controllers.invoice]
            [yahont.modules.register.controllers.invoice.all]
            [yahont.modules.register.controllers.invoice.show]
            [yahont.modules.register.controllers.patient]
            [yahont.modules.register.controllers.event]
            [yahont.modules.register.controllers.event.find]
            [yahont.modules.register.controllers.event.edit]
            [yahont.modules.register.controllers.event.show]
            [yahont.modules.register.controllers.sanction]
            [yahont.modules.register.controllers.sanction.show]
            [yahont.modules.register.controllers.service]
            [yahont.modules.register.controllers.person]
            [yahont.modules.dictionaries.register.controllers.payer]
            [yahont.modules.dictionaries.register.controllers.payer.all]
            [yahont.modules.dictionaries.register.controllers.payer.create]
            [yahont.modules.dictionaries.register.controllers.payer.edit]
            [yahont.modules.dictionaries.register.controllers.payer.show]
            [yahont.modules.dictionaries.register.controllers.payer.delete]
            [yahont.modules.dictionaries.register.controllers.department]
            [yahont.modules.dictionaries.register.controllers.department.all]
            [yahont.modules.dictionaries.register.controllers.department.create]
            [yahont.modules.dictionaries.register.controllers.department.edit]
            [yahont.modules.dictionaries.register.controllers.department.show]
            [yahont.modules.dictionaries.register.controllers.department.delete]
            [yahont.modules.dictionaries.register.controllers.profile]
            [yahont.modules.dictionaries.register.controllers.profile.all]
            [yahont.modules.dictionaries.register.controllers.profile.create]
            [yahont.modules.dictionaries.register.controllers.profile.edit]
            [yahont.modules.dictionaries.register.controllers.profile.show]
            [yahont.modules.dictionaries.register.controllers.profile.delete]
            [yahont.modules.dictionaries.register.controllers.mode]
            [yahont.modules.dictionaries.register.controllers.mode.all]
            [yahont.modules.dictionaries.register.controllers.mode.create]
            [yahont.modules.dictionaries.register.controllers.mode.edit]
            [yahont.modules.dictionaries.register.controllers.mode.show]
            [yahont.modules.dictionaries.register.controllers.mode.delete]
            [yahont.modules.register.controllers.record]
            [yahont.modules.register.controllers.failure_cause]))

(defn home [params]
  (yahont.modules.application.controllers.home/index))

(defn dictionaries-home [params]
  (yahont.modules.dictionaries.controllers.home/index))

(defn dictionaries-register-home [params]
  (yahont.modules.dictionaries.register.controllers.home/index))

(defn dictionaries-common-home [params]
  (yahont.modules.dictionaries.common.controllers.home/index))

(defn dictionaries-funding-home [params]
  (yahont.modules.dictionaries.funding.controllers.home/index))

(defn versions-home [params]
  (yahont.modules.versions.controllers.home/index))

(defn versions-register-home [params]
  (yahont.modules.versions.register.controllers.register/index))

(defn register-home [params]
  (yahont.modules.register.controllers.home/index))

(defn subdivisions-home [params]
  (yahont.modules.subdivisions.controllers.home/index))

(defn info [params]
  (yahont.modules.application.controllers.info/index))

(defn four-oh-four [params]
  (yahont.modules.status_code.controllers.four_oh_four/index))

(defn register [params]
  (yahont.modules.register.controllers.register/index))

(defn registers [params]
  (yahont.modules.register.controllers.register.all/index))

(defn register-show [params]
  (yahont.modules.register.controllers.register.show/index params))

(defn register-edit [params]
  (yahont.modules.register.controllers.register.edit/index params))

(defn register-delete [params]
  (yahont.modules.register.controllers.register.delete/delete params))

(defn register-upload [params]
  (yahont.modules.register.controllers.register.upload/index))

(defn institution [params]
  (yahont.modules.dictionaries.register.controllers.institution/index))

(defn institutions [params]
  (yahont.modules.dictionaries.register.controllers.institution.all/index))

(defn institution-create [params]
  (yahont.modules.dictionaries.register.controllers.institution.create/index))

(defn institution-show [params]
  (yahont.modules.dictionaries.register.controllers.institution.show/index params))

(defn institution-edit [params]
  (yahont.modules.dictionaries.register.controllers.institution.edit/index params))

(defn institution-delete [params]
  (yahont.modules.dictionaries.register.controllers.institution.delete/delete params))

(defn yes-no [params]
  (yahont.modules.dictionaries.common.controllers.yes_no/index))

(defn yes-no-all [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.all/index))

(defn yes-no-create [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.create/index))

(defn yes-no-show [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.show/index params))

(defn yes-no-edit [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.edit/index params))

(defn yes-no-delete [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.delete/delete params))

(defn dictionaries-funding-source [params]
  (yahont.modules.dictionaries.funding.controllers.source/index))

(defn dictionaries-funding-sources [params]
  (yahont.modules.dictionaries.funding.controllers.source.all/index))

(defn dictionaries-funding-source-create [params]
  (yahont.modules.dictionaries.funding.controllers.source.create/index))

(defn dictionaries-funding-source-show [params]
  (yahont.modules.dictionaries.funding.controllers.source.show/index params))

(defn dictionaries-funding-source-edit [params]
  (yahont.modules.dictionaries.funding.controllers.source.edit/index params))

(defn dictionaries-funding-source-delete [params]
  (yahont.modules.dictionaries.funding.controllers.source.delete/delete params))

(defn document [params]
  (yahont.modules.register.controllers.document/index))

(defn subdivision [params]
  (yahont.modules.dictionaries.register.controllers.subdivision/index))

(defn subdivisions [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.all/index))

(defn subdivision-create [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.create/index))

(defn subdivision-show [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.show/index params))

(defn subdivision-edit [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.edit/index params))

(defn subdivision-delete [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.delete/delete params))

(defn invoice [params]
  (yahont.modules.register.controllers.invoice/index))

(defn invoices [params]
  (yahont.modules.register.controllers.invoice.all/index))

(defn invoice-show [params]
  (yahont.modules.register.controllers.invoice.show/index params))

(defn patient [params]
  (yahont.modules.register.controllers.patient/index))

(defn event [params]
  (yahont.modules.register.controllers.event/index))

(defn event-find [params]
  (yahont.modules.register.controllers.event.find/index))

(defn event-show [params]
  (yahont.modules.register.controllers.event.show/index params))

(defn event-edit [params]
  (yahont.modules.register.controllers.event.edit/index params))

(defn sanction [params]
  (yahont.modules.register.controllers.sanction/index))

(defn sanction-show [params]
  (yahont.modules.register.controllers.sanction.show/index params))

(defn service [params]
  (yahont.modules.register.controllers.service/index))

(defn person [params]
  (yahont.modules.register.controllers.person/index))

(defn payer [params]
  (yahont.modules.dictionaries.register.controllers.payer/index))

(defn payers [params]
  (yahont.modules.dictionaries.register.controllers.payer.all/index))

(defn payer-create [params]
  (yahont.modules.dictionaries.register.controllers.payer.create/index))

(defn payer-show [params]
  (yahont.modules.dictionaries.register.controllers.payer.show/index params))

(defn payer-edit [params]
  (yahont.modules.dictionaries.register.controllers.payer.edit/index params))

(defn payer-delete [params]
  (yahont.modules.dictionaries.register.controllers.payer.delete/delete params))

(defn department [params]
  (yahont.modules.dictionaries.register.controllers.department/index))

(defn departments [params]
  (yahont.modules.dictionaries.register.controllers.department.all/index))

(defn department-create [params]
  (yahont.modules.dictionaries.register.controllers.department.create/index))

(defn department-show [params]
  (yahont.modules.dictionaries.register.controllers.department.show/index params))

(defn department-edit [params]
  (yahont.modules.dictionaries.register.controllers.department.edit/index params))

(defn department-delete [params]
  (yahont.modules.dictionaries.register.controllers.department.delete/delete params))

(defn versions-register [params]
  (yahont.modules.versions.register.controllers.register/index))

(defn versions-registers [params]
  (yahont.modules.versions.register.controllers.register.all/index))

(defn versions-register-create [params]
  (yahont.modules.versions.register.controllers.register.create/index))

(defn versions-register-show [params]
  (yahont.modules.versions.register.controllers.register.show/index params))

(defn versions-register-edit [params]
  (yahont.modules.versions.register.controllers.register.edit/index params))

(defn versions-register-delete [params]
  (yahont.modules.versions.register.controllers.register.delete/delete params))

(defn profile [params]
  (yahont.modules.dictionaries.register.controllers.profile/index))

(defn profiles [params]
  (yahont.modules.dictionaries.register.controllers.profile.all/index))

(defn profile-create [params]
  (yahont.modules.dictionaries.register.controllers.profile.create/index))

(defn profile-show [params]
  (yahont.modules.dictionaries.register.controllers.profile.show/index params))

(defn profile-edit [params]
  (yahont.modules.dictionaries.register.controllers.profile.edit/index params))

(defn profile-delete [params]
  (yahont.modules.dictionaries.register.controllers.profile.delete/delete params))

(defn mode [params]
  (yahont.modules.dictionaries.register.controllers.mode/index))

(defn modes [params]
  (yahont.modules.dictionaries.register.controllers.mode.all/index))

(defn mode-create [params]
  (yahont.modules.dictionaries.register.controllers.mode.create/index))

(defn mode-show [params]
  (yahont.modules.dictionaries.register.controllers.mode.show/index params))

(defn mode-edit [params]
  (yahont.modules.dictionaries.register.controllers.mode.edit/index params))

(defn mode-delete [params]
  (yahont.modules.dictionaries.register.controllers.mode.delete/delete params))

(defn record [params]
  (yahont.modules.register.controllers.record/index))

(defn failure-cause [params]
  (yahont.modules.register.controllers.failure_cause/index))

(defn services-home [params]
  (yahont.modules.services.controllers.home/index))

(defn services-paid-account-contract [params]
  (yahont.modules.services.paid.account.controllers.contract/index))

(defn services-paid-account-contracts [params]
  (yahont.modules.services.paid.account.controllers.contract.all/index params))

(defn services-paid-account-contract-create [params]
  (yahont.modules.services.paid.account.controllers.contract.create/index))

(defn services-paid-account-contract-edit [params]
  (yahont.modules.services.paid.account.controllers.contract.edit/index params))

(defn services-paid-account-contract-show [params]
  (yahont.modules.services.paid.account.controllers.contract.show/index params))

(defn services-paid-account-contract-find [params]
  (yahont.modules.services.paid.account.controllers.contract.find/index))

(defn services-paid-account-contract-delete [params]
  (yahont.modules.services.paid.account.controllers.contract.delete/delete params))
