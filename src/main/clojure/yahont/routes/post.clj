(ns yahont.routes.post
  (:require [yahont.modules.register.controllers.document.migration]
            [yahont.modules.register.controllers.event.migration]
            [yahont.modules.register.controllers.event.find]
            [yahont.modules.register.controllers.event.update]
            [yahont.modules.dictionaries.register.controllers.institution.migration]
            [yahont.modules.dictionaries.register.controllers.institution.update]
            [yahont.modules.dictionaries.register.controllers.institution.create]
            [yahont.modules.dictionaries.common.controllers.yes_no.migration]
            [yahont.modules.dictionaries.common.controllers.yes_no.update]
            [yahont.modules.dictionaries.common.controllers.yes_no.create]
            [yahont.modules.dictionaries.funding.controllers.source.migration]
            [yahont.modules.dictionaries.funding.controllers.source.update]
            [yahont.modules.dictionaries.funding.controllers.source.create]
            [yahont.modules.versions.register.controllers.register.migration]
            [yahont.modules.versions.register.controllers.register.update]
            [yahont.modules.versions.register.controllers.register.create]
            [yahont.modules.services.paid.account.controllers.contract.migration]
            [yahont.modules.services.paid.account.controllers.contract.update]
            [yahont.modules.services.paid.account.controllers.contract.create]
            [yahont.modules.services.paid.account.controllers.contract.find]
            [yahont.modules.register.controllers.invoice.migration]
            [yahont.modules.register.controllers.patient.migration]
            [yahont.modules.dictionaries.register.controllers.payer.migration]
            [yahont.modules.dictionaries.register.controllers.payer.update]
            [yahont.modules.dictionaries.register.controllers.payer.create]
            [yahont.modules.register.controllers.person.migration]
            [yahont.modules.register.controllers.register.migration]
            [yahont.modules.register.controllers.register.update]
            [yahont.modules.register.controllers.register.upload]
            [yahont.modules.register.controllers.sanction.migration]
            [yahont.modules.register.controllers.service.migration]
            [yahont.modules.dictionaries.register.controllers.department.migration]
            [yahont.modules.dictionaries.register.controllers.department.update]
            [yahont.modules.dictionaries.register.controllers.department.create]
            [yahont.modules.dictionaries.register.controllers.profile.migration]
            [yahont.modules.dictionaries.register.controllers.profile.update]
            [yahont.modules.dictionaries.register.controllers.profile.create]
            [yahont.modules.dictionaries.register.controllers.mode.migration]
            [yahont.modules.dictionaries.register.controllers.mode.update]
            [yahont.modules.dictionaries.register.controllers.mode.create]
            [yahont.modules.dictionaries.register.controllers.subdivision.migration]
            [yahont.modules.dictionaries.register.controllers.subdivision.update]
            [yahont.modules.dictionaries.register.controllers.subdivision.create]
            [yahont.modules.register.controllers.record.migration]
            [yahont.modules.register.controllers.failure_cause.migration]))

(defn document-migration [params]
  (yahont.modules.register.controllers.document.migration/migration-table))

(defn event-migration [params]
  (yahont.modules.register.controllers.event.migration/migration-table))

(defn event-find [params]
  (yahont.modules.register.controllers.event.find/find params))

(defn event-update [id params]
  (yahont.modules.register.controllers.event.update/update id params))

(defn institution-migration [params]
  (yahont.modules.dictionaries.register.controllers.institution.migration/migration-table))

(defn institution-create [params]
  (yahont.modules.dictionaries.register.controllers.institution.create/create params))

(defn institution-update [id params]
  (yahont.modules.dictionaries.register.controllers.institution.update/update id params))

(defn versions-register-migration [params]
  (yahont.modules.versions.register.controllers.register.migration/migration-table))

(defn yes-no-migration [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.migration/migration-table))

(defn yes-no-create [params]
  (yahont.modules.dictionaries.common.controllers.yes_no.create/create params))

(defn dictionaries-funding-source-migration [params]
  (yahont.modules.dictionaries.funding.controllers.source.migration/migration-table))

(defn dictionaries-funding-source-create [params]
  (yahont.modules.dictionaries.funding.controllers.source.create/create params))

(defn dictionaries-funding-source-update [id params]
  (yahont.modules.dictionaries.funding.controllers.source.update/update id params))

(defn yes-no-update [id params]
  (yahont.modules.dictionaries.common.controllers.yes_no.update/update id params))

(defn versions-register-create [params]
  (yahont.modules.versions.register.controllers.register.create/create params))

(defn versions-register-update [id params]
  (yahont.modules.versions.register.controllers.register.update/update id params))

(defn invoice-migration [params]
  (yahont.modules.register.controllers.invoice.migration/migration-table))

(defn patient-migration [params]
  (yahont.modules.register.controllers.patient.migration/migration-table))

(defn payer-migration [params]
  (yahont.modules.dictionaries.register.controllers.payer.migration/migration-table))

(defn payer-create [params]
  (yahont.modules.dictionaries.register.controllers.payer.create/create params))

(defn payer-update [id params]
  (yahont.modules.dictionaries.register.controllers.payer.update/update id params))

(defn person-migration [params]
  (yahont.modules.register.controllers.person.migration/migration-table))

(defn register-migration [params]
  (yahont.modules.register.controllers.register.migration/migration-table))

(defn register-update [id params]
  (yahont.modules.register.controllers.register.update/update id params))

(defn register-upload [params]
  (yahont.modules.register.controllers.register.upload/upload-file params))

(defn sanction-migration [params]
  (yahont.modules.register.controllers.sanction.migration/migration-table))

(defn service-migration [params]
  (yahont.modules.register.controllers.service.migration/migration-table))

(defn subdivision-migration [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.migration/migration-table))

(defn subdivision-create [params]
  (yahont.modules.dictionaries.register.controllers.subdivision.create/create params))

(defn subdivision-update [id params]
  (yahont.modules.dictionaries.register.controllers.subdivision.update/update id params))

(defn department-migration [params]
  (yahont.modules.dictionaries.register.controllers.department.migration/migration-table))

(defn department-create [params]
  (yahont.modules.dictionaries.register.controllers.department.create/create params))

(defn department-update [id params]
  (yahont.modules.dictionaries.register.controllers.department.update/update id params))

(defn profile-migration [params]
  (yahont.modules.dictionaries.register.controllers.profile.migration/migration-table))

(defn profile-create [params]
  (yahont.modules.dictionaries.register.controllers.profile.create/create params))

(defn profile-update [id params]
  (yahont.modules.dictionaries.register.controllers.profile.update/update id params))

(defn mode-migration [params]
  (yahont.modules.dictionaries.register.controllers.mode.migration/migration-table))

(defn mode-create [params]
  (yahont.modules.dictionaries.register.controllers.mode.create/create params))

(defn mode-update [id params]
  (yahont.modules.dictionaries.register.controllers.mode.update/update id params))

(defn record-migration [params]
  (yahont.modules.register.controllers.record.migration/migration-table))

(defn failure-cause-migration [params]
  (yahont.modules.register.controllers.failure_cause.migration/migration-table))

(defn services-paid-account-contract-migration [params]
  (yahont.modules.services.paid.account.controllers.contract.migration/migration-table))

(defn services-paid-account-contract-update [id params]
  (yahont.modules.services.paid.account.controllers.contract.update/update id params))

(defn services-paid-account-contract-create [params]
  (yahont.modules.services.paid.account.controllers.contract.create/create params))

(defn services-paid-account-contract-find [params]
  (yahont.modules.services.paid.account.controllers.contract.find/find params))
