(ns yahont.syntax.binding)

(defmacro let-try [bindings & body]
  (when-not (even? (count bindings))
    (throw (IllegalArgumentException. "try-let requires an even number of forms in binding vector")))
  `(let ~bindings
     (try
       ~@body)))
