(ns yahont.syntax.map)

(defn merge-in [m ks & maps]
  (apply update-in m ks merge maps))
