(ns yahont.syntax.html.tag
  (:require [yahont.syntax.conditional :refer [in?]]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn add-option-tooltip [options title]
  (if (in? (map #(:selected (second %)) options) true)
    (conj options (list [:option {:value nil :disabled true} title]))
    (conj options (list [:option {:value nil :selected true :disabled true} title]))))
