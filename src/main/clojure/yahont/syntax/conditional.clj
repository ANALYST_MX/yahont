(ns yahont.syntax.conditional)

(defmacro if-let*
  ([bindings then]
     `(if-let* ~bindings ~then nil))
  ([bindings then else]
     (if (empty? bindings)
       then
       `(if-let ~(vec (take 2 bindings))
          (if-let* ~(drop 2 bindings) ~then ~else)
          ~else))))

(defmacro no-nil-assoc
  ([coll key val]
   `(if (contains? ~coll ~key)
      ~coll
      (assoc ~coll ~key ~val))))

(defmacro if-not-empty
  ([coll then] `(if-not-empty ~coll ~then nil))
  ([coll then else]
   `(if-not (empty? ~coll)
      ~then
      ~else)))

(def not-nil?
  (complement nil?))

(defmacro if-not-nil
  ([coll then] `(if-not-nil ~coll ~then nil))
  ([coll then else]
   `(if (not-nil? ~coll)
      ~then
      ~else)))

(defn in? [seq elt]
  (cond
    (empty? seq) false
    (= (first seq) elt) true
    true (in? (rest seq) elt)))
