(ns yahont.config)

;; ----- Postgres config -----

(defonce database-adapter "postgresql")
(defonce database-host "localhost")
(defonce database-port "5432")
(defonce database-name "yahont")
(defonce database-password nil)
(defonce database-user "yahont")

;; ----- Web server config -----

(defonce web-server-port 3000)
(defonce hot-reload-web-server false)
(defonce asset-dir "public")
(defonce locale :ru)

;; ----- XML Schema Validator -----
(defn register-xml-schema-validator
  ([] (register-xml-schema-validator 0))
  ([^Integer v] (str "resources/xml/validation/register/version/" v "/OMS-D1.xsd")))
