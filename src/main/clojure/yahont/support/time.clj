(ns yahont.support.time
  (:import
   [java.util Date]
   [java.sql.Timestamp]))

(defn today []
  (java.util.Date.))

(defn now []
  (java.sql.Timestamp. (.getTime (today))))
