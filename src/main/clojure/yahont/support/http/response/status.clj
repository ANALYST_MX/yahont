(ns yahont.support.http.response.status)

(def ^:const not-found {:status 404})
(def ^:const success {:status 200})
(def ^:const server-error {:status 500})
