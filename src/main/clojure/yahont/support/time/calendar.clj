(ns yahont.support.time.calendar
  (:import [java.util Calendar GregorianCalendar]))

(defn divisible? [num div] (zero? (rem num div)))

(defn leap-year? [year]
  (or (divisible? year 400)
      (and (divisible? year 100)
           (not (divisible? year 4)))))

(defn month-durations [year]
  [31 (if leap-year? 29 28) 31 30 31 30 31 31 30 31 30 31])

(defn last-day-of-month [year month]
  (->(doto
         (GregorianCalendar.)
       (.set Calendar/DATE 1)
       (.set Calendar/MONTH month)
       (.set Calendar/YEAR year)
       (.add Calendar/DATE -1))))

(defn first-day-of-month [year month]
  (->(doto
         (GregorianCalendar.)
       (.set Calendar/DATE 1)
       (.set Calendar/MONTH (dec month))
       (.set Calendar/YEAR year))))
