(ns yahont.support.time.format
  (:import
   [java.text SimpleDateFormat]
   [java.util Date TimeZone Calendar Locale]))

(defn calendar [date]
  (doto (Calendar/getInstance)
    (.setTime date)))

(defn formatter
  ([^String fmts]
     (SimpleDateFormat. fmts Locale/ROOT))
  ([^String fmts ^TimeZone zone]
     (.setTimeZone (SimpleDateFormat. fmts Locale/ROOT) zone)))

(defn parse
  [^SimpleDateFormat fmt ^String s]
  (.parse fmt s))

(defn unparse [^SimpleDateFormat fmt date]
  (cond
    (instance? Date date)
    (.format fmt date)
    (instance? Calendar date)
    (recur fmt (.getTime date))))

(def ^:dynamic *default-formats*
  ["EEE MMM dd HH:mm:ss ZZZ yyyy"
   "yyyy-MM-dd HH:mm:ss ZZZ"
   "yyyy-MM-dd HH:mm:ss"
   "yyyy-MM-dd HH:mm"
   "yyyy-MM-dd"])

(defprotocol ToFormatter
  (->formatter [fmt]))

(extend-protocol ToFormatter
  java.lang.String
  (->formatter [fmt] (formatter fmt))
  clojure.lang.Keyword
  (->formatter [fmt] (formatter fmt)))

(defn parse-or-nil
  [fmt date-str]
  (try
    (parse (->formatter fmt) date-str)
    (catch Exception e nil)))

(defn normalize-datetime
  [date-str]
  (first (remove nil?
                 (map #(parse-or-nil % date-str)
                      *default-formats*))))

(defprotocol DateTimeProtocol
  (year [instant])
  (month [instant])
  (day [instant])
  (day-of-week [instant])
  (hour [instant])
  (minute [instant])
  (sec [instant])
  (instant->map [instant]))

(defn- to-map [year month day hour minute second]
  {:year year
   :month month
   :day day
   :hour hour
   :minute minute
   :second second})

(extend-protocol DateTimeProtocol
  Date
  (year [dt] (.get (calendar dt) Calendar/YEAR))
  (month [dt] (inc (.get (calendar dt) Calendar/MONTH)))
  (day [dt] (.get (calendar dt) Calendar/DAY_OF_MONTH))
  (day-of-week [dt] (.get (calendar dt) Calendar/DAY_OF_WEEK))
  (hour [dt] (.get (calendar dt) Calendar/HOUR_OF_DAY))
  (minute [dt] (.get (calendar dt) Calendar/MINUTE))
  (sec [dt] (.get (calendar dt) Calendar/SECOND))
  (instant->map [dt]
    (let [cal (calendar dt)]
      (to-map
       (.get cal Calendar/YEAR)
       (inc (.get cal Calendar/MONTH))
       (.get cal Calendar/DAY_OF_MONTH)
       (.get cal Calendar/HOUR_OF_DAY)
       (.get cal Calendar/MINUTE)
       (.get cal Calendar/SECOND)))))

(defn map-parse
  [^String s]
  (instant->map (normalize-datetime s)))
