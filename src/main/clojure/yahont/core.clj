(ns yahont.core
  (:require
   [yahont.app :refer [app]]
   [yahont.config :refer [web-server-port hot-reload-web-server]]
   [ring.adapter.jetty :refer [run-jetty]]))

(defn start [& {:keys [port hot-reload] :or {port web-server-port hot-reload hot-reload-web-server}}]
  (run-jetty app {:port port :join? hot-reload}))

(defn -main []
  (start))
