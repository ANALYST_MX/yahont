(ns yahont.env)

(defonce os (System/getProperty "os.name"))
(defonce os-arch (System/getProperty "os.arch"))
(defonce user-home (System/getProperty (str "user.home")))
(defonce classpath (System/getProperty "java.class.path"))
(defonce java-home (System/getProperty "java.home"))
(defonce system-tmp-dir (System/getProperty "java.io.tmpdir"))
(defonce repository-url "https://bitbucket.org/ANALYST_MX/yahont")
