(ns yahont.modules)

(def ^:const application "application")
(def ^:const dictionaries "dictionaries")
(def ^:const register "register")
(def ^:const status_code "status_code")
(def ^:const versions "versions")
(def ^:const subdivisions "subdivisions")
(def ^:const services "services")
