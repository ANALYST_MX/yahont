(ns yahont.app
  (:use [compojure.core])
  (:require
   [yahont.routes :refer [my-routes]]
   [compojure.handler :as handler]))

(def app
  (-> my-routes
      handler/site))
