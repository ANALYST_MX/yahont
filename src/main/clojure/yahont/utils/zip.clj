(ns yahont.utils.zip
  (:require
   [clojure.java.io :as io])
  (:import
   [java.io StringWriter File FileInputStream FileOutputStream BufferedInputStream BufferedOutputStream]
   [java.util.zip ZipFile ZipEntry ZipInputStream]))

(def ^:const buffer 2048)
(def ^:const file_name_sentinel "_file_#")
(def ^:const entry_name_sentinel "_entry_#")

(defn entry-name [file] (.getName file))

(defn encode-file-name [^File zipfile ^String current-entry]
  (str file_name_sentinel (.getName zipfile) entry_name_sentinel current-entry))

(defn list-entries
  [zipfile]
  (enumeration-seq (.entries zipfile)))

(defn list-file-entries
  [zipfile]
  (remove #(.isDirectory %) (list-entries zipfile)))

(defn list-file
  [zipfile]
  (reduce
   (fn [acc file]
     (with-open [in (.getInputStream zipfile file)
                 out (StringWriter.)]
       (io/copy in out)
       (assoc acc (entry-name file) (str out))))
   {}
   (list-file-entries zipfile)))

(defn zipfile [file]
  (ZipFile. file))

(defn read-zip
  [file]
  (with-open [z (zipfile file)]
    (list-file z)))

(defn read-zip-matches-filename
  [file re]
  (filter #(re-matches re (key %)) (read-zip file)))

(defn filename-contains? [file filename]
  (let [entries (list-entries (zipfile file))]
    (.contains (map #(.getName %) entries) filename)))

(defn extract-file [zipfile current-entry zis]
  (let [buf (make-array (. Byte TYPE) buffer)
        output-file (File/createTempFile "zip_" (encode-file-name zipfile current-entry))]
    (with-open [#^BufferedOutputStream bos (BufferedOutputStream. (FileOutputStream. output-file) buffer)]
      (do
        (loop [bytes-read (.read zis buf 0 buffer)]
          (if (= bytes-read -1)
            output-file
            (recur
             (do
               (.write bos buf 0 bytes-read)
               (.read zis buf 0 buffer)))))))))

(defn extract-file-from-zipfile [^java.util.regex.Pattern re ^File zipfile]
  (let [path (.getAbsolutePath zipfile)
        zis (ZipInputStream. (BufferedInputStream. (FileInputStream. zipfile)))]
    (loop [current-entry (.getNextEntry zis)]
      (do
        (if (re-find re (.getName current-entry))
          (extract-file zipfile current-entry zis)
          (recur (.getNextEntry zis)))))))
