(ns yahont.utils.xml
  (:import [javax.xml XMLConstants]
           [org.xml.sax SAXException]
           [javax.xml.validation SchemaFactory]
           [java.io File StringReader Writer]
           [javax.xml.transform.stream StreamSource]
           [javax.xml.stream XMLStreamWriter XMLOutputFactory])
  (:require [yahont.config :refer [register-xml-schema-validator]]
            [clojure.data.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :as zip-xml]
            [yahont.utils.convert :as convert]))

(def ^:const empty-element {})

(defn root [^File file]
  (-> file
      clojure.java.io/input-stream
      xml/parse
      zip/xml-zip))

(defn emit [e ^Writer stream & {:as opts}]
  (let [^XMLStreamWriter writer (-> (XMLOutputFactory/newInstance)
                                    (.createXMLStreamWriter stream))]
    (when (instance? java.io.OutputStreamWriter stream)
      (xml/check-stream-encoding stream (or (:encoding opts) "UTF-8")))
    (.writeStartDocument writer (or (:encoding opts) "UTF-8") (or (:version opts) "1.0"))
    (doseq [event (xml/flatten-elements [e])]
      (xml/emit-event event writer))
    (.writeEndDocument writer)
    stream))

(defn emit-element [tag & [attrs & content]]
  (xml/element tag (or attrs {}) (remove nil? content)))

(defn is-valid? [^File file]
  (let [schema (-> (register-xml-schema-validator)
                   clojure.java.io/file
                   StreamSource.)
        validator (-> (SchemaFactory/newInstance XMLConstants/W3C_XML_SCHEMA_NS_URI)
                      (.newSchema schema)
                      .newValidator)]
    (try
      (.validate validator (StreamSource. file))
      true
      (catch SAXException e false))))

(defn text [preds]
  (zip-xml/text (apply zip-xml/xml1-> preds)))

(defn node [loc & preds]
  (first (apply zip-xml/xml-> loc preds)))
    
(defn nodes [loc & preds]
  (apply zip-xml/xml-> loc preds))
    
(defn element [name & preds]
  (try
    (assoc {} name (text preds))
    (catch NullPointerException e empty-element)))
