(ns yahont.utils.zip.register
  (:require [yahont.utils.zip :as zip]))

(defn read-register
  [path]
  (zip/read-zip-matches-filename path #"[H|L]M.+"))

(defn extract-hm-file [^java.io.File zipfile]
  (zip/extract-file-from-zipfile #"HM.+" zipfile))

(defn extract-lm-file [^java.io.File zipfile]
  (zip/extract-file-from-zipfile #"LM.+" zipfile))

(defn extract-register-id-file [^java.io.File zipfile]
  (if (zip/filename-contains? zipfile "ID_REG.txt")
    (zip/extract-file-from-zipfile #"ID_REG.+" zipfile)))
