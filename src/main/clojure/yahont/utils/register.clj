(ns yahont.utils.register
  (:require [clojure.string :refer [trim-newline]]
            [yahont.utils.xml :refer [root]]
            [yahont.utils.zip.register :as register]
            [yahont.utils.xml.register :as xml]))

(defn id->map [^java.io.File zipfile]
  (when-let [id-file (register/extract-register-id-file zipfile)]
    {:id (-> id-file
             slurp
             trim-newline)}))

(defn hm->map [^java.io.File zipfile]
  {:hm (-> zipfile
           register/extract-hm-file
           root
           xml/root-hm->map)})

(defn lm->map [^java.io.File zipfile]
  {:lm (-> zipfile
           register/extract-lm-file
           root
           xml/root-lm->map)})

(defn ->map [^java.io.File zipfile]
  (into {} ((juxt id->map hm->map lm->map) zipfile)))
