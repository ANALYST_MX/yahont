(ns yahont.utils.convert
  (:require
   [clojure.string :refer [lower-case]]
   [yahont.support.time.format :as format]
   [yahont.syntax.conditional :refer [in?]]))

(defn ->str [x]
  (if (string? x) x (str x)))

(defn ->str->int [str]
  (->> (->str str)
       (re-find #"\d+")
       Integer.))

(defn ->str->double [str]
  (->> (->str str)
       (re-find #"\d+(\.\d+)?")
       first
       Double.))

(defn ->str->date [str]
  (format/normalize-datetime (->str str)))

(defn ->str->sql-date [str]
  (-> str
      ->str->date
      .getTime
      java.sql.Date.))

(defn ->str->timestamp [str]
  (java.sql.Timestamp. (.getTime (->str->date str))))

(defn ->str->date->map [str]
  (format/instant->map (->str->date str)))

(defn ->str->date->year [str]
  (format/year (->str->date str)))

(defn ->str->date->month [str]
  (format/month (->str->date str)))

(defn ->str->date->day [str]
  (format/day (->str->date str)))

(defn ->str->date->hour [str]
  (format/hour (->str->date str)))

(defn ->str->date->minute [str]
  (format/minute (->str->date str)))

(defn ->str->date->sec [str]
  (format/sec (->str->date str)))

(def ^:private ^:const TRUTHY '("t" "true" "y" "yes" "on" "1"))

(defn ->str->boolean [str]
  (in? TRUTHY (->str str)))
