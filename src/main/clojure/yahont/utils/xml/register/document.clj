(ns yahont.utils.xml.register.document
  (:require [yahont.utils.xml :as xml]))

(defn data [root]
  (xml/element :data root :ZGLV :DATA))

(defn filename [root]
  (xml/element :filename root :ZGLV :FILENAME))

(defn ->map [root]
  {:document (into {} ((juxt data filename) root))})
