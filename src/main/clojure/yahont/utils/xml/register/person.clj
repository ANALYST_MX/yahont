(ns yahont.utils.xml.register.person
  (:require [yahont.utils.xml :as xml]))

(defn id_pac [person]
  (xml/element :id_pac person :ID_PAC))

(defn fam [person]
  (xml/element :fam person :FAM))

(defn im [person]
  (xml/element :im person :IM))

(defn ot [person]
  (xml/element :ot person :OT))

(defn w [person]
  (xml/element :w person :W))

(defn dr [event]
  (xml/element :dr event :DR))

(defn fam_p [person]
  (xml/element :fam_p person :FAM_P))

(defn im_p [person]
  (xml/element :im_p person :IM_P))

(defn ot_p [person]
  (xml/element :ot_p person :OT_P))

(defn w_p [person]
  (xml/element :w_p person :W_P))

(defn dr_p [event]
  (xml/element :dr_p event :DR))

(defn mr [person]
  (xml/element :mr person :MR))

(defn doctype [person]
  (xml/element :doctype person :DOCTYPE))

(defn docser [person]
  (xml/element :docser person :DOCSER))

(defn docnum [person]
  (xml/element :docnum person :DOCNUM))

(defn snils [person]
  (xml/element :snils person :SNILS))

(defn status [person]
  (xml/element :status person :STATUS))

(defn oksm [person]
  (xml/element :oksm person :OKSM))

(defn okatog [person]
  (xml/element :okatog person :OKATOG))

(defn okatop [person]
  (xml/element :okatop person :OKATOP))

(defn zip [person]
  (xml/element :zip person :ZIP))

(defn area [person]
  (xml/element :area person :AREA))

(defn region [person]
  (xml/element :region person :REGION))

(defn reg_city [person]
  (xml/element :reg_city person :REG_CITY))

(defn item [person]
  (xml/element :item person :ITEM))

(defn type_item [person]
  (xml/element :type_item person :TYPE_ITEM))

(defn type_ul [person]
  (xml/element :type_ul person :TYPE_UL))

(defn street [person]
  (xml/element :street person :STREET))

(defn house [person]
  (xml/element :house person :HOUSE))

(defn liter [person]
  (xml/element :liter person :LITER))

(defn flat [person]
  (xml/element :flat person :FLAT))

(defn comentp [person]
  (xml/element :comentp person :COMENTP))

(defn dost [person]
  (xml/element :dost person :DOST))

(defn dost_p [person]
  (xml/element :dost_p person :DOST_P))

(defn person->map [person]
  {:person (into {} ((juxt
                      id_pac fam im ot w dr fam_p
                      im_p ot_p w_p dr_p mr doctype
                      docser docnum snils status
                      oksm okatog okatop zip area
                      region reg_city item type_item
                      type_ul street house liter
                      flat comentp dost dost_p) person))})

(defn ->map [persons]
  {:persons (mapv person->map persons)})
