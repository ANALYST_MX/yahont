(ns yahont.utils.xml.register.record.event.service
  (:require [yahont.utils.xml :as xml]))

(defn idserv [service]
  (xml/element :idserv service :IDSERV))

(defn vid_vme [service]
  (xml/element :vid_vme service :VID_VME))

(defn det [service]
  (xml/element :det service :DET))

(defn date_in [service]
  (xml/element :date_in service :DATE_IN))

(defn date_out [service]
  (xml/element :date_out service :DATE_OUT))

(defn ds [service]
  (xml/element :ds service :DS))

(defn code_usl [service]
  (xml/element :code_usl service :CODE_USL))

(defn kol_usl [service]
  (xml/element :kol_usl service :KOL_USL))

(defn tarif [service]
  (xml/element :tarif service :TARIF))

(defn sumv_usl [service]
  (xml/element :sumv_usl service :SUMV_USL))

(defn prvs [service]
  (xml/element :prvs service :PRVS))

(defn code_md [service]
  (xml/element :code_md service :CODE_MD))

(defn comentu [service]
  (xml/element :comentu service :COMENTU))

(defn service->map [service]
  {:service (into {} ((juxt
                       idserv vid_vme det date_in
                       date_out ds code_usl kol_usl tarif
                       sumv_usl prvs code_md comentu) service))})

(defn ->map [services]
  {:services (mapv service->map services)})
