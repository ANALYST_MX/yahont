(ns yahont.utils.xml.register.record.event.failure_cause
  (:require [yahont.utils.xml :as xml]))

(defn code [failure-cause]
  (xml/element :code failure-cause :CODE))

(defn comments [failure-cause]
  (xml/element :comments failure-cause :COMMENTS))

(defn ->map [failure-cause]
  {:failure-cause (into {} ((juxt code comments) failure-cause))})
