(ns yahont.utils.xml.register.record.patient
  (:require [yahont.utils.xml :as xml]))

(defn id_pac [patient]
  (xml/element :id_pac patient :ID_PAC))

(defn vpolis [patient]
  (xml/element :vpolis patient :VPOLIS))

(defn spolis [patient]
  (xml/element :spolis patient :SPOLIS))

(defn npolis [patient]
  (xml/element :npolis patient :NPOLIS))

(defn st_okato [patient]
  (xml/element :st_okato patient :ST_OKATO))

(defn smo [patient]
  (xml/element :smo patient :SMO))

(defn smo_ogrn [patient]
  (xml/element :smo_ogrn patient :SMO_OGRN))

(defn smo_ok [patient]
  (xml/element :smo_ok patient :SMO_OK))

(defn smo_nam [patient]
  (xml/element :smo_nam patient :SMO_NAM))

(defn prin_pol [patient]
  (xml/element :prin_pol patient :PRIN_POL))

(defn novor [patient]
  (xml/element :novor patient :NOVOR))

(defn vnov_d [patient]
  (xml/element :vnov_d patient :VNOV_D))

(defn ->map [patient]
  {:patient (into {} ((juxt
                       id_pac vpolis spolis npolis
                       st_okato smo smo_ogrn smo_ok
                       smo_nam prin_pol novor vnov_d) patient))})
