(ns yahont.utils.xml.register.record.event
  (:require [yahont.utils.xml :as xml]
            [yahont.utils.xml.register.record.event.sanction :as sanction]
            [yahont.utils.xml.register.record.event.service :as service]
            [yahont.utils.xml.register.record.event.failure_cause :as failure-cause]))

(defn idcase [event]
  (xml/element :idcase event :IDCASE))

(defn usl_ok [event]
  (xml/element :usl_ok event :USL_OK))

(defn vidpom [event]
  (xml/element :vidpom event :VIDPOM))

(defn for_pom [event]
  (xml/element :for_pom event :FOR_POM))

(defn vid_hmp [event]
  (xml/element :vid_hmp event :VID_HMP))

(defn metod_hmp [event]
  (xml/element :metod_hmp event :METOD_HMP))

(defn npr_mo [event]
  (xml/element :npr_mo event :NPR_MO))

(defn extr [event]
  (xml/element :extr event :EXTR))

(defn lpu_dep [event]
  (xml/element :lpu_dep event :LPU_DEP))

(defn amb_dep [event]
  (xml/element :amb_dep event :AMB_DEP))

(defn det [event]
  (xml/element :det event :DET))

(defn nhistory [event]
  (xml/element :nhistory event :NHISTORY))

(defn date_1 [event]
  (xml/element :date_1 event :DATE_1))

(defn date_2 [event]
  (xml/element :date_2 event :DATE_2))

(defn ds0 [event]
  (xml/element :ds0 event :DS0))

(defn ds1 [event]
  (xml/element :ds1 event :DS1))

(defn ds2 [event]
  (xml/element :ds2 event :DS2))

(defn ds3 [event]
  (xml/element :ds3 event :DS3))

(defn code_mes1 [event]
  (xml/element :code_mes1 event :CODE_MES1))

(defn code_mes2 [event]
  (xml/element :code_mes2 event :CODE_MES2))

(defn rslt [event]
  (xml/element :rslt event :RSLT))

(defn ishod [event]
  (xml/element :ishod event :ISHOD))

(defn prvs [event]
  (xml/element :prvs event :PRVS))

(defn iddokt [event]
  (xml/element :iddokt event :IDDOKT))

(defn idsp [event]
  (xml/element :idsp event :IDSP))

(defn ed_col [event]
  (xml/element :ed_col event :ED_COL))

(defn rean_d [event]
  (xml/element :rean_d event :REAN_D))

(defn kskp_coef [event]
  (xml/element :kskp_coef event :KSKP_COEF))

(defn kpg [event]
  (xml/element :kpg event :KPG))

(defn ksg [event]
  (xml/element :ksg event :KSG))

(defn tarif_r [event]
  (xml/element :tarif_r event :TARIF_R))

(defn tarif_f [event]
  (xml/element :tarif_f event :TARIF_F))

(defn sumv [event]
  (xml/element :sumv event :SUMV))

(defn sumv_r [event]
  (xml/element :sumv_r event :SUMV_R))

(defn sumv_f [event]
  (xml/element :sumv_f event :SUMV_F))

(defn oplata [event]
  (xml/element :oplata event :OPLATA))

(defn sump [event]
  (xml/element :sump event :SUMP))

(defn sump_r [event]
  (xml/element :sump_r event :SUMР_R))

(defn sump_f [event]
  (xml/element :sump_f event :SUMР_F))

(defn sank_it [event]
  (xml/element :sank_it event :SANK_IT))

(defn comentsl [event]
  (xml/element :comentsl event :COMENTSL))

(defn vnov_m [event]
  (xml/element :vnov_m event :VNOV_M))

(defn os_sluch [event]
  (xml/element :os_sluch event :OS_SLUCH))

(defn sanction->map [event]
  (sanction/->map (xml/node event :SANK)))

(defn failure-cause->map [event]
  (failure-cause/->map (xml/node event :REFREASON)))

(defn services->map [event]
  (service/->map (xml/nodes event :USL)))

(defn ->map [event]
  {:event (into {} ((juxt
                     idcase usl_ok vidpom for_pom vid_hmp
                     metod_hmp npr_mo extr lpu_dep amb_dep
                     det nhistory date_1 date_2 ds0 ds1 ds2
                     ds3 code_mes1 code_mes2 rslt ishod prvs
                     iddokt idsp ed_col rean_d kskp_coef kpg
                     ksg tarif_r tarif_f sumv sumv_r sumv_f
                     oplata sump sump_r sump_f sank_it
                     comentsl vnov_m os_sluch sanction->map
                     services->map failure-cause->map) event))})
