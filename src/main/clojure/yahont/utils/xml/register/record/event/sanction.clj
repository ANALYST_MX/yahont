(ns yahont.utils.xml.register.record.event.sanction
  (:require [yahont.utils.xml :as xml]))

(defn s_code [sanction]
  (xml/element :s_code sanction :S_CODE))

(defn s_sum [sanction]
  (xml/element :s_sum sanction :S_SUM))

(defn s_tip [sanction]
  (xml/element :s_tip sanction :S_TIP))

(defn s_osn [sanction]
  (xml/element :s_osn sanction :S_OSN))

(defn s_com [sanction]
  (xml/element :s_com sanction :S_COM))

(defn s_ist [sanction]
  (xml/element :s_ist sanction :S_IST))

(defn ->map [sanction]
  {:sanction (into {} ((juxt s_code s_sum s_tip s_osn s_com s_ist) sanction))})
