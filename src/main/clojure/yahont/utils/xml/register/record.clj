(ns yahont.utils.xml.register.record
  (:require [yahont.utils.xml :as xml]
            [yahont.utils.xml.register.record.patient :as patient]
            [yahont.utils.xml.register.record.event :as event]))

(defn n_zap [record]
  (xml/element :n_zap record :N_ZAP))

(defn pr_nov [record]
  (xml/element :pr_nov record :PR_NOV))

(defn patient->map [record]
  (patient/->map (xml/node record :PACIENT)))

(defn event->map [record]
  (event/->map (xml/node record :SLUCH)))

(defn record->map [record]
  {:record (into {} ((juxt n_zap pr_nov patient->map event->map) record))})

(defn ->map [records]
  {:records (mapv record->map records)})
