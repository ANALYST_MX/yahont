(ns yahont.utils.xml.register.invoice
  (:require [yahont.utils.xml :as xml]))

(defn code [root]
  (xml/element :code root :SCHET :CODE))

(defn nschet [root]
  (xml/element :nschet root :SCHET :NSCHET))

(defn plat [root]
  (xml/element :plat root :SCHET :PLAT))

(defn dschet [root]
  (xml/element :dschet root :SCHET :DSCHET))

(defn summav [root]
  (xml/element :summav root :SCHET :SUMMAV))

(defn summav_r [root]
  (xml/element :summav_r root :SCHET :SUMMAV_R))

(defn summav_f [root]
  (xml/element :summav_f root :SCHET :SUMMAV_F))

(defn summap [root]
  (xml/element :summap root :SCHET :SUMMAP))

(defn summap_r [root]
  (xml/element :summap_r root :SCHET :SUMMAP_R))

(defn summap_f [root]
  (xml/element :summap_f root :SCHET :SUMMAP_F))

(defn sank_mek [root]
  (xml/element :sank_mek root :SCHET :SANK_MEK))

(defn sank_mee [root]
  (xml/element :sank_mee root :SCHET :SANK_MEE))

(defn sank_ekmp [root]
  (xml/element :sank_ekmp root :SCHET :SANK_EKMP))

(defn coments [root]
  (xml/element :coments root :SCHET :COMENTS))

(defn disp [root]
  (xml/element :disp root :SCHET :DISP))

(defn ->map [root]
  {:invoice (into {} ((juxt
                       code nschet plat dschet
                       summav summav_r summav_f
                       summap summap_r summap_f
                       sank_mek sank_mee sank_ekmp
                       coments disp) root))})
