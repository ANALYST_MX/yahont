(ns yahont.utils.xml.register
  (:require [yahont.utils.xml :as xml]
            [yahont.utils.xml.register.document :as document]
            [yahont.utils.xml.register.invoice :as invoice]
            [yahont.utils.xml.register.record :as record]
            [yahont.utils.xml.register.person :as person]))

(defn podr [root]
  (xml/element :podr root :SCHET :PODR))

(defn version-xml [root]
  (xml/element :version_xml root :ZGLV :VERSION))

(defn year [root]
  (xml/element :year root :SCHET :YEAR))

(defn month [root]
  (xml/element :month root :SCHET :MONTH))

(defn register->map [root]
  {:register (into {} ((juxt podr version-xml year month) root))})

(defn records->map [root]
  (record/->map (xml/nodes root :ZAP)))

(defn document->map [root]
  (document/->map root))

(defn invoice->map [root]
  (invoice/->map root))

(defn persons->map [root]
  (person/->map (xml/nodes root :PERS)))

(defn root-hm->map [root]
  (into {} ((juxt register->map document->map invoice->map records->map) root)))

(defn root-lm->map [root]
  (into {} ((juxt document->map persons->map) root)))
