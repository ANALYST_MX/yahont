(ns yahont.modules.services.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.services.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/home (t :label/home))]
                   [:li (link-to url/services-paid-account-contract (t :label/account-contract))]]]]))
