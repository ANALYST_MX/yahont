(ns yahont.modules.services.paid.account.controllers.contract.show
  (:require [yahont.modules.services.paid.account.views.contract.show :as view]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.services.paid.account.persistence.contract :as contract]))

(defn index [id]
  (try
    (view/index (provision/key-entry (contract/select-by-id id)))
    (catch Exception e (view/error))))
