(ns yahont.modules.services.paid.account.db.schema.contract.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.services.paid.account.provision.contract])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code name surname patronymic signing-date
            total department institution funding-source paid])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   name convert/->str
   surname convert/->str
   patronymic convert/->str
   signing-date convert/->str->sql-date
   total convert/->str->double
   department convert/->str->int
   institution convert/->str->int
   funding-source convert/->str->int
   paid convert/->str->int})

(def mapping
  {})
