(ns yahont.modules.services.paid.account.views.contract.show
  (:require [yahont.url :as url]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.services.paid.account.provision.contract :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn index [contract]
  (common :title (t :title/contract)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/services-paid-account-contract (t :label/contract))]
                   [:li (link-to url/services-paid-account-contracts (t :label/contracts))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code contract)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name contract)
                  [:br]
                  (label "surname" (t :label/surname))
                  [:br]
                  (provision/surname contract)
                  [:br]
                  (label "patronymic" (t :label/patronymic))
                  [:br]
                  (provision/patronymic contract)
                  [:br]
                  (label "signing_date" (t :label/signing-date))
                  [:br]
                  (unparse default-formatter (provision/signing-date contract))
                  [:br]
                  (label "total" (t :label/total))
                  [:br]
                  (provision/total contract)
                  [:br]
                  (label "institution" (t :label/institution))
                  [:br]
                  (provision/institution contract)
                  [:br]
                  (label "department" (t :label/department))
                  [:br]
                  (provision/department contract)
                  [:br]
                  (label "funding_source" (t :label/funding-source))
                  [:br]
                  (provision/funding-source contract)
                  [:br]
                  (label "paid" (t :label/paid))
                  [:br]
                  (provision/paid contract)
                  [:br]]]))

(defn error [] (server-error))
