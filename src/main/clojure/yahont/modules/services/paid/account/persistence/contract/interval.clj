(ns yahont.modules.services.paid.account.persistence.contract.interval
  (:require [honeysql.helpers :refer :all]
            [yahont.modules.services.paid.account.models.contract :as contract]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.db.marshal :refer [entries->map]]
            [yahont.modules.services.paid.account.models :as models]
            [yahont.modules.application.helpers.internal :as helpers]))

(defn paginate
  ([page num-per-page] (paginate [:= 1 1] page num-per-page))
  ([condition page num-per-page]
   (entries->map models/contract (-> (select :*)
                                     (from (keyword provision/contract))
                                     (where condition)
                                     (helpers/paginate :page page :num-per-page num-per-page)
                                     contract/select-rows))))
