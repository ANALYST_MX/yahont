(ns yahont.modules.services.paid.account.controllers.contract.migration
  (:require [yahont.modules.services.paid.account.views.contract.migration :as view]
            [yahont.modules.services.paid.account.models.contract :as contract]))

(defn migration-table []
  (try
    (contract/create-table)
    (view/ok)
    (catch Exception e (view/error))))
