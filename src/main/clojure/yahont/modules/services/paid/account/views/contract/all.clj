(ns yahont.modules.services.paid.account.views.contract.all
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.services.paid.account.persistence.contract]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.application.views.templates.internal :as render-internal]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn contract-summary [contract]
  [:tr
   [:td (provision/pk contract)]
   [:td (provision/code contract)]
   [:td (provision/name contract)]
   [:td (provision/surname contract)]
   [:td (unparse default-formatter (provision/signing-date contract))]
   [:td (provision/total contract)]
   [:td [:a {:href (string/replace url/services-paid-account-contract-edit #":id" (str (provision/pk contract)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/services-paid-account-contract-delete #":id" (str (provision/pk contract)))} (t :label/delete)]]])

(defn list-contracts [contracts]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name" "surname" "date" "total"])
     ]]
   (into [:tbody]
         (map #(contract-summary (provision/key-entry %)) contracts))])

(defn index [contracts page page-count per-page]
  (common :title (t :title/contract)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/services-paid-account-contract (t :label/home))]
                   ]]
                 [:div.all
                  (list-contracts contracts)]
                 [:div.pagination
                  (render-internal/paginate page page-count per-page)]]))

(defn error [] (server-error))
