(ns yahont.modules.services.paid.account.views.contract.create
  (:require [yahont.url :as url]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [yahont.syntax.html.tag :refer [add-option-tooltip]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field text-area check-box submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.dictionaries.register.controllers.department.templates :as render-department]
            [yahont.modules.dictionaries.register.controllers.institution.templates :as render-institution]
            [yahont.modules.dictionaries.common.controllers.yes_no.templates :as render-yes-no]
            [yahont.modules.dictionaries.funding.controllers.source.templates :as render-funding-source]))

(defn index []
  (common :title (t :title/contract)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/services-paid-account-contract (t :label/home))]
                    ]]
                 [:div.create
                  (form-to [:post url/services-paid-account-contract-create]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (label "surname" (t :label/surname))
                           [:br]
                           (text-field (name provision/surname))
                           [:br]
                           (label "patronymic" (t :label/patronymic))
                           [:br]
                           (text-field (name provision/patronymic))
                           [:br]
                           (label "signing_date" (t :label/signing-date))
                           [:br]
                           (text-field (name provision/signing-date))
                           [:br]
                           (label "total" (t :label/total))
                           [:br]
                           (text-field (name provision/total))
                           [:br]
                           (label "institution" (t :label/institution))
                           [:br]
                           [:select {provision/pk provision/institution provision/name provision/institution}
                            (render-institution/select-options)
                            ]
                           [:br]
                           (label "department" (t :label/department))
                           [:br]
                           [:select {provision/pk provision/department provision/name provision/department}
                            (add-option-tooltip (render-department/select-options) (t :option/tooltip))
                            ]
                           [:br]
                           (label "funding_source" (t :label/funding-source))
                           [:br]
                           [:select {provision/pk provision/funding-source provision/name provision/funding-source}
                            (add-option-tooltip (render-funding-source/select-options) (t :option/tooltip))
                            ]
                           [:br]
                           (label "paid" (t :label/paid))
                           [:br]
                           [:select {provision/pk provision/paid provision/name provision/paid}
                            (render-yes-no/select-options)
                            ]
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
