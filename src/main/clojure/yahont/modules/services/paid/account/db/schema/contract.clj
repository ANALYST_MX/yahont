(ns yahont.modules.services.paid.account.db.schema.contract
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.services.paid.account.db.schema.contract.row.scope :refer [scope conversion-pk]]
            [yahont.modules.services.paid.account.db.schema.contract.row.conversion :refer [conversion]]
            [yahont.modules.services.paid.account.db.schema.contract.row.mapping :refer [mapping]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (conversion-pk id))
