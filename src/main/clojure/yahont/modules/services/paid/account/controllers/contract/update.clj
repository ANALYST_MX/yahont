(ns yahont.modules.services.paid.account.controllers.contract.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.services.paid.account.views.contract.update :as view]
            [yahont.modules.services.paid.account.persistence.contract :as contract]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (contract/update-by-id id params)
    (redirect (string/replace url/services-paid-account-contract-show #":id" id))
    (catch Exception e (view/error))))
