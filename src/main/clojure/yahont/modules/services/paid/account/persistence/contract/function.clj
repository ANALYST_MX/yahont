(ns yahont.modules.services.paid.account.persistence.contract.function
  (:refer-clojure :exclude [count])
  (:require [honeysql.helpers :refer :all]
            [yahont.modules.services.paid.account.models.contract :as contract]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.db.marshal :refer [entry->map]]
            [yahont.modules.services.paid.account.models :as models]))

(defn count
  ([] (count [:= 1 1]))
  ([condition]
   (entry->map models/contract (-> (select :%count.*)
                                   (from (keyword provision/contract))
                                   (where
                                    condition)
                                   contract/select-rows))))
