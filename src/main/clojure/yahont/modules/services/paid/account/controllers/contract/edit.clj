(ns yahont.modules.services.paid.account.controllers.contract.edit
  (:require [yahont.url :as url]
            [yahont.modules.services.paid.account.views.contract.edit :as view]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.services.paid.account.persistence.contract :as contract]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (contract/select-by-id id)))
    (catch Exception e (view/error))))
