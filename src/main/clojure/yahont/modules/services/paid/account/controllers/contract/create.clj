(ns yahont.modules.services.paid.account.controllers.contract.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.services.paid.account.views.contract.create :as view]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.services.paid.account.persistence.contract :as contract]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (contract/create params)))]
      (redirect (string/replace url/services-paid-account-contract-show #":id" (str id))))
    (catch Exception e (view/error))))
