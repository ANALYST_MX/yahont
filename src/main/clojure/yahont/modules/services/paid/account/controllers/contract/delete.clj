(ns yahont.modules.services.paid.account.controllers.contract.delete
  (:require [yahont.url :as url]
            [yahont.modules.services.paid.account.views.contract.delete :as view]
            [yahont.modules.services.paid.account.persistence.contract :as contract]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (contract/delete-by-id id)
    (redirect url/services-paid-account-contract)
    (catch Exception e (view/error))))
