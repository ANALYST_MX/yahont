(ns yahont.modules.services.paid.account.views.contract
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/services-home (t :label/home))]
                   [:li (link-to url/services-paid-account-contracts (t :label/all))]
                   [:li (link-to url/services-paid-account-contract-create (t :label/create))]
                   [:li (link-to url/services-paid-account-contract-find (t :label/find))]]]]))
