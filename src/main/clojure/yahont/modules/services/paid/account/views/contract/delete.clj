(ns yahont.modules.services.paid.account.views.contract.delete
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
