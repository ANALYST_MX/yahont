(ns yahont.modules.services.paid.account.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :contract "Договор"
      }
     :label
     {:home "Главная"
      :code "№"
      :name "Имя"
      :surname "Фамилия"
      :patronymic "Отчество"
      :signing-date "Дата"
      :total "Сумма"
      :contract "Договор"
      :contracts "Договоры"
      :department "Отделение"
      :institution "Учреждение"
      :funding-source "Источник финансирования"
      :paid "Оплата"
      :edit "Редактировать"
      :all "Все"
      :show "Просмотреть"
      :find "Искать"
      :create "Создать"
      :delete "Удалить"
      :start-date "С"
      :end-date "До"
      }
     :option
     {
      :tooltip "Выберeте"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
