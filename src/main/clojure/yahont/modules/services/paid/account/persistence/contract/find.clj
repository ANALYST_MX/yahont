(ns yahont.modules.services.paid.account.persistence.contract.find
  (:require [honeysql.helpers :refer :all]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.utils.convert :as convert]
            [yahont.modules.services.paid.account.models.contract :as contract]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.db.marshal :refer [entries->map]]
            [yahont.modules.services.paid.account.models :as models]
            [yahont.modules.dictionaries.register.provision.department :as provision-department]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision-yes-no]
            [yahont.modules.dictionaries.funding.provision.source :as provision-funding-source]))

(defn select-where [condition]
  (entries->map models/contract (-> (select (keyword (full-name provision/contract "*"))
                                            [(keyword (full-name provision-department/department provision-department/name)) "department_name"]
                                            [(keyword (full-name provision-yes-no/yes-no provision-yes-no/name)) "yes_no_name"]
                                            [(keyword (full-name provision-funding-source/source provision-funding-source/name)) "funding_source_name"]
                                            )
                                    (from (keyword provision/contract))
                                    (left-join (keyword provision-department/department)
                                               [:=
                                                (keyword (full-name provision/contract provision/department))
                                                (keyword (full-name provision-department/department provision-department/pk))])
                                    (merge-left-join (keyword provision-yes-no/yes-no)
                                                      [:=
                                                       (keyword (full-name provision/contract provision/paid))
                                                       (keyword (full-name provision-yes-no/yes-no provision-yes-no/pk))])
                                    (merge-left-join (keyword provision-funding-source/source)
                                                     [:=
                                                      (keyword (full-name provision/contract provision/funding-source))
                                                      (keyword (full-name provision-funding-source/source provision-funding-source/pk))])
                                    (where
                                     condition)
                                    contract/select-rows)))
