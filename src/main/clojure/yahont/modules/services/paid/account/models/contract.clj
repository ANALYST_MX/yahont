(ns yahont.modules.services.paid.account.models.contract
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id]]))

(defn create-table []
  (migrate/create provision/contract
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/name)
                  (row-specs provision/surname)
                  (row-specs provision/patronymic)
                  (row-specs provision/signing-date)
                  (row-specs provision/total)
                  (row-specs provision/department)
                  (row-specs provision/institution)
                  (row-specs provision/funding-source)
                  (row-specs provision/paid)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/contract row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/contract id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/contract id))

(defn select-all-rows []
  (select-all provision/contract))

(defn select-rows [sql]
  (select (sql/format sql)))

(defn delete-row-by-id [id]
  (delete-by-id provision/contract id))
