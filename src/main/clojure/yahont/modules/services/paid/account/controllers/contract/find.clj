(ns yahont.modules.services.paid.account.controllers.contract.find
  (:refer-clojure :exclude [find])
  (:require [yahont.utils.convert :as convert]
            [yahont.support.time :refer [now]]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.modules.services.paid.account.views.contract.find :as view]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.syntax.conditional :refer [if-not-empty]]
            [yahont.modules.services.paid.account.persistence.contract.find :as contract]
            [yahont.syntax.binding :refer [let-try]]))

(defn index []
  (view/index))

(defn- join-conditions [& conditions]
  (if-let [conditions (filter identity conditions)]
    (if (> (count conditions) 1) (into [:and] conditions) (first conditions))))

(defn- prepare-conditions [params]
  (let [start-date (if-not-empty (:start_date params) [:>= provision/signing-date (convert/->str->sql-date (:start_date params))])
        end-date (if-not-empty (:end_date params) [:<= provision/signing-date (convert/->str->sql-date (:end_date params))])
        code (if-not-empty (provision/code params) [:= (keyword (full-name provision/contract provision/code)) (provision/code params)])]
    (join-conditions start-date end-date code)))

(defn find [params]
  (let-try [start-date (:start_date params)
            end-date (:end_date params)
            code (provision/code params)]
           (view/index (provision/key-entries (contract/select-where (prepare-conditions params)))
                       {:start_date (if-not-empty start-date (convert/->str->date start-date))
                        :end_date (if-not-empty end-date (convert/->str->date end-date))
                        provision/code (if-not-empty code code)})
           (catch Exception e (view/error))))
