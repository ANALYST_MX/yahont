(ns yahont.modules.services.paid.account.views.contract.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.syntax.html.tag :refer [add-option-tooltip]]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field check-box submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.dictionaries.register.controllers.department.templates :as render-department]
            [yahont.modules.dictionaries.register.controllers.institution.templates :as render-institution]
            [yahont.modules.dictionaries.common.controllers.yes_no.templates :as render-yes-no]
            [yahont.modules.dictionaries.funding.controllers.source.templates :as render-funding-source]))

(def default-formatter (formatter "yyyy-MM-dd"))
  
(defn index [contract]
  (common :title (t :title/contract)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/services-paid-account-contract (t :label/home))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/services-paid-account-contract-update #":id" (str (:id contract)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code contract))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name contract))
                           [:br]
                           (label "surname" (t :label/surname))
                           [:br]
                           (text-field (name provision/surname) (provision/surname contract))
                           [:br]
                           (label "patronymic" (t :label/patronymic))
                           [:br]
                           (text-field (name provision/patronymic) (provision/patronymic contract))
                           [:br]
                           (label "signing_date" (t :label/signing-date))
                           [:br]
                           (text-field (name provision/signing-date) (unparse default-formatter (provision/signing-date contract)))
                           [:br]
                           (label "total" (t :label/total))
                           [:br]
                           (text-field (name provision/total) (provision/total contract))
                           [:br]
                           (label "institution" (t :label/institution))
                           [:br]
                           [:select {provision/pk provision/institution provision/name provision/institution}
                            (render-institution/select-options (provision/institution contract))
                            ]
                           [:br]
                           (label "department" (t :label/department))
                           [:br]
                           [:select {provision/pk provision/department provision/name provision/department}
                            (add-option-tooltip (render-department/select-options (provision/department contract)) (t :option/tooltip))
                            ]
                           [:br]
                           (label "funding_source" (t :label/funding-source))
                           [:br]
                           [:select {provision/pk provision/funding-source provision/name provision/funding-source}
                            (add-option-tooltip (render-funding-source/select-options (provision/funding-source contract)) (t :option/tooltip))
                            ]
                           [:br]
                           (label "paid" (t :label/paid))
                           [:br]
                           [:select {provision/pk provision/paid provision/name provision/paid}
                            (render-yes-no/select-options (provision/paid contract))
                            ]
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
