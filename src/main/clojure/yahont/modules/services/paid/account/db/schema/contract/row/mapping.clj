(ns yahont.modules.services.paid.account.db.schema.contract.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.services.paid.account.db.schema.contract.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
