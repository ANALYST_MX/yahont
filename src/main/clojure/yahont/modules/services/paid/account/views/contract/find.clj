(ns yahont.modules.services.paid.account.views.contract.find
  (:require [clojure.string :as string]
            [yahont.syntax.conditional :refer [no-nil-assoc]]
            [yahont.support.time :refer [now]]
            [yahont.utils.convert :as convert]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.support.time.calendar :as calendar]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.url :as url]
            [yahont.modules.services.paid.account.i18n :refer [t]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.services.paid.account.provision.contract :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn contract-summary [contract]
  [:tr
   [:td (provision/pk contract)]
   [:td (provision/code contract)]
   [:td (provision/name contract)]
   [:td (provision/surname contract)]
   [:td (provision/patronymic contract)]
   [:td (unparse default-formatter (provision/signing-date contract))]
   [:td (provision/total contract)]
   [:td (:department_name contract)]
   [:td (:funding_source_name contract)]
   [:td (:yes_no_name contract)]
   [:td [:a {:href (string/replace url/services-paid-account-contract-edit #":id" (str (provision/pk contract)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/services-paid-account-contract-delete #":id" (str (provision/pk contract)))} (t :label/delete)]]])

(defn list-contracts [contracts]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name" "surname" "patronymic" "signing_date" "total" "department" "funding_source" "paid"])
     ]]
   (into [:tbody]
         (map #(contract-summary (provision/key-entry %)) contracts))])

(defn- wrap-params [params]
  (let [date (convert/->str->date->map (now))]
    (-> params
        (no-nil-assoc :start_date (calendar/first-day-of-month (:year date) (:month date)))
        (no-nil-assoc :end_date (calendar/last-day-of-month (:year date) (:month date)))
        )))

(defn index
  ([] (index nil {}))
  ([contracts params]
   (let [params (wrap-params params)]
     (common :title (t :title/contract)
             :body [:div {:id "section"}
                    [:div.menu
                     [:ul
                      [:li (link-to url/services-home (t :label/home))]
                      [:li (link-to url/services-paid-account-contract (t :label/contract))]
                      ]]
                    [:div.find
                     (form-to [:post url/services-paid-account-contract-find]
                              (label "code" (t :label/code))
                              [:br]
                              (text-field provision/code (provision/code params))
                              [:br]
                              (label "start_date" (t :label/start-date))
                              [:br]
                              (text-field "start_date" (if (:start_date params) (unparse default-formatter (:start_date params))))
                              [:br]
                              (label "end_date" (t :label/end-date))
                              [:br]
                              (text-field "end_date" (if (:end_date params) (unparse default-formatter (:end_date params))))
                              [:br]
                              (submit-button (t :label/find)))]
                    [:div.all
                     (list-contracts contracts)]]))))
  
(defn error [] (server-error))
