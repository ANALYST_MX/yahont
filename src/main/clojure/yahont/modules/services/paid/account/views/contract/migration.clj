(ns yahont.modules.services.paid.account.views.contract.migration
  (:require [yahont.modules.application.views.layout :refer [success server-error]]))

(defn ok [] (success))
(defn error [] (server-error))
