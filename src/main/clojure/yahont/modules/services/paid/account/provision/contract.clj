(ns yahont.modules.services.paid.account.provision.contract
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.services.submodules :as services-submodules]
            [yahont.modules.services.paid.submodules :as paid-submodules]
            [yahont.modules.services.paid.account.models :as models]
            [yahont.modules.dictionaries.register.provision.department :as provision-department]
            [yahont.modules.dictionaries.register.provision.institution :as provision-institution]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision-yes-no]
            [yahont.modules.dictionaries.funding.provision.source :as provision-funding-source]))

(def ^:const key-entry (keyword-entry models/contract))

(def ^:const key-entries (keyword-entries models/contract))

(def ^:const contract (naming/table modules/services services-submodules/paid paid-submodules/account models/contract))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key contract))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:varchar "NOT NULL"]} surname :surname)
(def ^{:const true, :specs [:varchar]} patronymic :patronymic)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} signing-date :signing_date)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} total :total)
(def ^{:const true, :specs [:integer]} department (naming/foreign-key provision-department/department))
(def ^{:const true, :specs [:integer "NOT NULL"]} institution (naming/foreign-key provision-institution/institution))
(def ^{:const true, :specs [:integer "NOT NULL"]} funding-source (naming/foreign-key provision-funding-source/source))
(def ^{:const true, :specs [:integer "NOT NULL"]} paid (naming/foreign-key provision-yes-no/yes-no))
