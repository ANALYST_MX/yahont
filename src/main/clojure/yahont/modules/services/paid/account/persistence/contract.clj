(ns yahont.modules.services.paid.account.persistence.contract
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.services.paid.account.db.schema.contract :refer [prepare prepare-pk]]
            [yahont.modules.services.paid.account.models.contract :as contract]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.services.paid.account.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/contract (contract/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/contract (contract/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/contract (contract/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (contract/delete-row-by-id id)))

(defn select-all []
  (entries->map models/contract (contract/select-all-rows)))
