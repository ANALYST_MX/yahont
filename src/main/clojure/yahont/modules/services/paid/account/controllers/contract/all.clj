(ns yahont.modules.services.paid.account.controllers.contract.all
  (:require [yahont.url :as url]
            [yahont.utils.convert :as convert]
            [yahont.modules.services.paid.account.views.contract.all :as view]
            [yahont.modules.services.paid.account.provision.contract :as provision]
            [yahont.modules.services.paid.account.persistence.contract.interval :as contract-interval]
            [yahont.modules.services.paid.account.persistence.contract.function :as function]
            [yahont.modules.application.helpers.internal :refer [default-pagination-count]]
            [yahont.syntax.binding :refer [let-try]]))

(defn index [params]
  (let-try
   [page (if (:page params) (convert/->str->int (:page params)) 1)
    per-page default-pagination-count]
    (view/index (provision/key-entries (contract-interval/paginate page per-page)) page (:count (provision/key-entry (function/count))) per-page)
    (catch Exception e (view/error))))
