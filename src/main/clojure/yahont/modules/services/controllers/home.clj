(ns yahont.modules.services.controllers.home
  (:require [yahont.modules.services.views.home :as view]))

(defn index []
  (view/index))
