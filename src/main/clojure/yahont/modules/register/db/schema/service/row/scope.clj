(ns yahont.modules.register.db.schema.service.row.scope
  (:use [yahont.modules.register.provision.service])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code event vid-vme det
            date-in date-out ds code-usl
            kol-usl tarif sumv-usl
            prvs code-md comentu])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   event convert/->str->int
   vid-vme convert/->str
   det convert/->str->int
   date-in convert/->str->sql-date
   date-out convert/->str->sql-date
   ds convert/->str
   code-usl convert/->str
   kol-usl convert/->str->double
   tarif convert/->str->double
   sumv-usl convert/->str->double
   prvs convert/->str->int
   code-md convert/->str
   comentu convert/->str})

(def mapping
  {:idserv code})
