(ns yahont.modules.register.db.schema.failure_cause.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.failure_cause.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
