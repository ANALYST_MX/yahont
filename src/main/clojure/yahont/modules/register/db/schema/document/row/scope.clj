(ns yahont.modules.register.db.schema.document.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.register.provision.document])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk register date name])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   register convert/->str->int
   date convert/->str->sql-date
   name convert/->str})

(def mapping
  {:filename name
   :data date})
