(ns yahont.modules.register.db.schema.record.row.scope
  (:use [yahont.modules.register.provision.record])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code document pr-nov])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   document convert/->str->int
   pr-nov convert/->str->int})
   
(def mapping
  {:n_zap code})
