(ns yahont.modules.register.db.schema.document.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.document.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
