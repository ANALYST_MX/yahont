(ns yahont.modules.register.db.schema.service.row.conversion
  (:require [yahont.syntax.conditional :refer [if-let*]]
            [yahont.modules.register.db.schema.service.row.scope :as scope]))

(defn- mapping [object]
  (if-let* [[k v] object
            f (k scope/conversion)]
           (conj {k v} {k (f v)})))

(defn conversion [row-map]
  (into {} (map mapping row-map)))
