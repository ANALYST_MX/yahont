(ns yahont.modules.register.db.schema.sanction.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.sanction.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
