(ns yahont.modules.register.db.schema.sanction.row.scope
  (:use [yahont.modules.register.provision.sanction])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code s-sum s-tip s-osn s-com s-ist])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   s-sum convert/->str->double
   s-tip convert/->str->int
   s-osn convert/->str->int
   s-com convert/->str
   s-ist convert/->str->int})

(def mapping
  {:s_code code})
