(ns yahont.modules.register.db.schema.invoice.row.scope
  (:use [yahont.modules.register.provision.invoice])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code number subdivision payer date summav
            summav-r summav-f summap summap-r summap-f disp
            sanction-mek sanction-mee sanction-ekmp coments])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   number convert/->str
   subdivision convert/->str->int
   payer convert/->str->int
   date convert/->str->sql-date
   summav convert/->str->double
   summav-r convert/->str->double
   summav-f convert/->str->double
   summap convert/->str->double
   summap-r convert/->str->double
   summap-f convert/->str->double
   sanction-mek convert/->str->double
   sanction-mee convert/->str->double
   sanction-ekmp convert/->str->double
   coments convert/->str
   disp convert/->str})

(def mapping
  {:nschet number
   :dschet date})
