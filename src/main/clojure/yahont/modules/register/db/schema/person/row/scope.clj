(ns yahont.modules.register.db.schema.person.row.scope
  (:use [yahont.modules.register.provision.person])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code document fam im ot w
            birthday fam-p im-p ot-p w-p
            birthday-p mr doctype docser
            docnum snils status oksm okatog
            okatop zip area region reg-city
            item type-item type-ul street
            house liter flat comentp dost dost-p])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   document convert/->str->int
   fam convert/->str
   im convert/->str
   ot convert/->str
   w convert/->str->int
   birthday convert/->str->sql-date
   fam-p convert/->str
   im-p convert/->str
   ot-p convert/->str
   w-p convert/->str->int
   birthday-p convert/->str->sql-date
   mr convert/->str
   doctype convert/->str
   docser convert/->str
   docnum convert/->str
   snils convert/->str
   status convert/->str->int
   oksm convert/->str
   okatog convert/->str
   okatop convert/->str
   zip convert/->str->int
   area convert/->str
   region convert/->str
   reg-city convert/->str
   item convert/->str
   type-item convert/->str->int
   type-ul convert/->str->int
   street convert/->str
   house convert/->str->int
   liter convert/->str
   flat convert/->str
   comentp convert/->str
   dost convert/->str->int
   dost-p convert/->str->int})

(def mapping
  {:id_pac code
   :dr birthday
   :dr_p birthday-p})
