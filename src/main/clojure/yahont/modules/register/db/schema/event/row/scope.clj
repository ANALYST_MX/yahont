(ns yahont.modules.register.db.schema.event.row.scope
  (:use [yahont.modules.register.provision.event])
  (:require [yahont.utils.convert :as convert]))

(def scope
  [pk code record department usl-ok
   vid-pom for-pom vid-hmp metod-hmp
   npr-mo sanction extr det nhistory
   date-in date-out ds-0 ds-1 ds-2
   ds-3 code-mes-1 code-mes-2 rslt
   ishod prvs iddokt idsp ed-col rean-d
   kskp-coef kpg ksg tarif-r tarif-f
   sumv sumv-r sumv-f oplata
   sump sump-r sump-f sank-it
   comentsl vnov-m os-sluch failure-cause])

(def conversion-pk convert/->str->int)

(def conversion
  {pk convert/->str->int
   code convert/->str
   record convert/->str->int
   department convert/->str->int
   sanction convert/->str->int
   usl-ok convert/->str->int
   vid-pom convert/->str->int
   for-pom convert/->str->int
   vid-hmp convert/->str
   metod-hmp convert/->str->int
   npr-mo convert/->str
   extr convert/->str->int
   det convert/->str->int
   nhistory convert/->str
   date-in convert/->str->sql-date
   date-out convert/->str->sql-date
   ds-0 convert/->str
   ds-1 convert/->str
   ds-2 convert/->str
   ds-3 convert/->str
   code-mes-1 convert/->str
   code-mes-2 convert/->str
   rslt convert/->str->int
   ishod convert/->str->int
   prvs convert/->str->int
   iddokt convert/->str
   idsp convert/->str->int
   ed-col convert/->str->double
   rean-d convert/->str->int
   kskp-coef convert/->str->double
   kpg convert/->str->int
   ksg convert/->str->int
   tarif-r convert/->str->double
   tarif-f convert/->str->double
   sumv convert/->str->double
   sumv-r convert/->str->double
   sumv-f convert/->str->double
   oplata convert/->str->int
   sump convert/->str->double
   sump-r convert/->str->double
   sump-f convert/->str->double
   sank-it convert/->str->double
   comentsl convert/->str
   vnov-m convert/->str->int
   os-sluch convert/->str->int
   failure-cause convert/->str->int})

(def mapping 
  {:idcase code
   :ds0 ds-0
   :ds1 ds-1
   :ds2 ds-2
   :ds3 ds-3
   :code_mes1 code-mes-1
   :code_mes2 code-mes-2
   :date_1 date-in
   :date_2 date-out})
