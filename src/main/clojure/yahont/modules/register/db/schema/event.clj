(ns yahont.modules.register.db.schema.event
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.register.db.schema.event.row.scope :refer [scope conversion-pk]]
            [yahont.modules.register.db.schema.event.row.conversion :refer [conversion]]
            [yahont.modules.register.db.schema.event.row.mapping :refer [mapping]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (when id (conversion-pk id)))
