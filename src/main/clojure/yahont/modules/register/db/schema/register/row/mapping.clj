(ns yahont.modules.register.db.schema.register.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.register.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
