(ns yahont.modules.register.db.schema.person.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.person.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
