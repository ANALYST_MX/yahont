(ns yahont.modules.register.db.schema.patient.row.scope
  (:use [yahont.modules.register.provision.patient])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code record person vpolis spolis npolis st-okato
            smo smo-ogrn smo-ok smo-nam prin-pol novor vnov-d])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   person convert/->str->int
   record convert/->str->int
   vpolis convert/->str->int
   spolis convert/->str
   npolis convert/->str
   st-okato convert/->str
   smo convert/->str
   smo-ogrn convert/->str
   smo-ok convert/->str
   smo-nam convert/->str
   prin-pol convert/->str
   novor convert/->str
   vnov-d convert/->str->int})

(def mapping
  {:id_pac code})
