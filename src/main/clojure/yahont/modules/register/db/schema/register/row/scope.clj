(ns yahont.modules.register.db.schema.register.row.scope
  (:use [yahont.modules.register.provision.register])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code invoice version-xml year month version mode])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   invoice convert/->str->int
   version-xml convert/->str
   year convert/->str->int
   month convert/->str->int
   version convert/->str->int
   mode convert/->str->int})

(def mapping
  {})
