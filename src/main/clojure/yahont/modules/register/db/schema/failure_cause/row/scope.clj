(ns yahont.modules.register.db.schema.failure_cause.row.scope
  (:use [yahont.modules.register.provision.failure_cause])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code comments])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   comments convert/->str})

(def mapping
  {})
