(ns yahont.modules.register.db.schema.service.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.register.db.schema.service.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
