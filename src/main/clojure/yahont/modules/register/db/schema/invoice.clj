(ns yahont.modules.register.db.schema.invoice
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.register.db.schema.invoice.row.scope :refer [scope conversion-pk]]
            [yahont.modules.register.db.schema.invoice.row.conversion :refer [conversion]]
            [yahont.modules.register.db.schema.invoice.row.mapping :refer [mapping]]
            [yahont.syntax.conditional :refer [if-let*]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (when id (conversion-pk id)))
