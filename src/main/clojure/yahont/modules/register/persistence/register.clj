(ns yahont.modules.register.persistence.register
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.register :refer [prepare prepare-pk]]
            [yahont.modules.register.models.register :as register]
            [yahont.modules.register.provision.register :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.persistence.invoice :as invoice]
            [yahont.modules.register.provision.invoice :as provision-invoice]
            [yahont.modules.register.persistence.document :as document]
            [yahont.modules.register.provision.document :as provision-document]
            [yahont.modules.versions.register.persistence.register :as version]
            [yahont.modules.versions.register.provision.register :as provision-version]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]
            [yahont.modules.dictionaries.register.provision.mode :as provision-mode]))

(defn case-mode [code]
  (provision-mode/pk (provision-mode/key-entry (if (empty? code)
                                                 (mode/select-test)
                                                 (mode/select-production)))))

(defn create [row-map]
  (let [code {provision/code row-map}
        version-current (provision-version/pk (provision-version/key-entry (version/select-current)))
        row-map (assoc (prepare row-map) provision/version version-current provision/mode (case-mode code))]
    (entry->map models/register (register/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/register (register/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/register (register/select-row-by-id id))))

(defn select-all []
  (entries->map models/register (register/select-all-rows)))

(defn select-where [condition]
  (entries->map models/register (-> (select :*)
                                (from (keyword provision/register))
                                (where condition)
                                register/select-rows)))

(defn recursively-select-by-id [id]
  (when-let [reg (provision/key-entry (select-by-id id))]
    (let [invoice (invoice/select-by-id (provision/invoice reg))
          documents (document/recursively-select-where [:= provision-document/register (provision/pk reg)])]
      (merge reg invoice documents))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (register/delete-row-by-id id)))

(defn recursively-delete-by-id [id]
  (when-let [reg (provision/key-entry (select-by-id id))]
    (invoice/delete-where [:= provision-invoice/pk (provision/invoice reg)])
    (document/recursively-delete-where [:= provision-document/register (provision/pk reg)])
    (delete-by-id id)))
