(ns yahont.modules.register.persistence.event
  (:require [honeysql.helpers :refer :all]
            [yahont.syntax.map :refer [merge-in]]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.event :refer [prepare prepare-pk]]
            [yahont.modules.register.models.event :as event]
            [yahont.modules.register.provision.event :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.persistence.service :as service]
            [yahont.modules.register.provision.service :as provision-service]
            [yahont.modules.register.persistence.failure_cause :as failure-cause]
            [yahont.modules.register.provision.failure_cause :as provision-failure-cause]
            [yahont.modules.register.persistence.sanction :as sanction]
            [yahont.modules.register.provision.sanction :as provision-sanction]
            [yahont.modules.dictionaries.register.persistence.department :as department]
            [yahont.modules.dictionaries.register.provision.department :as provision-department]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/event (event/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/event (event/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/event (event/select-row-by-id id))))

(defn select-all []
  (entries->map models/event (event/select-all-rows)))

(defn select-where [condition]
  (entries->map models/event (-> (select :*)
                                 (from (keyword provision/event))
                                 (where condition)
                                 event/select-rows)))

(defn select-fk-department-by-code [code]
  {provision/department (provision-department/pk (provision-department/key-entry (department/select-by-code code)))})

(defn recursively-select-by-id [id]
  (let [evn (select-by-id id)
        fc (provision/failure-cause (provision/key-entry evn))
        sn (provision/sanction (provision/key-entry evn))]
    (if (provision/key-entry evn)
      (merge-in evn [provision/key-entry]
                (failure-cause/select-by-id (provision/failure-cause (provision/key-entry evn)))
                (sanction/select-by-id (provision/sanction (provision/key-entry evn)))
                (service/select-where [:= provision-service/event (provision/pk (provision/key-entry evn))]))
      evn)))

(defn recursively-select-where [condition]
  (let [events (provision/key-entries (select-where condition))]
    {provision/key-entries (map #(recursively-select-by-id (provision/pk (provision/key-entry %))) events)}))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (event/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/event))
      (where condition)
      event/delete-rows))

(defn recursively-delete-where [condition]
  (doseq [evn (provision/key-entries (select-where condition))]
    (failure-cause/delete-where [:= provision-failure-cause/pk (provision/failure-cause (provision/key-entry evn))])
    (sanction/delete-where [:= provision-sanction/pk (provision/sanction (provision/key-entry evn))])
    (service/delete-where [:= provision-service/event (provision/pk (provision/key-entry evn))]))
  (delete-where condition))
