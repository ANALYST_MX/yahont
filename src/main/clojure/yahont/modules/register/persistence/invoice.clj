(ns yahont.modules.register.persistence.invoice
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.invoice :refer [prepare prepare-pk]]
            [yahont.modules.register.models.invoice :as invoice]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision-subdivision]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]
            [yahont.modules.dictionaries.register.provision.payer :as provision-payer]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/invoice (invoice/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/invoice (invoice/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/invoice (invoice/select-row-by-id id))))

(defn select-all []
  (entries->map models/invoice (invoice/select-all-rows)))

(defn select-fk-subdivision-by-code [code]
  {provision/subdivision (provision-subdivision/pk (provision-subdivision/key-entry (subdivision/select-by-code code)))})

(defn select-fk-payer-by-code [code]
  {provision/payer (provision-payer/pk (provision-payer/key-entry (payer/select-by-code code)))})

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (invoice/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/invoice))
      (where condition)
      invoice/delete-rows))
