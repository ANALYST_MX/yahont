(ns yahont.modules.register.persistence.invoice.all
  (:require [honeysql.helpers :refer :all]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.modules.register.models.invoice :as invoice]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.db.marshal :refer [entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.dictionaries.register.provision.payer :as provision-payer]))

(defn select-all []
  (entries->map models/invoice (-> (select (keyword (full-name provision/invoice "*"))
                                           [(keyword (full-name provision-payer/payer provision-payer/name)) "invoice_name"])
                                   (from (keyword provision/invoice)
                                         (keyword provision-payer/payer))
                                   (where [:=
                                           (keyword (full-name provision/invoice provision/payer))
                                           (keyword (full-name provision-payer/payer provision-payer/pk))])
                                   invoice/select-rows)))
