(ns yahont.modules.register.persistence.invoice.show
  (:require [honeysql.helpers :refer :all]
            [yahont.modules.register.db.schema.invoice :refer [prepare-pk]]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.modules.register.models.invoice :as invoice]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.db.marshal :refer [entry->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.dictionaries.register.provision.payer :as provision-payer]))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/invoice (-> (select (keyword (full-name provision/invoice "*"))
                                         [(keyword (full-name provision-payer/payer provision-payer/name)) "invoice_name"])
                                   (from (keyword provision/invoice)
                                         (keyword provision-payer/payer))
                                 (where [:and
                                         [:=
                                          (keyword (full-name provision/invoice provision/payer))
                                          (keyword (full-name provision-payer/payer provision-payer/pk))]
                                         [:=
                                          (keyword (full-name provision/invoice provision/pk))
                                          id]])
                                 invoice/select-rows))))
