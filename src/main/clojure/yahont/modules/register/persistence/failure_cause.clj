(ns yahont.modules.register.persistence.failure_cause
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.failure_cause :refer [prepare prepare-pk]]
            [yahont.modules.register.models.failure_cause :as failure-cause]
            [yahont.modules.register.provision.failure_cause :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/failure-cause (failure-cause/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/failure-cause (failure-cause/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/failure-cause (if id (failure-cause/select-row-by-id id)))))

(defn select-all []
  (entries->map models/failure-cause (failure-cause/select-all-rows)))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (failure-cause/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/failure-cause))
      (where condition)
      failure-cause/delete-rows))
