(ns yahont.modules.register.persistence.record
  (:require [honeysql.helpers :refer :all]
            [yahont.syntax.map :refer [merge-in]]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.record :refer [prepare prepare-pk]]
            [yahont.modules.register.models.record :as record]
            [yahont.modules.register.provision.record :as provision]
            [yahont.modules.register.persistence.event :as event]
            [yahont.modules.register.provision.event :as provision-event]
            [yahont.modules.register.persistence.patient :as patient]
            [yahont.modules.register.provision.patient :as provision-patient]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/record (record/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/record (record/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/record (record/select-row-by-id id))))

(defn select-where [condition]
  (entries->map models/record (-> (select :*)
                                  (from (keyword provision/record))
                                  (where condition)
                                  record/select-rows)))
  
(defn select-all []
  (entries->map models/record (record/select-all-rows)))

(defn recursively-select-by-id [id]
  (let [rec (select-by-id id)
        id (provision/pk (provision/key-entry rec))]
    (if (provision/key-entry rec)
      (merge-in rec [provision/key-entry]
                (first (provision-event/key-entries (event/recursively-select-where [:= provision-patient/record id])))
                (first (provision-patient/key-entries (patient/select-where [:= provision-patient/record id]))))
      rec)))

(defn recursively-select-where [condition]
  (let [records (provision/key-entries (select-where condition))]
    {provision/key-entries (map #(recursively-select-by-id (provision/pk (provision/key-entry %))) records)}))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (record/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/record))
      (where condition)
      record/delete-rows))

(defn recursively-delete-where [condition]
  (doseq [rec (provision/key-entries (select-where condition))]
    (event/recursively-delete-where [:= provision-event/record (provision/pk (provision/key-entry rec))])
    (patient/recursively-delete-where [:= provision-patient/record (provision/pk (provision/key-entry rec))]))
  (delete-where condition))
