(ns yahont.modules.register.persistence.sanction
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.sanction :refer [prepare prepare-pk]]
            [yahont.modules.register.models.sanction :as sanction]
            [yahont.modules.register.provision.sanction :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/sanction (sanction/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/sanction (sanction/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/sanction (if id (sanction/select-row-by-id id)))))

(defn select-all []
  (entries->map models/sanction (sanction/select-all-rows)))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (sanction/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/sanction))
      (where condition)
      sanction/delete-rows))
