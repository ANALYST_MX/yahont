(ns yahont.modules.register.persistence.service
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.service :refer [prepare prepare-pk]]
            [yahont.modules.register.models.service :as service]
            [yahont.modules.register.provision.service :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/service (service/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/service (service/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/service (if id (service/select-row-by-id id)))))

(defn select-all []
  (entries->map models/service (service/select-all-rows)))

(defn select-where [condition]
  (entries->map models/service (-> (select :*)
                                   (from (keyword provision/service))
                                   (where condition)
                                   service/select-rows)))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (service/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/service))
      (where condition)
      service/delete-rows))
