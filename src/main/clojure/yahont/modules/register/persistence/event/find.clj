(ns yahont.modules.register.persistence.event.find
  (:require [honeysql.helpers :refer :all]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.utils.convert :as convert]
            [yahont.modules.register.models.event :as event]
            [yahont.modules.register.provision.event :as provision]
            [yahont.db.marshal :refer [entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.service :as provision-service]
            [yahont.modules.register.provision.invoice :as provision-invoice]
            [yahont.modules.register.provision.register :as provision-register]
            [yahont.modules.register.provision.document :as provision-document]
            [yahont.modules.register.provision.record :as provision-record]
            [yahont.modules.dictionaries.register.provision.payer :as provision-payer]
            [yahont.modules.dictionaries.register.provision.department :as provision-department]))

(defn select-where [condition]
  (entries->map models/event (-> (select (keyword (full-name provision/event "*"))
                                         [(keyword (full-name provision-invoice/invoice provision-invoice/date)) "invoice_date"]
                                         [(keyword (full-name provision-register/register provision-register/code)) "register_code"]
                                         [(keyword (full-name provision-payer/payer provision-payer/name)) "payer_name"]
                                         [(keyword (full-name provision-department/department provision-department/name)) "department_name"]
                                         )
                                 (from (keyword provision/event)
                                       (keyword provision-invoice/invoice)
                                       (keyword provision-register/register)
                                       (keyword provision-payer/payer)
                                       (keyword provision-department/department))
                                 (where [:and
                                         [:=
                                          (keyword (full-name provision-register/register provision-register/pk))
                                          (-> (select (keyword (full-name provision-document/document provision-document/register)))
                                              (from (keyword provision-document/document))
                                              (where [:=
                                                      (keyword (full-name provision-document/document provision-document/pk))
                                                      (-> (select (keyword (full-name provision-record/record provision-record/document)))
                                                          (from (keyword provision-record/record))
                                                          (where [:=
                                                                  (keyword (full-name provision-record/record provision-record/pk))
                                                                  (keyword (full-name provision/event provision/record))]))]))]
                                         [:=
                                          (keyword (full-name provision-register/register provision-register/invoice))
                                          (keyword (full-name provision-invoice/invoice provision-invoice/pk))]
                                         [:=
                                          (keyword (full-name provision-invoice/invoice provision-invoice/payer))
                                          (keyword (full-name provision-payer/payer provision-payer/pk))]
                                         [:=
                                          (keyword (full-name provision/event provision/department))
                                          (keyword (full-name provision-department/department provision-department/pk))]
                                         condition])
                                 event/select-rows)))
