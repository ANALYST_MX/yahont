(ns yahont.modules.register.persistence.document
  (:require [honeysql.helpers :refer :all]
            [yahont.syntax.map :refer [merge-in]]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.document :refer [prepare prepare-pk]]
            [yahont.modules.register.models.document :as document]
            [yahont.modules.register.provision.document :as provision]
            [yahont.modules.register.persistence.person :as person]
            [yahont.modules.register.provision.person :as provision-person]
            [yahont.modules.register.persistence.record :as record]
            [yahont.modules.register.provision.record :as provision-record]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/document (document/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/document (document/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/document (document/select-row-by-id id))))

(defn select-where [condition]
  (entries->map models/document (-> (select :*)
                                    (from (keyword provision/document))
                                    (where condition)
                                    document/select-rows)))

(defn select-all []
  (entries->map models/document (document/select-all-rows)))

(defn recursively-select-by-id [id]
  (let [doc (select-by-id id)
        id (provision/pk (provision/key-entry doc))]
    (if (provision/key-entry doc)
      (merge-in doc [provision/key-entry]
                                     (case (clojure.string/upper-case (first (provision/name (provision/key-entry doc))))
                                       "H" (record/recursively-select-where [:= provision-record/document id])
                                       "L" (person/select-where [:= provision-person/document id])))
      doc)))

(defn recursively-select-where [condition]
  (let [documents (provision/key-entries (select-where condition))]
    {provision/key-entries (map #(recursively-select-by-id (provision/pk (provision/key-entry %))) documents)}))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (document/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/document))
      (where condition)
      document/delete-rows))

(defn recursively-delete-where [condition]
  (doseq [doc (provision/key-entries (select-where condition))]
    (person/delete-where [:= provision-person/document (provision/pk (provision/key-entry doc))])
    (record/recursively-delete-where [:= provision-record/document (provision/pk (provision/key-entry doc))]))
  (delete-where condition))
