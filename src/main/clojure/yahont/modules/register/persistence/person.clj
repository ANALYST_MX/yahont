(ns yahont.modules.register.persistence.person
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.person :refer [prepare prepare-pk]]
            [yahont.modules.register.models.person :as person]
            [yahont.modules.register.provision.person :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/person (person/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/person (person/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/person (person/select-row-by-id id))))

(defn select-all []
  (entries->map models/person (person/select-all-rows)))

(defn select-where [condition]
  (entries->map models/person (-> (select :*)
                                  (from (keyword provision/person))
                                  (where condition)
                                  person/select-rows)))

(defn select-by-document-and-code [document code]
  (select-where [:and
                 [:= provision/code code]
                 [:= provision/document document]]))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (person/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/person))
      (where condition)
      person/delete-rows))
