(ns yahont.modules.register.persistence.patient
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.register.db.schema.patient :refer [prepare prepare-pk]]
            [yahont.modules.register.models.patient :as patient]
            [yahont.modules.register.provision.patient :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.persistence.person :as person]
            [yahont.modules.register.provision.person :as provision-person]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/patient (patient/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/patient (patient/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/patient (patient/select-row-by-id id))))

(defn select-all []
  (entries->map models/patient (patient/select-all-rows)))

(defn select-fk-person-by-document-and-code [document code]
  (let [per (first (provision-person/key-entries (person/select-by-document-and-code document code)))]
    {provision/person (provision-person/pk (provision-person/key-entry per))}))

(defn select-where [condition]
  (entries->map models/patient (-> (select :*)
                                   (from (keyword provision/patient))
                                   (where condition)
                                   patient/select-rows)))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (patient/delete-row-by-id id)))

(defn delete-where [condition]
  (-> (delete-from (keyword provision/patient))
      (where condition)
      patient/delete-rows))

(defn recursively-delete-where [condition]
  (delete-where condition))
