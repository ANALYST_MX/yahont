(ns yahont.modules.register.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :register "Реестр"
      :document "Файлы со сведениями об оказанной медицинской помощи"
      :invoice "Счет"
      :patient "Пациент"
      :event "Случай"
      :sanction "Санкция"
      :service "Услуга"
      :person "Персональные данные"
      :record "Запись"
      :failure-cause "Причина отказа"
      }
     :label
     {:home "Главная"
      :register "Реестр"
      :registers "Реестры"
      :document "Файлы со сведениями об оказанной медицинской помощи"
      :invoice "Счет"
      :invoices "Счета"
      :patient "Пациент"
      :event "Случай"
      :sanction "Санкция"
      :service "Услуга"
      :person "Персональные данные"
      :record "Запись"
      :failure-cause "Причина отказа"
      :register-upload "Загрузка реестра"
      :oplata "Оплата"
      :version "Версия"
      :submit "Отпарвить"
      :create "Создать"
      :find "Искать"
      :date "Дата"
      :start-date "С"
      :end-date "До"
      :nhistory "ИБ"
      :payer "Платильщик"
      :vid-pom "Вид помощи"
      :code "Код"
      :mode "Режим" 
      :year "Год"
      :month "Месяц"
      :s-sum "Сумма"
      :s-osn "Основание"
      :s-com "Комментарий"
      :ds-1 "Диагноз"
      :name "Наименование"
      :edit "Редактировать"
      :show "Просмотреть"
      :all "Все"
      :delete "Удалить"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
