(ns yahont.modules.register.controllers.document.migration
  (:require [yahont.modules.register.views.document.migration :as view]
            [yahont.modules.register.models.document :as document]))

(defn migration-table []
  (try
    (document/create-table)
    (view/ok)
    (catch Exception e (view/error))))
