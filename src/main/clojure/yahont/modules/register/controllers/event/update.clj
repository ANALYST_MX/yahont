(ns yahont.modules.register.controllers.event.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.views.event.update :as view]
            [yahont.modules.register.persistence.event :as profile]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (profile/update-by-id id params)
    (redirect (string/replace url/register-event-show #":id" id))
    (catch Exception e (view/error))))
