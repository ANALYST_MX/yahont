(ns yahont.modules.register.controllers.event.migration
  (:require [yahont.modules.register.views.event.migration :as view]
            [yahont.modules.register.models.event :as event]))

(defn migration-table []
  (try
    (event/create-table)
    (view/ok)
    (catch Exception e (view/error))))
