(ns yahont.modules.register.controllers.event.find
  (:refer-clojure :exclude [find])
  (:require [yahont.utils.convert :as convert]
            [yahont.support.time :refer [now]]
            [yahont.db.convention.name :refer [full-name]]
            [yahont.modules.register.views.event.find :as view]
            [yahont.modules.register.provision.event :as provision]
            [yahont.modules.register.provision.register :as provision-register]
            [yahont.syntax.conditional :refer [if-not-empty]]
            [yahont.modules.register.persistence.event.find :as event]
            [yahont.syntax.binding :refer [let-try]]))

(defn index []
  (view/index))

(defn- join-conditions [& conditions]
  (if-let [conditions (filter identity conditions)]
    (if (> (count conditions) 1) (into [:and] conditions) (first conditions))))

(defn- prepare-conditions [params]
  (let [start-date (if-not-empty (:start_date params) [:>= provision/date-out (convert/->str->sql-date (:start_date params))])
        end-date (if-not-empty (:end_date params) [:<= provision/date-out (convert/->str->sql-date (:end_date params))])
        nhistory (if-not-empty (provision/nhistory params) [:= provision/nhistory (provision/nhistory params)])
        vid-pom (if-not-empty (provision/vid-pom params) [:= provision/vid-pom (convert/->str->int (provision/vid-pom params))])
        register-code (if-not-empty (:register_code params) [:= (keyword (full-name provision-register/register provision-register/code)) (:register_code params)])]
    (join-conditions start-date end-date nhistory vid-pom register-code)))

(defn find [params]
  (let-try [start-date (:start_date params)
            end-date (:end_date params)
            nhistory (provision/nhistory params)
            vid-pom (provision/vid-pom params)
            register-code (:register_code params)]
           (view/index (provision/key-entries (event/select-where (prepare-conditions params)))
                       {:start_date (if-not-empty start-date (convert/->str->date start-date))
                        :end_date (if-not-empty end-date (convert/->str->date end-date))
                        provision/nhistory (if-not-empty nhistory nhistory)
                        provision/vid-pom (if-not-empty vid-pom vid-pom)
                        :register_code (if-not-empty register-code register-code)})
           (catch Exception e (view/error))))
