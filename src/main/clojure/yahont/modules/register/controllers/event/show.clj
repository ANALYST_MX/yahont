(ns yahont.modules.register.controllers.event.show
  (:require [yahont.modules.register.views.event.show :as view]
            [yahont.modules.register.provision.event :as provision]
            [yahont.modules.register.persistence.event :as event]))

(defn index [id]
  (try
    (view/index (provision/key-entry (event/select-by-id id)))
    (catch Exception e (view/error))))
