(ns yahont.modules.register.controllers.event.edit
  (:require [yahont.url :as url]
            [yahont.modules.register.views.event.edit :as view]
            [yahont.modules.register.provision.event :as provision]
            [yahont.modules.register.persistence.event :as event]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (event/select-by-id id)))
    (catch Exception e (view/error))))
