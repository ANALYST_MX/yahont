(ns yahont.modules.register.controllers.register.all
  (:require [yahont.url :as url]
            [yahont.modules.register.views.register.all :as view]
            [yahont.modules.register.provision.register :as provision]
            [yahont.modules.register.persistence.register :as register]))

(defn index []
  (try
    (view/index (provision/key-entries (register/select-all)))
    (catch Exception e (view/error))))
