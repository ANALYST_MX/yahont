(ns yahont.modules.register.controllers.register.migration
  (:require [yahont.modules.register.views.register.migration :as view]
            [yahont.modules.register.models.register :as register]))

(defn migration-table []
  (try
    (register/create-table)
    (view/ok)
    (catch Exception e (view/error))))
