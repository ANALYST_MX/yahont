(ns yahont.modules.register.controllers.register.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.views.register.update :as view]
            [yahont.modules.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (register/update-by-id id params)
    (redirect (string/replace url/register-register-show #":id" id))
    (catch Exception e (view/error))))
