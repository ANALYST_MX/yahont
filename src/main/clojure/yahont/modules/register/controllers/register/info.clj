(ns yahont.modules.register.controllers.register.info
  (:require [yahont.url :as url]
            [yahont.modules.register.views.register.info :as view]
            [yahont.modules.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (register/recursively-delete-by-id id)
    (redirect url/register-registers)
    (catch Exception e (view/error))))
