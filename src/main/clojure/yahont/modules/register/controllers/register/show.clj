(ns yahont.modules.register.controllers.register.show
  (:require [yahont.modules.register.views.register.show :as view]
            [yahont.modules.register.provision.register :as provision]
            [yahont.modules.register.persistence.register :as register]
            [yahont.modules.register.provision.event :as provision-event]
            [yahont.modules.register.persistence.event.find :as event]
            [yahont.utils.convert :as convert]
            [yahont.db.convention.name :refer [full-name]]))

(defn index [id]
  (try
    (view/index
     (provision/key-entry (register/select-by-id id))
     (provision-event/key-entries (event/select-where [:= (keyword (full-name provision/register provision/pk)) (convert/->str->int id)])))
    (catch Exception e (view/error))))
