(ns yahont.modules.register.controllers.register.fetch
  (:require [yahont.utils.xml :as xml]
            [yahont.modules.register.views.register.fetch :as view]
            [yahont.modules.register.persistence.document :as document]
            [yahont.modules.register.provision.document :as provision-document]
            [yahont.modules.register.persistence.invoice :as invoice]
            [yahont.modules.register.provision.invoice :as provision-invoice]
            [yahont.modules.register.persistence.record :as record]
            [yahont.modules.register.provision.record :as provision-record]
            [yahont.modules.register.persistence.register :as register]
            [yahont.modules.register.provision.register :as provision-register]
            [yahont.modules.register.persistence.patient :as patient]
            [yahont.modules.register.provision.patient :as provision-patient]
            [yahont.modules.register.persistence.event :as event]
            [yahont.modules.register.provision.event :as provision-event]
            [yahont.modules.register.persistence.sanction :as sanction]
            [yahont.modules.register.provision.sanction :as provision-sanction]
            [yahont.modules.register.persistence.service :as service]
            [yahont.modules.register.provision.service :as provision-service]
            [yahont.modules.register.persistence.failure_cause :as failure-cause]
            [yahont.modules.register.provision.failure_cause :as provision-failure-cause]
            [yahont.modules.register.persistence.person :as person]
            [yahont.modules.register.provision.person :as provision-person]
            [yahont.utils.register :refer [->map]]))

(defn index []
  (view/index))

(defn create-invoice [h-map]
  (let [subdivision (invoice/select-fk-subdivision-by-code (:podr (:register h-map)))
        payer (invoice/select-fk-payer-by-code (:plat (:invoice h-map)))
        rows (:invoice h-map)]
    (pk :provision-invoice (invoice/create (merge {} rows subdivision payer)))))

(defn create-register [id h-map invoice-pk]
  (let [code (if (empty? id) {} {provision-register/code id})
        invoice {provision-register/invoice invoice-pk}
        rows (:register h-map)]
    (pk :provision-register (register/create (merge {} rows code invoice)))))

(defn create-document [register-pk m]
  (let [register {provision-document/register register-pk}
        rows (:document m)]
    (pk :provision-document (document/create (merge {} rows register)))))

(defn create-service [event-pk service-map]
  (let [event {provision-service/event event-pk}
        rows (:service service-map)]
    (service/create (merge {} rows event))))

(defn save-services [event-pk event-map]
  (doseq [service-map (:services event-map)] (create-service event-pk service-map)))

(defn create-sanction [event-map]
  (let [rows (:sanction event-map)]
    (when-not (empty? rows)
      (pk :provision-sanction (sanction/create (merge {} rows))))))

(defn create-failure-cause [event-map]
  (let [rows (:failure-cause event-map)]
    (when-not (empty? rows)
      (pk :provision-failure-cause (failure-cause/create (merge {} rows))))))

(defn create-event [record-pk record-map]
  (let [record {provision-event/record record-pk}
        rows (:event record-map)
        department (event/select-fk-department-by-code (if (empty? (:lpu_dep rows)) (:amb_dep rows) (:lpu_dep rows)))
        sanction (if (empty? (:sanction rows)) {} {provision-event/sanction (create-sanction rows)})
        failure-cause (if (empty? (:failure-cause rows)) {} {provision-event/failure-cause (create-failure-cause rows)})
        event-pk (pk :provision-event (event/create (merge {} rows record department sanction failure-cause)))]
    (save-services event-pk rows)))

(defn create-patient [l-document-pk record-pk record-map]
  (let [record {provision-patient/record record-pk}
        rows (:patient record-map)
        person (patient/select-fk-person-by-document-and-code l-document-pk (:id_pac rows))]
    (pk :provision-patient (patient/create (merge {} rows record person)))))

(defn create-record [h-document-pk l-document-pk record-map]
  (let [document {provision-record/document h-document-pk}
        rows (:record record-map)
        record-pk (pk :provision-record (record/create (merge {} rows document)))]
    (create-patient l-document-pk record-pk rows)
    (create-event record-pk rows)))
  
(defn save-records [h-document-pk l-document-pk h-map]
  (doseq [record-map (:records h-map)] (create-record h-document-pk l-document-pk record-map)))

(defn create-person [l-document-pk person-map]
  (let [document {provision-person/document l-document-pk}
        rows (:person person-map)]
    (pk :provision-person (person/create (merge {} rows document)))))

(defn save-persons [l-document-pk l-map]
  (doseq [person-map (:persons l-map)] (create-person l-document-pk person-map)))

(defn save-register [m]
  (let [id (if (empty? (:id m)) () (:id m))
        h-map (:hm m)
        l-map (:lm m)
        invoice-pk (create-invoice h-map)
        register-pk (create-register id h-map invoice-pk)
        h-document-pk (create-document register-pk h-map)
        l-document-pk (create-document register-pk l-map)]
    (save-persons l-document-pk l-map)
    (save-records h-document-pk l-document-pk h-map)))

(defn index [id]
  (let [r (register/recursively-select-by-id id)]
    ))
