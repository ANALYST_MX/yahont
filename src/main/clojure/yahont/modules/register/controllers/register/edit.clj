(ns yahont.modules.register.controllers.register.edit
  (:require [yahont.url :as url]
            [yahont.modules.register.views.register.edit :as view]
            [yahont.modules.register.provision.register :as provision]
            [yahont.modules.register.persistence.register :as register]))

(defn index [id]
  (try
    (view/index (provision/key-entry (register/select-by-id id)))
    (catch Exception e (view/error))))
