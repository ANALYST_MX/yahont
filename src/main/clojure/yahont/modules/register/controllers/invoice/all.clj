(ns yahont.modules.register.controllers.invoice.all
  (:require [yahont.url :as url]
            [yahont.modules.register.views.invoice.all :as view]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.modules.register.persistence.invoice.all :as invoice]))

(defn index []
  (try
    (view/index (provision/key-entries (invoice/select-all)))
    (catch Exception e (view/error))))
