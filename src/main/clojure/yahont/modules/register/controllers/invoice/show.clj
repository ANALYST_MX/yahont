(ns yahont.modules.register.controllers.invoice.show
  (:require [yahont.modules.register.views.invoice.show :as view]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.modules.register.persistence.invoice.show :as invoice]))

(defn index [id]
  (try
    (view/index (provision/key-entry (invoice/select-by-id id)))
    (catch Exception e (view/error))))
