(ns yahont.modules.register.controllers.invoice.migration
  (:require [yahont.modules.register.views.invoice.migration :as view]
            [yahont.modules.register.models.invoice :as invoice]))

(defn migration-table []
  (try
    (invoice/create-table)
    (view/ok)
    (catch Exception e (view/error))))
