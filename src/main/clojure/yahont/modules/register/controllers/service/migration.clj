(ns yahont.modules.register.controllers.service.migration
  (:require [yahont.modules.register.views.service.migration :as view]
            [yahont.modules.register.models.service :as service]))

(defn migration-table []
  (try
    (service/create-table)
    (view/ok)
    (catch Exception e (view/error))))
