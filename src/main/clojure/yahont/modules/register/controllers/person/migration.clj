(ns yahont.modules.register.controllers.person.migration
  (:require [yahont.modules.register.views.person.migration :as view]
            [yahont.modules.register.models.person :as person]))

(defn migration-table []
  (try
    (person/create-table)
    (view/ok)
    (catch Exception e (view/error))))
