(ns yahont.modules.register.controllers.record.migration
  (:require [yahont.modules.register.views.record.migration :as view]
            [yahont.modules.register.models.record :as record]))

(defn migration-table []
  (try
    (record/create-table)
    (view/ok)
    (catch Exception e (view/error))))
