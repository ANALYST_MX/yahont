(ns yahont.modules.register.controllers.sanction.migration
  (:require [yahont.modules.register.views.sanction.migration :as view]
            [yahont.modules.register.models.sanction :as sanction]))

(defn migration-table []
  (try
    (sanction/create-table)
    (view/ok)
    (catch Exception e (view/error))))
