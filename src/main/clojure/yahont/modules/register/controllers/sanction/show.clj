(ns yahont.modules.register.controllers.sanction.show
  (:require [yahont.modules.register.views.sanction.show :as view]
            [yahont.modules.register.provision.sanction :as provision]
            [yahont.modules.register.persistence.sanction :as sanction]))

(defn index [id]
  (try
    (view/index
     (provision/key-entry (sanction/select-by-id id)))
    (catch Exception e (view/error))))
