(ns yahont.modules.register.controllers.patient.migration
  (:require [yahont.modules.register.views.patient.migration :as view]
            [yahont.modules.register.models.patient :as patient]))

(defn migration-table []
  (try
    (patient/create-table)
    (view/ok)
    (catch Exception e (view/error))))
