(ns yahont.modules.register.controllers.home
  (:require [yahont.modules.register.views.home :as view]))

(defn index []
  (view/index))
