(ns yahont.modules.register.controllers.failure_cause.migration
  (:require [yahont.modules.register.views.failure_cause.migration :as view]
            [yahont.modules.register.models.failure_cause :as failure-cause]))

(defn migration-table []
  (try
    (failure-cause/create-table)
    (view/ok)
    (catch Exception e (view/error))))
