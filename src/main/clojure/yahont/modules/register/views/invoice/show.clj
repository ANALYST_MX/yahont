(ns yahont.modules.register.views.invoice.show
  (:require [yahont.url :as url]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.invoice :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn index [invoice]
  (common :title (t :title/invoice)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-invoice (t :label/invoice))]
                   [:li (link-to url/register-invoices (t :label/invoices))]
                   ]]
                 [:div.show
                  (label "date" (t :label/date))
                  [:br]
                  (unparse default-formatter (provision/date invoice))
                  [:br]
                  (label "payer" (t :label/payer))
                  [:br]
                  (:invoice_name invoice)
                  [:br]]]))

(defn error [] (server-error))
