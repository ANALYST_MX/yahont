(ns yahont.modules.register.views.invoice.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.invoice :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn invoice-summary [invoice]
  [:tr
   [:td (provision/pk invoice)]
   [:td (unparse default-formatter (provision/date invoice))]
   [:td (:invoice_name invoice)]
   [:td [:a {:href (string/replace url/register-invoice-show #":id" (str (provision/pk invoice)))} (t :label/show)]]])

(defn list-invoices [invoices]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "date" "payer"])
     ]]
   (into [:tbody]
         (map #(invoice-summary (provision/key-entry %)) invoices))])

(defn index [invoices]
  (common :title (t :title/invoice)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-invoice (t :label/invoice))]
                   ]]
                 [:div.all
                  (list-invoices invoices)]]))

(defn error [] (server-error))
