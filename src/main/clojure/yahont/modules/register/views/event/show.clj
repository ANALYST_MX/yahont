(ns yahont.modules.register.views.event.show
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.syntax.conditional :refer [if-not-nil]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.event :as provision]))

(defn index [event]
  (common :title (t :title/event)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-event (t :label/event))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code event)
                  [:br]
                  (label "vidpom" (t :label/vid-pom))
                  [:br]
                  (provision/vid-pom event)
                  [:br]
                  (label "oplata" (t :label/oplata))
                  [:br]
                  (provision/oplata event)
                  [:br]
                  (label "ds_1" (t :label/ds-1))
                  [:br]
                  (provision/ds-1 event)
                  [:br]
                  (if-not-nil (provision/sanction event) (link-to (string/replace url/register-sanction-show #":id" (str (provision/sanction event))) (t :label/sanction)) "")]]))

(defn error [] (server-error))
