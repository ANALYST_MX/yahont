(ns yahont.modules.register.views.event.find
  (:require [clojure.string :as string]
            [yahont.syntax.conditional :refer [no-nil-assoc]]
            [yahont.utils.convert :as convert]
            [yahont.support.time :refer [now]]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.support.time.calendar :as calendar]            
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.event :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn event-summary [event]
  [:tr
   [:td (provision/pk event)]
   [:td (provision/code event)]
   [:td (provision/nhistory event)]
   [:td (provision/vid-pom event)]
   [:td (provision/date-in event)]
   [:td (provision/date-out event)]
   [:td (provision/oplata event)]
   [:td (provision/sumv event)]
   [:td (provision/sump event)]
   [:td (:invoice_date event)]
   [:td (:department_name event)]
   [:td (:payer_name event)]
   [:td (:register_code event)]
   [:td [:a {:href (string/replace url/register-event-show #":id" (str (provision/pk event)))} (t :label/show)]]
   [:td [:a {:href (string/replace url/register-event-edit #":id" (str (provision/pk event)))} (t :label/edit)]]])

(defn list-events [events]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "nhistory" "vidpom" "date_in" "date_out" "oplata" "sumv" "sump" "date" "department" "payer" "register_code"])
     ]]
   (into [:tbody]
         (map #(event-summary (provision/key-entry %)) events))])

(defn- wrap-params [params]
  (let [date (convert/->str->date->map (now))]
    (-> params
        (no-nil-assoc :start_date (calendar/first-day-of-month (:year date) (:month date)))
        (no-nil-assoc :end_date (calendar/last-day-of-month (:year date) (:month date)))
        )))

(defn index
  ([] (index nil {}))
  ([events params]
   (let [params (wrap-params params)]
     (common :title (t :title/event)
             :body [:div {:id "section"}
                    [:div.menu
                     [:ul
                      [:li (link-to url/register-home (t :label/home))]
                      [:li (link-to url/register-event (t :label/event))]
                      ]]
                    [:div.find
                     (form-to [:post url/register-event-find]
                              (label "nhistory" (t :label/nhistory))
                              [:br]
                              (text-field provision/nhistory (provision/nhistory params))
                              [:br]
                              (label "start_date" (t :label/start-date))
                              [:br]
                              (text-field "start_date" (if (:start_date params) (unparse default-formatter (:start_date params))))
                              [:br]
                              (label "end_date" (t :label/end-date))
                              [:br]
                              (text-field "end_date" (if (:end_date params) (unparse default-formatter (:end_date params))))
                              [:br]
                              (label "vidpom" (t :label/vid-pom))
                              [:br]
                              (text-field provision/vid-pom (provision/vid-pom params))
                              [:br]
                              (label "register_code" (t :label/register))
                              [:br]
                              (text-field "register_code" (:register_code params))
                              [:br]
                              (submit-button (t :label/find)))]
                    [:div.all
                     (list-events events)]]))))
  
(defn error [] (server-error))
