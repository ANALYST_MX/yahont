(ns yahont.modules.register.views.event.update
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
