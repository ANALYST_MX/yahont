(ns yahont.modules.register.views.event.edit
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.event :as provision]))

(defn index [event]
  (common :title (t :title/event)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-event (t :label/event))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/register-event-update #":id" (str (provision/pk event)))]
                           (label "oplata" (t :label/oplata))
                           [:br]
                           (text-field (name provision/oplata) (provision/oplata event))
                           [:br]
                           (label "ds_1" (t :label/ds-1))
                           [:br]
                           (text-field (name provision/ds-1) (provision/ds-1 event))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
