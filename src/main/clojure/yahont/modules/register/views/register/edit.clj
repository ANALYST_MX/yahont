(ns yahont.modules.register.views.register.edit
  (:require [clojure.string :as string]
            [yahont.syntax.relational :refer [select-values]]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.register :as provision]
            [yahont.modules.dictionaries.register.controllers.mode.templates :as render-mode]))

(defn index [register]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-register (t :label/register))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/register-register-update #":id" (str (provision/pk register)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code register))
                           [:br]
                           (label "mode" (t :label/mode))
                           [:br]
                           [:select {:id provision/mode :name provision/mode}
                            (render-mode/select-options (provision/mode register))
                            ]
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
