(ns yahont.modules.register.views.register.all
  (:require [yahont.syntax.html.tag :refer [map-tag]]
            [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.register :as provision]))

(defn register-summary [register]
  [:tr
   [:td (provision/pk register)]
   [:td (provision/code register)]
   [:td (provision/year register)]
   [:td (provision/month register)]
   [:td [:a {:href (string/replace url/register-register-edit #":id" (str (provision/pk register)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/register-register-show #":id" (str (provision/pk register)))} (t :label/show)]]
   [:td [:a {:href (string/replace url/register-register-delete #":id" (str (provision/pk register)))} (t :label/delete)]]])

(defn list-registers [registers]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "year" "month"])
     ]]
   (into [:tbody]
         (map #(register-summary (provision/key-entry %)) registers))])

(defn index [registers]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-register (t :label/register))]
                   ]]
                 [:div.all
                  (list-registers registers)]]))

(defn error [] (server-error))
