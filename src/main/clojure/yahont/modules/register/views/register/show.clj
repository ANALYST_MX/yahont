(ns yahont.modules.register.views.register.show
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.register :as provision]
            [yahont.modules.register.provision.event :as provision-event]))

(defn group-by-oplata [events]
  (map #(get-in % [provision-event/key-entry provision-event/oplata]) events))

(defn index [register events]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-register (t :label/register))]
                   [:li (link-to url/register-registers (t :label/registers))]
                   ]]
                 [:div.show
                  (label "year" (t :label/year))
                  [:br]
                  (provision/year register)
                  [:br]
                  (label "month" (t :label/month))
                  [:br]
                  (provision/month register)
                  [:br]
                  (label "oplata" (t :label/oplata))
                  [:br]
                  (str ((juxt count frequencies) (group-by-oplata events)))
                  [:br]
                  (link-to (string/replace url/register-invoice-show #":id" (str (provision/invoice register))) (t :label/invoice))
                  [:br]]]))

(defn error [] (server-error))
