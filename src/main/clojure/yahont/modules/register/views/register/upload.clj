(ns yahont.modules.register.views.register.upload
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to file-upload label submit-button]]))

(defn index []
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-register (t :label/register))]
                   ]]
                 [:div.upload
                  (form-to {:enctype "multipart/form-data"}
                         [:post url/register-register-upload]
                         (label :file (t :label/register-upload))
                         (file-upload :file)
                         [:br]
                         (submit-button (t :label/submit)))]]))
