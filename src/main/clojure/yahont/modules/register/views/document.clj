(ns yahont.modules.register.views.document
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/document)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]]]]))
