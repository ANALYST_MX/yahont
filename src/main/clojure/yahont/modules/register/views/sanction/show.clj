(ns yahont.modules.register.views.sanction.show
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.register.provision.sanction :as provision]))

(defn index [sanction]
  (common :title (t :title/sanction)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]
                   [:li (link-to url/register-sanction (t :label/register))]
                   ]]
                 [:div.show
                  (label "s_sum" (t :label/s-sum))
                  [:br]
                  (provision/s-sum sanction)
                  [:br]
                  (label "s_osn" (t :label/s-osn))
                  [:br]
                  (provision/s-osn sanction)
                  [:br]
                  (label "s_com" (t :label/s-com))
                  [:br]
                  (provision/s-com sanction)
                  [:br]]]))

(defn error [] (server-error))
