(ns yahont.modules.register.views.failure_cause.migration
  (:require [yahont.modules.application.views.layout :refer [success server-error]]))

(defn ok [] (success))
(defn error [] (server-error))
