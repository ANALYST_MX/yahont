(ns yahont.modules.register.views.info
  (:require [yahont.url :as url]
            [yahont.env :refer [repository-url]]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/home))]]
                  [:div.info
                   (link-to repository-url (str repository-url))]]]))
