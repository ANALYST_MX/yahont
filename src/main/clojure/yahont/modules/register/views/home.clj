(ns yahont.modules.register.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/home (t :label/home))]
                   [:li (link-to url/register-register (t :label/register))]
                   [:li (link-to url/register-document (t :label/document))]
                   [:li (link-to url/register-invoice (t :label/invoice))]
                   [:li (link-to url/register-patient (t :label/patient))]
                   [:li (link-to url/register-event (t :label/event))]
                   [:li (link-to url/register-sanction (t :label/sanction))]
                   [:li (link-to url/register-service (t :label/service))]
                   [:li (link-to url/register-person (t :label/person))]
                   [:li (link-to url/register-record (t :label/record))]
                   [:li (link-to url/register-failure-cause (t :label/failure-cause))]]]]))
