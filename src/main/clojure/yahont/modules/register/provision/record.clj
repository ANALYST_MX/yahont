(ns yahont.modules.register.provision.record
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.document :as provision-document]))

(def ^:const key-entry (keyword-entry models/record))

(def ^:const key-entries (keyword-entries models/record))

(def ^:const record (naming/table modules/register models/record))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key record))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} document (naming/foreign-key provision-document/document))
(def ^{:const true, :specs [:integer "NOT NULL"]} pr-nov :pr_nov)
