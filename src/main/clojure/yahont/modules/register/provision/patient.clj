(ns yahont.modules.register.provision.patient
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.record :as provision-record]
            [yahont.modules.register.provision.person :as provision-person]))

(def ^:const key-entry (keyword-entry models/patient))

(def ^:const key-entries (keyword-entries models/patient))

(def ^:const patient (naming/table modules/register models/patient))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key patient))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} record (naming/foreign-key provision-record/record))
(def ^{:const true, :specs [:integer "NOT NULL"]} person (naming/foreign-key provision-person/person))
(def ^{:const true, :specs [:integer "NOT NULL"]} vpolis :vpolis)
(def ^{:const true, :specs [:varchar]} spolis :spolis)
(def ^{:const true, :specs [:varchar "NOT NULL"]} npolis :npolis)
(def ^{:const true, :specs [:varchar]} st-okato :st_okato)
(def ^{:const true, :specs [:varchar]} smo :smo)
(def ^{:const true, :specs [:varchar]} smo-ogrn :smo_ogrn)
(def ^{:const true, :specs [:varchar]} smo-ok :smo_ok)
(def ^{:const true, :specs [:varchar]} smo-nam :smo_nam)
(def ^{:const true, :specs [:varchar "NOT NULL"]} prin-pol :prin_pol)
(def ^{:const true, :specs [:varchar "NOT NULL"]} novor :novor)
(def ^{:const true, :specs [:integer]} vnov-d :vnov_d)
