(ns yahont.modules.register.provision.person
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.document :as provision-document]))

(def ^:const key-entry (keyword-entry models/person))

(def ^:const key-entries (keyword-entries models/person))

(def ^:const person (naming/table modules/register models/person))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key person))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} document (naming/foreign-key provision-document/document))
(def ^{:const true, :specs [:varchar "NOT NULL"]} fam :fam)
(def ^{:const true, :specs [:varchar "NOT NULL"]} im :im)
(def ^{:const true, :specs [:varchar "NOT NULL"]} ot :ot)
(def ^{:const true, :specs [:integer "NOT NULL"]} w :w)
(def ^{:const true, :specs [:date "NOT NULL"]} birthday :birthday)
(def ^{:const true, :specs [:varchar]} fam-p :fam_p)
(def ^{:const true, :specs [:varchar]} im-p :im_p)
(def ^{:const true, :specs [:varchar]} ot-p :ot_p)
(def ^{:const true, :specs [:integer]} w-p :w_p)
(def ^{:const true, :specs [:date]} birthday-p :birthday_p)
(def ^{:const true, :specs [:varchar]} mr :mr)
(def ^{:const true, :specs [:varchar]} doctype :doctype)
(def ^{:const true, :specs [:varchar]} docser :docser)
(def ^{:const true, :specs [:varchar]} docnum :docnum)
(def ^{:const true, :specs [:varchar]} snils :snils)
(def ^{:const true, :specs [:integer "NOT NULL"]} status :status)
(def ^{:const true, :specs [:varchar]} oksm :oksm)
(def ^{:const true, :specs [:varchar "NOT NULL"]} okatog :okatog)
(def ^{:const true, :specs [:varchar "NOT NULL"]} okatop :okatop)
(def ^{:const true, :specs [:integer]} zip :zip)
(def ^{:const true, :specs [:varchar]} area :area)
(def ^{:const true, :specs [:varchar]} region :region)
(def ^{:const true, :specs [:varchar]} reg-city :reg_city)
(def ^{:const true, :specs [:varchar]} item :item)
(def ^{:const true, :specs [:integer]} type-item :type_item)
(def ^{:const true, :specs [:integer]} type-ul :type_ul)
(def ^{:const true, :specs [:varchar]} street :street)
(def ^{:const true, :specs [:integer]} house :house)
(def ^{:const true, :specs [:varchar]} liter :liter)
(def ^{:const true, :specs [:varchar]} flat :flat)
(def ^{:const true, :specs [:varchar]} comentp :comentp)
(def ^{:const true, :specs [:integer]} dost :dost)
(def ^{:const true, :specs [:integer]} dost-p :dost_p)
