(ns yahont.modules.register.provision.sanction
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]))

(def ^:const key-entry (keyword-entry models/sanction))

(def ^:const key-entries (keyword-entries models/sanction))

(def ^:const sanction (naming/table modules/register models/sanction))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key sanction))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} s-sum :s_sum)
(def ^{:const true, :specs [:integer "NOT NULL"]} s-tip :s_tip)
(def ^{:const true, :specs [:integer "NOT NULL"]} s-osn :s_osn)
(def ^{:const true, :specs [:varchar]} s-com :s_com)
(def ^{:const true, :specs [:integer "NOT NULL"]} s-ist :s_ist)
