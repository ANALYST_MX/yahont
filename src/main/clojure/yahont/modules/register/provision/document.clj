(ns yahont.modules.register.provision.document
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.register :as provision-register]))

(def ^:const key-entry (keyword-entry models/document))

(def ^:const key-entries (keyword-entries models/document))

(def ^:const document (naming/table modules/register models/document))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key document))
(def ^{:const true, :specs [:integer "NOT NULL"]} register (naming/foreign-key provision-register/register))
(def ^{:const true, :specs [:date "NOT NULL"]} date :date_create)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
