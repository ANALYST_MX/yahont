(ns yahont.modules.register.provision.failure_cause
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]))

(def ^:const key-entry (keyword-entry models/failure-cause))

(def ^:const key-entries (keyword-entries models/failure-cause))

(def ^:const failure-cause (naming/table modules/register models/failure-cause))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key failure-cause))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} comments :comments)
