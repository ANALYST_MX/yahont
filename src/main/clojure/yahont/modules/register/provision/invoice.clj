(ns yahont.modules.register.provision.invoice
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.dictionaries.register.provision.payer :as provision-payer]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision-subdivision]))

(def ^:const key-entry (keyword-entry models/invoice))

(def ^:const key-entries (keyword-entries models/invoice))

(def ^:const invoice (naming/table modules/register models/invoice))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key invoice))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar]} number :number)
(def ^{:const true, :specs [:integer "NOT NULL"]} payer (naming/foreign-key provision-payer/payer))
(def ^{:const true, :specs [:integer "NOT NULL"]} subdivision (naming/foreign-key provision-subdivision/subdivision))
(def ^{:const true, :specs [:date "NOT NULL"]} date :date_issue)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} summav :summav)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} summav-r :summav_r)
(def ^{:const true, :specs ["numeric(15,2)"]} summav-f :summav_f)
(def ^{:const true, :specs ["numeric(15,2)"]} summap :summap)
(def ^{:const true, :specs ["numeric(15,2)"]} summap-r :summap_r)
(def ^{:const true, :specs ["numeric(15,2)"]} summap-f :summap_f)
(def ^{:const true, :specs ["numeric(15,2)"]} sanction-mek :sanction_mek)
(def ^{:const true, :specs ["numeric(15,2)"]} sanction-mee :sanction_mee)
(def ^{:const true, :specs ["numeric(15,2)"]} sanction-ekmp :sanction_ekmp)
(def ^{:const true, :specs [:varchar]} coments :coments)
(def ^{:const true, :specs [:varchar]} disp :disp)
