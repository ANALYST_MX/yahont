(ns yahont.modules.register.provision.register
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.invoice :as provision-invoice]
            [yahont.modules.dictionaries.register.provision.mode :as provision-mode]
            [yahont.modules.versions.register.provision.register :as provision-version]))

(def ^:const key-entry (keyword-entry models/register))

(def ^:const key-entries (keyword-entries models/register))

(def ^:const register (naming/table modules/register models/register))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key register))
(def ^{:const true, :specs [:varchar]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} invoice (naming/foreign-key provision-invoice/invoice))
(def ^{:const true, :specs [:varchar "NOT NULL"]} version-xml :version_xml)
(def ^{:const true, :specs [:integer "NOT NULL"]} year :year)
(def ^{:const true, :specs [:integer "NOT NULL"]} month :month)
(def ^{:const true, :specs [:integer "NOT NULL"]} version (naming/foreign-key provision-version/register))
(def ^{:const true, :specs [:integer "NOT NULL"]} mode (naming/foreign-key provision-mode/mode))
