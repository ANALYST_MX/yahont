(ns yahont.modules.register.provision.service
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.event :as provision-event]))

(def ^:const key-entry (keyword-entry models/service))

(def ^:const key-entries (keyword-entries models/service))

(def ^:const service (naming/table modules/register models/service))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key service))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} event (naming/foreign-key provision-event/event))
(def ^{:const true, :specs [:varchar]} vid-vme :vid_vme)
(def ^{:const true, :specs [:integer]} det :det)
(def ^{:const true, :specs [:date "NOT NULL"]} date-in :date_in)
(def ^{:const true, :specs [:date "NOT NULL"]} date-out :date_out)
(def ^{:const true, :specs [:varchar "NOT NULL"]} ds :ds)
(def ^{:const true, :specs [:varchar "NOT NULL"]} code-usl :code_usl)
(def ^{:const true, :specs ["numeric(15,2)"]} kol-usl :kol_usl)
(def ^{:const true, :specs ["numeric(15,2)"]} tarif :tarif)
(def ^{:const true, :specs ["numeric(15,2)"]} sumv-usl :sumv_usl)
(def ^{:const true, :specs [:integer]} prvs :prvs)
(def ^{:const true, :specs [:varchar]} code-md :code_md)
(def ^{:const true, :specs [:varchar]} comentu :comentu)
