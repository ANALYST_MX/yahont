(ns yahont.modules.register.provision.event
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.register.models :as models]
            [yahont.modules.register.provision.record :as provision-record]
            [yahont.modules.dictionaries.register.provision.department :as provision-department]
            [yahont.modules.register.provision.sanction :as provision-sanction]
            [yahont.modules.register.provision.failure_cause :as provision-failure-cause]))

(def ^:const key-entry (keyword-entry models/event))

(def ^:const key-entries (keyword-entries models/event))

(def ^:const event (naming/table modules/register models/event))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key event))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} record (naming/foreign-key provision-record/record))
(def ^{:const true, :specs [:integer]} department (naming/foreign-key provision-department/department))
(def ^{:const true, :specs [:integer "NOT NULL"]} usl-ok :usl_ok)
(def ^{:const true, :specs [:integer "NOT NULL"]} vid-pom :vidpom)
(def ^{:const true, :specs [:integer "NOT NULL"]} for-pom :for_pom)
(def ^{:const true, :specs [:varchar]} vid-hmp :vid_hmp)
(def ^{:const true, :specs [:integer]} metod-hmp :metod_hmp)
(def ^{:const true, :specs [:varchar]} npr-mo :npr_mo)
(def ^{:const true, :specs [:integer]} extr :extr)
(def ^{:const true, :specs [:integer]} det :det)
(def ^{:const true, :specs [:varchar "NOT NULL"]} nhistory :nhistory)
(def ^{:const true, :specs [:date "NOT NULL"]} date-in :date_in)
(def ^{:const true, :specs [:date "NOT NULL"]} date-out :date_out)
(def ^{:const true, :specs [:varchar]} ds-0 :ds_0)
(def ^{:const true, :specs [:varchar "NOT NULL"]} ds-1 :ds_1)
(def ^{:const true, :specs [:varchar]} ds-2 :ds_2)
(def ^{:const true, :specs [:varchar]} ds-3 :ds_3)
(def ^{:const true, :specs [:varchar]} code-mes-1 :code_mes_1)
(def ^{:const true, :specs [:varchar]} code-mes-2 :code_mes_2)
(def ^{:const true, :specs [:integer "NOT NULL"]} rslt :rslt)
(def ^{:const true, :specs [:integer "NOT NULL"]} ishod :ishod)
(def ^{:const true, :specs [:integer "NOT NULL"]} prvs :prvs)
(def ^{:const true, :specs [:varchar "NOT NULL"]} iddokt :iddokt)
(def ^{:const true, :specs [:integer "NOT NULL"]} idsp :idsp)
(def ^{:const true, :specs ["numeric(15,2)"]} ed-col :ed_col)
(def ^{:const true, :specs [:integer]} rean-d :rean_d)
(def ^{:const true, :specs ["numeric(15,2)"]} kskp-coef :kskp_coef)
(def ^{:const true, :specs [:integer]} kpg :kpg)
(def ^{:const true, :specs [:integer]} ksg :ksg)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} tarif-r :tarif_r)
(def ^{:const true, :specs ["numeric(15,2)"]} tarif-f :tarif_f)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} sumv :sumv)
(def ^{:const true, :specs ["numeric(15,2)" "NOT NULL"]} sumv-r :sumv_r)
(def ^{:const true, :specs ["numeric(15,2)"]} sumv-f :sumv_f)
(def ^{:const true, :specs [:integer]} oplata :oplata)
(def ^{:const true, :specs ["numeric(15,2)"]} sump :sump)
(def ^{:const true, :specs ["numeric(15,2)"]} sump-r :sump_r)
(def ^{:const true, :specs ["numeric(15,2)"]} sump-f :sump_f)
(def ^{:const true, :specs ["numeric(15,2)"]} sank-it :sank_it)
(def ^{:const true, :specs [:varchar]} comentsl :comentsl)
(def ^{:const true, :specs [:integer]} vnov-m :vnov_m)
(def ^{:const true, :specs [:integer]} sanction (naming/foreign-key provision-sanction/sanction))
(def ^{:const true, :specs [:integer]} os-sluch :os_sluch)
(def ^{:const true, :specs [:integer]} failure-cause (naming/foreign-key provision-failure-cause/failure-cause))
