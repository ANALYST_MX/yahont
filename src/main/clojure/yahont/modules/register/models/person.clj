(ns yahont.modules.register.models.person
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.person :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/person
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/document)
                  (row-specs provision/fam)
                  (row-specs provision/im)
                  (row-specs provision/ot)
                  (row-specs provision/w)
                  (row-specs provision/birthday)
                  (row-specs provision/fam-p)
                  (row-specs provision/im-p)
                  (row-specs provision/ot-p)
                  (row-specs provision/w-p)
                  (row-specs provision/birthday-p)
                  (row-specs provision/mr)
                  (row-specs provision/doctype)
                  (row-specs provision/docser)
                  (row-specs provision/docnum)
                  (row-specs provision/snils)
                  (row-specs provision/status)
                  (row-specs provision/oksm)
                  (row-specs provision/okatog)
                  (row-specs provision/okatop)
                  (row-specs provision/zip)
                  (row-specs provision/area)
                  (row-specs provision/region)
                  (row-specs provision/reg-city)
                  (row-specs provision/item)
                  (row-specs provision/type-item)
                  (row-specs provision/type-ul)
                  (row-specs provision/street)
                  (row-specs provision/house)
                  (row-specs provision/liter)
                  (row-specs provision/flat)
                  (row-specs provision/comentp)
                  (row-specs provision/dost)
                  (row-specs provision/dost-p)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/person row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/person id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/person id))

(defn select-all-rows []
  (select-all provision/person))

(defn select-rows [sql]
  (select (sql/format sql)))

(defn delete-row-by-id [id]
  (delete-by-id provision/person id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
