(ns yahont.modules.register.models.patient
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.patient :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/patient
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/record)
                  (row-specs provision/person)
                  (row-specs provision/vpolis)
                  (row-specs provision/spolis)
                  (row-specs provision/npolis)
                  (row-specs provision/st-okato)
                  (row-specs provision/smo)
                  (row-specs provision/smo-ogrn)
                  (row-specs provision/smo-ok)
                  (row-specs provision/smo-nam)
                  (row-specs provision/prin-pol)
                  (row-specs provision/novor)
                  (row-specs provision/vnov-d)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/patient row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/patient id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/patient id))

(defn select-all-rows []
  (select-all provision/patient))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn delete-row-by-id [id]
  (delete-by-id provision/patient id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
