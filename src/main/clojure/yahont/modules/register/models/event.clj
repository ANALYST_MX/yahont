(ns yahont.modules.register.models.event
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.event :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/event
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/record)
                  (row-specs provision/department)
                  (row-specs provision/usl-ok)
                  (row-specs provision/vid-pom)
                  (row-specs provision/for-pom)
                  (row-specs provision/vid-hmp)
                  (row-specs provision/metod-hmp)
                  (row-specs provision/npr-mo)
                  (row-specs provision/extr)
                  (row-specs provision/det)
                  (row-specs provision/nhistory)
                  (row-specs provision/date-in)
                  (row-specs provision/date-out)
                  (row-specs provision/ds-0)
                  (row-specs provision/ds-1)
                  (row-specs provision/ds-2)
                  (row-specs provision/ds-3)
                  (row-specs provision/code-mes-1)
                  (row-specs provision/code-mes-2)
                  (row-specs provision/rslt)
                  (row-specs provision/ishod)
                  (row-specs provision/prvs)
                  (row-specs provision/iddokt)
                  (row-specs provision/idsp)
                  (row-specs provision/ed-col)
                  (row-specs provision/rean-d)
                  (row-specs provision/kskp-coef)
                  (row-specs provision/kpg)
                  (row-specs provision/ksg)
                  (row-specs provision/tarif-r)
                  (row-specs provision/tarif-f)
                  (row-specs provision/sumv)
                  (row-specs provision/sumv-r)
                  (row-specs provision/sumv-f)
                  (row-specs provision/oplata)
                  (row-specs provision/sump)
                  (row-specs provision/sump-r)
                  (row-specs provision/sump-f)
                  (row-specs provision/sank-it)
                  (row-specs provision/comentsl)
                  (row-specs provision/vnov-m)
                  (row-specs provision/sanction)
                  (row-specs provision/os-sluch)
                  (row-specs provision/failure-cause)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/event row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/event id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/event id))

(defn select-all-rows []
  (select-all provision/event))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn delete-row-by-id [id]
  (delete-by-id provision/event id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
