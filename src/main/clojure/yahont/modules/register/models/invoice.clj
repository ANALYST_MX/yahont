(ns yahont.modules.register.models.invoice
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.invoice :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/invoice
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/number)
                  (row-specs provision/payer)
                  (row-specs provision/subdivision)
                  (row-specs provision/date)
                  (row-specs provision/summav)
                  (row-specs provision/summav-r)
                  (row-specs provision/summav-f)
                  (row-specs provision/summap)
                  (row-specs provision/summap-r)
                  (row-specs provision/summap-f)
                  (row-specs provision/sanction-mek)
                  (row-specs provision/sanction-mee)
                  (row-specs provision/sanction-ekmp)
                  (row-specs provision/coments)
                  (row-specs provision/disp)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/invoice row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/invoice id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/invoice id))

(defn select-all-rows []
  (select-all provision/invoice))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn delete-row-by-id [id]
  (delete-by-id provision/invoice id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
