(ns yahont.modules.register.models.record
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.record :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/record
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/document)
                  (row-specs provision/pr-nov)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/record row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/record id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/record id))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn select-all-rows []
  (select-all provision/record))

(defn delete-row-by-id [id]
  (delete-by-id provision/record id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
