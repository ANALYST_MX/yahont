(ns yahont.modules.register.models.register
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.register :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/register
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/invoice)
                  (row-specs provision/version-xml)
                  (row-specs provision/year)
                  (row-specs provision/month)
                  (row-specs provision/version)
                  (row-specs provision/mode)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/register row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/register id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/register id))

(defn select-all-rows []
  (select-all provision/register))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn delete-row-by-id [id]
  (delete-by-id provision/register id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
