(ns yahont.modules.register.models.service
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.register.provision.service :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id delete]]))

(defn create-table []
  (migrate/create provision/service
                  (row-specs provision/code)
                  (row-specs provision/event)
                  (row-specs provision/vid-vme)
                  (row-specs provision/det)
                  (row-specs provision/date-in)
                  (row-specs provision/date-out)
                  (row-specs provision/ds)
                  (row-specs provision/code-usl)
                  (row-specs provision/kol-usl)
                  (row-specs provision/tarif)
                  (row-specs provision/sumv-usl)
                  (row-specs provision/prvs)
                  (row-specs provision/code-md)
                  (row-specs provision/comentu)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/service row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/service id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/service id))

(defn select-all-rows []
  (select-all provision/service))

(defn select-rows [sql-map]
  (select (sql/format sql-map)))

(defn delete-row-by-id [id]
  (delete-by-id provision/service id))

(defn delete-rows [sql-map]
  (delete (sql/format sql-map)))
