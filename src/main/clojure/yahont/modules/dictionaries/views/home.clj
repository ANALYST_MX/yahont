(ns yahont.modules.dictionaries.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/home (t :label/home))]
                   [:li (link-to url/dictionaries-register-home (t :label/register))]
                   [:li (link-to url/dictionaries-common-home (t :label/common))]
                   [:li (link-to url/dictionaries-funding-home (t :label/funding))]]]]))
