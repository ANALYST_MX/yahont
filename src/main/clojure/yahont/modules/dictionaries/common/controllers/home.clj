(ns yahont.modules.dictionaries.common.controllers.home
  (:require [yahont.modules.dictionaries.common.views.home :as view]))

(defn index []
  (view/index))
