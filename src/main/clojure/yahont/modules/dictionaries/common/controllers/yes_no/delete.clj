(ns yahont.modules.dictionaries.common.controllers.yes_no.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.common.views.yes_no.delete :as view]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (yes-no/delete-by-id id)
    (redirect url/dictionaries-common-yes-no-all)
    (catch Exception e (view/error))))
