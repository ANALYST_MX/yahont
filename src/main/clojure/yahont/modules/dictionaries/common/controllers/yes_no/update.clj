(ns yahont.modules.dictionaries.common.controllers.yes_no.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.common.views.yes_no.update :as view]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (yes-no/update-by-id id params)
    (redirect (string/replace url/dictionaries-common-yes-no-show #":id" id))
    (catch Exception e (view/error))))
