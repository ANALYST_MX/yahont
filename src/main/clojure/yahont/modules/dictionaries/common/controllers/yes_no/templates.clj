(ns yahont.modules.dictionaries.common.controllers.yes_no.templates
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.common.views.yes_no.all :as view]
            [yahont.modules.dictionaries.common.views.yes_no.templates.select :as select]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]))

(defn select-options
  ([] (select-options nil))
  ([selected]
   (try
     (select/options (provision/key-entries (yes-no/select-all)) selected)
     (catch Exception e (view/error)))))
