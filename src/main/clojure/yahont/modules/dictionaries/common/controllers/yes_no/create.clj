(ns yahont.modules.dictionaries.common.controllers.yes_no.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.common.views.yes_no.create :as view]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (yes-no/create params)))]
      (redirect (string/replace url/dictionaries-common-yes-no-show #":id" (str id))))
    (catch Exception e (view/error))))
