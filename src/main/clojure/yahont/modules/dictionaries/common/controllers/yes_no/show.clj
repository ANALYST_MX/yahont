(ns yahont.modules.dictionaries.common.controllers.yes_no.show
  (:require [yahont.modules.dictionaries.common.views.yes_no.show :as view]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]))

(defn index [id]
  (try
    (view/index (provision/key-entry (yes-no/select-by-id id)))
    (catch Exception e (view/error))))
