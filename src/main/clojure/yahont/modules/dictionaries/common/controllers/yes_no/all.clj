(ns yahont.modules.dictionaries.common.controllers.yes_no.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.common.views.yes_no.all :as view]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]
            [yahont.modules.dictionaries.common.persistence.yes_no :as yes-no]))

(defn index []
  (try
    (view/index (provision/key-entries (yes-no/select-all)))
    (catch Exception e (view/error))))
