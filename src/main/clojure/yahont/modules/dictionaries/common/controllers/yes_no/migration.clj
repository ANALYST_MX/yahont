(ns yahont.modules.dictionaries.common.controllers.yes_no.migration
  (:require [yahont.modules.dictionaries.common.views.yes_no.migration :as view]
            [yahont.modules.dictionaries.common.models.yes_no :as yes-no]))

(defn migration-table []
  (try
    (yes-no/create-table)
    (view/ok)
    (catch Exception e (view/error))))
