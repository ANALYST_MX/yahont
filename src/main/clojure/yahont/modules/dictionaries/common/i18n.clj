(ns yahont.modules.dictionaries.common.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :yes-no "Да/Нет"
      }
     :label
     {:home "Главная"
      :yes-no "Да/Нет"
      :yes-no-all "Все"
      :create "Создать"
      :start-date "С"
      :end-date "До"
      :code "Код"
      :name "Наименование"
      :edit "Редактировать"
      :all "Все"
      :delete "Удалить"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
