(ns yahont.modules.dictionaries.common.db.schema.yes_no.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.dictionaries.common.provision.yes_no])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code name])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   name convert/->str})

(def mapping
  {})
