(ns yahont.modules.dictionaries.common.db.schema.yes_no
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.dictionaries.common.db.schema.yes_no.row.scope :refer [scope conversion-pk]]
            [yahont.modules.dictionaries.common.db.schema.yes_no.row.conversion :refer [conversion]]
            [yahont.modules.dictionaries.common.db.schema.yes_no.row.mapping :refer [mapping]]
            [yahont.syntax.conditional :refer [if-let*]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (conversion-pk id))
