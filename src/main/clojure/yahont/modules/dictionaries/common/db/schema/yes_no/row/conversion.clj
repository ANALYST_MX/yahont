(ns yahont.modules.dictionaries.common.db.schema.yes_no.row.conversion
  (:require [yahont.syntax.conditional :refer [if-let*]]
            [yahont.modules.dictionaries.common.db.schema.yes_no.row.scope :as scope]))

(defn- mapping [object]
  (if-let* [[k v] object
            f (k scope/conversion)]
           (conj {k v} {k (f v)})))

(defn conversion [row-map]
  (into {} (map mapping row-map)))
