(ns yahont.modules.dictionaries.common.db.schema.yes_no.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.dictionaries.common.db.schema.yes_no.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
