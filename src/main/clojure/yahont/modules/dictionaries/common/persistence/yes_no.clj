(ns yahont.modules.dictionaries.common.persistence.yes_no
  (:require [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.common.db.schema.yes_no :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.common.models.yes_no :as yes-no]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.common.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/yes-no (yes-no/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/yes-no (yes-no/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/yes-no (yes-no/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (yes-no/delete-row-by-id id)))

(defn select-all []
  (entries->map models/yes-no (yes-no/select-all-rows)))
