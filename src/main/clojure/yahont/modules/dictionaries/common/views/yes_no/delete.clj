(ns yahont.modules.dictionaries.common.views.yes_no.delete
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
