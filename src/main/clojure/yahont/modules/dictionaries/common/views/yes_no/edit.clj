(ns yahont.modules.dictionaries.common.views.yes_no.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))

(defn index [yes-no]
  (common :title (t :title/yes-no)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-common-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no (t :label/yes-no))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-common-yes-no-update #":id" (str (provision/pk yes-no)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code yes-no))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name yes-no))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
