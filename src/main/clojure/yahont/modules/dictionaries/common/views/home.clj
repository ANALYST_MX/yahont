(ns yahont.modules.dictionaries.common.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no (t :label/yes-no))]]]]))
