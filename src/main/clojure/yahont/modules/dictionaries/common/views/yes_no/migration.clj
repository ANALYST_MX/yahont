(ns yahont.modules.dictionaries.common.views.yes_no.migration
  (:require [yahont.modules.application.views.layout :refer [success server-error]]))

(defn ok [] (success))
(defn error [] (server-error))
