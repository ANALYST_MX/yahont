(ns yahont.modules.dictionaries.common.views.yes_no.create
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]))

(defn index []
  (common :title (t :title/yes-no)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-common-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no (t :label/yes-no))]
                   ]]
                 [:div.create
                  (form-to [:post url/dictionaries-common-yes-no-create]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
