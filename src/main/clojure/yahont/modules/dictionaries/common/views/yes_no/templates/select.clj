(ns yahont.modules.dictionaries.common.views.yes_no.templates.select
  (:require [yahont.syntax.relational :refer [select-values]]
            [hiccup.form :refer [select-options]]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]))

(defmacro options [yes-no-all selected]
  (let [entry (symbol (name provision/key-entry))]
    `(select-options
      (map (fn [{:keys [~entry]}] (select-values ~entry [~provision/pk ~provision/name])) ~yes-no-all) ~selected)))
