(ns yahont.modules.dictionaries.common.views.yes_no
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/yes-no)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-common-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no-all (t :label/all))]
                   [:li (link-to url/dictionaries-common-yes-no-create (t :label/create))]]]]))
