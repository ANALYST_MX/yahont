(ns yahont.modules.dictionaries.common.views.yes_no.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn yes-no-summary [yes-no]
  [:tr
   [:td (provision/pk yes-no)]
   [:td (provision/code yes-no)]
   [:td (provision/name yes-no)]
   [:td [:a {:href (string/replace url/dictionaries-common-yes-no-edit #":id" (str (provision/pk yes-no)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-common-yes-no-delete #":id" (str (provision/pk yes-no)))} (t :label/delete)]]])

(defn list-yes-no [yes-nos]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(yes-no-summary (provision/key-entry %)) yes-nos))])

(defn index [yes-no-all]
  (common :title (t :title/yes-no)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-common-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no (t :label/yes-no))]
                   ]]
                 [:div.all
                  (list-yes-no yes-no-all)]]))

(defn error [] (server-error))
