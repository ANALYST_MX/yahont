(ns yahont.modules.dictionaries.common.views.yes_no.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.common.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision]))

(defn index [yes-no]
  (common :title (t :title/yes-no)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-common-home (t :label/home))]
                   [:li (link-to url/dictionaries-common-yes-no (t :label/yes-no))]
                   [:li (link-to url/dictionaries-common-yes-no-all (t :label/yes-no-all))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code yes-no)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name yes-no)
                  [:br]]]))

(defn error [] (server-error))
