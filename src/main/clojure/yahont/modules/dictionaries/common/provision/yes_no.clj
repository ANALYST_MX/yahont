(ns yahont.modules.dictionaries.common.provision.yes_no
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.common.models :as models]))

(def ^:const key-entry (keyword-entry models/yes-no))

(def ^:const key-entries (keyword-entries models/yes-no))

(def ^:const yes-no (naming/table modules/dictionaries submodules/common models/yes-no))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key yes-no))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
