(ns yahont.modules.dictionaries.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      }
     :label
     {:home "Главная"
      :register "Реестр"
      :common "Общее"
      :funding "Финансирование"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
