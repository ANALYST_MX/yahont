(ns yahont.modules.dictionaries.funding.persistence.source
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.funding.db.schema.source :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.funding.models.source :as source]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.funding.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/source (source/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/source (source/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/source (source/select-row-by-id id))))

(defn select-all []
  (entries->map models/source (source/select-all-rows)))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (source/delete-row-by-id id)))
