(ns yahont.modules.dictionaries.funding.controllers.source.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.delete :as view]
            [yahont.modules.dictionaries.funding.persistence.source :as source]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (source/delete-by-id id)
    (redirect url/dictionaries-funding-source)
    (catch Exception e (view/error))))
