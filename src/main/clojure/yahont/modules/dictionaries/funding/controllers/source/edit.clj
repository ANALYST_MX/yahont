(ns yahont.modules.dictionaries.funding.controllers.source.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.edit :as view]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.modules.dictionaries.funding.persistence.source :as source]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (source/select-by-id id)))
    (catch Exception e (view/error))))
