(ns yahont.modules.dictionaries.funding.controllers.source.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.create :as view]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.modules.dictionaries.funding.persistence.source :as source]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (source/create params)))]
      (redirect (string/replace url/dictionaries-funding-source-show #":id" (str id))))
    (catch Exception e (view/error))))
