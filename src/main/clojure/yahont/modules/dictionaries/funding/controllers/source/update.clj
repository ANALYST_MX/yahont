(ns yahont.modules.dictionaries.funding.controllers.source.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.update :as view]
            [yahont.modules.dictionaries.funding.persistence.source :as source]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (source/update-by-id id params)
    (redirect (string/replace url/dictionaries-funding-source-show #":id" id))
    (catch Exception e (view/error))))
