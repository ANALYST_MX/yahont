(ns yahont.modules.dictionaries.funding.controllers.source.templates
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.all :as view]
            [yahont.modules.dictionaries.funding.views.source.templates.select :as select]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.modules.dictionaries.funding.persistence.source :as source]))

(defn select-options
  ([] (select-options nil))
  ([selected]
   (try
     (select/options (provision/key-entries (source/select-all)) selected)
     (catch Exception e (view/error)))))
