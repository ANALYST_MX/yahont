(ns yahont.modules.dictionaries.funding.controllers.source.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.views.source.all :as view]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.modules.dictionaries.funding.persistence.source :as source]))

(defn index []
  (try
    (view/index (provision/key-entries (source/select-all)))
    (catch Exception e (view/error))))
