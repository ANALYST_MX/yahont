(ns yahont.modules.dictionaries.funding.controllers.home
  (:require [yahont.modules.dictionaries.funding.views.home :as view]))

(defn index []
  (view/index))
