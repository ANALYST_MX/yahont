(ns yahont.modules.dictionaries.funding.controllers.source.migration
  (:require [yahont.modules.dictionaries.funding.views.source.migration :as view]
            [yahont.modules.dictionaries.funding.models.source :as source]))

(defn migration-table []
  (try
    (source/create-table)
    (view/ok)
    (catch Exception e (view/error))))
