(ns yahont.modules.dictionaries.funding.controllers.source.show
  (:require [yahont.modules.dictionaries.funding.views.source.show :as view]
            [yahont.modules.dictionaries.funding.provision.source :as provision]
            [yahont.modules.dictionaries.funding.persistence.source :as source]))

(defn index [id]
  (try
    (view/index (provision/key-entry (source/select-by-id id)))
    (catch Exception e (view/error))))
