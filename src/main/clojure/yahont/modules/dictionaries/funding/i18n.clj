(ns yahont.modules.dictionaries.funding.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :funding-source "Источник финансирования"
      }
     :label
     {:home "Главная"
      :funding-source "Источник финансирования"
      :funding-sources "Источники финансирования"
      :create "Создать"
      :start-date "С"
      :end-date "До"
      :name "Наименование"
      :description "Описание"
      :edit "Редактировать"
      :all "Все"
      :delete "Удалить"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
