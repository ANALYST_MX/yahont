(ns yahont.modules.dictionaries.funding.db.schema.source.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.dictionaries.funding.provision.source])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk name description start-date end-date])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   name convert/->str
   description convert/->str
   start-date convert/->str->timestamp
   end-date convert/->str->timestamp})

(def mapping
  {})
