(ns yahont.modules.dictionaries.funding.db.schema.source.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.dictionaries.funding.db.schema.source.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
