(ns yahont.modules.dictionaries.funding.db.schema.source
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.dictionaries.funding.db.schema.source.row.scope :refer [scope conversion-pk]]
            [yahont.modules.dictionaries.funding.db.schema.source.row.conversion :refer [conversion]]
            [yahont.modules.dictionaries.funding.db.schema.source.row.mapping :refer [mapping]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (conversion-pk id))
