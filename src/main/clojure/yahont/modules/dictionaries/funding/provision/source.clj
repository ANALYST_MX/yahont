(ns yahont.modules.dictionaries.funding.provision.source
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.funding.models :as models]))

(def ^:const key-entry (keyword-entry models/source))

(def ^:const key-entries (keyword-entries models/source))

(def ^:const source (naming/table modules/dictionaries submodules/funding models/source))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key source))
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
(def ^{:const true, :specs [:varchar]} description :description)
