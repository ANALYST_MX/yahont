(ns yahont.modules.dictionaries.funding.views.source.update
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
