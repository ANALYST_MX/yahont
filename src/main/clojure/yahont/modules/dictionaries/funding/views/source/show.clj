(ns yahont.modules.dictionaries.funding.views.source.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.funding.provision.source :as provision]))

(defn index [source]
  (common :title (t :title/funding-source)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-funding-source (t :label/funding-source))]
                   [:li (link-to url/dictionaries-funding-sources (t :label/funding-sources))]
                   ]]
                 [:div.show
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name source)
                  [:br]
                  (label "description" (t :label/description))
                  [:br]
                  (provision/description source)
                  [:br]]]))

(defn error [] (server-error))
