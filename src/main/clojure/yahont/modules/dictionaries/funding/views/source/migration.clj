(ns yahont.modules.dictionaries.funding.views.source.migration
  (:require [yahont.modules.application.views.layout :refer [success server-error]]))

(defn ok [] (success))
(defn error [] (server-error))
