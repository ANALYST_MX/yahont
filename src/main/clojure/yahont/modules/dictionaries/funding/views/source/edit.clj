(ns yahont.modules.dictionaries.funding.views.source.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.dictionaries.funding.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.funding.provision.source :as provision]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))
  
(defn index [source]
  (common :title (t :title/funding-source)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-funding-source (t :label/home))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-funding-source-update #":id" (str (:id source)))]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name source))
                           [:br]
                           (label "date_start" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date) (unparse default-formatter (provision/start-date source)))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date) (unparse default-formatter (provision/end-date source)))
                           [:br]
                           (label "description" (t :label/description))
                           [:br]
                           (text-field (name provision/description) (provision/description source))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
