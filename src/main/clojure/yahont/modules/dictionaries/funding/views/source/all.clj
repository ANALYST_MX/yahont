(ns yahont.modules.dictionaries.funding.views.source.all
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.dictionaries.funding.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.funding.persistence.source]
            [yahont.modules.dictionaries.funding.provision.source :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn source-summary [source]
  [:tr
   [:td (provision/pk source)]
   [:td (provision/name source)]
   [:td (unparse default-formatter (provision/start-date source))]
   [:td (unparse default-formatter (provision/end-date source))]
   [:td [:a {:href (string/replace url/dictionaries-funding-source-edit #":id" (str (provision/pk source)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-funding-source-delete #":id" (str (provision/pk source)))} (t :label/delete)]]])

(defn list-sources [sources]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "name" "start_date" "end_date"])
     ]]
   (into [:tbody]
         (map #(source-summary (provision/key-entry %)) sources))])

(defn index [sources]
  (common :title (t :title/funding-source)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-funding-source (t :label/home))]
                   ]]
                 [:div.all
                  (list-sources sources)]]))

(defn error [] (server-error))
