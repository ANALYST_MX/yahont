(ns yahont.modules.dictionaries.funding.views.source.templates.select
  (:require [yahont.syntax.relational :refer [select-values]]
            [hiccup.form :refer [select-options]]
            [yahont.modules.dictionaries.funding.provision.source :as provision]))

(defmacro options [sources selected]
  (let [entry (symbol (name provision/key-entry))]
    `(select-options
      (map (fn [{:keys [~entry]}] (select-values ~entry [~provision/pk ~provision/name])) ~sources) ~selected)))
