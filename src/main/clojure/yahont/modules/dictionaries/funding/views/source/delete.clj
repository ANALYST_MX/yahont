(ns yahont.modules.dictionaries.funding.views.source.delete
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
