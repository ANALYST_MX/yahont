(ns yahont.modules.dictionaries.funding.views.source.create
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.funding.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field text-area submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.dictionaries.funding.provision.source :as provision]))

(defn index []
  (common :title (t :title/funding-source)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-funding-source (t :label/home))]
                    ]]
                 [:div.create
                  (form-to [:post url/dictionaries-funding-source-create]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (label "start_date" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date))
                           [:br]
                           (label "description" (t :label/description))
                           [:br]
                           (text-area (name provision/description))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
