(ns yahont.modules.dictionaries.funding.views.source
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.funding.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-funding-home (t :label/home))]
                   [:li (link-to url/dictionaries-funding-sources (t :label/all))]
                   [:li (link-to url/dictionaries-funding-source-create (t :label/create))]]]]))
