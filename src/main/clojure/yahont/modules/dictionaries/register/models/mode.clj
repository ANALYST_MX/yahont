(ns yahont.modules.dictionaries.register.models.mode
  (:require [yahont.db.provision :refer [row-specs]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all]]
            [yahont.db.sql.row.delete :refer [delete-by-id]]))

(defn create-table []
  (migrate/create provision/mode
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/name)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/mode row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/mode id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/mode id))

(defn delete-row-by-id [id]
  (delete-by-id provision/mode id))

(defn select-all-rows []
  (select-all provision/mode))
