(ns yahont.modules.dictionaries.register.models.profile
  (:require [yahont.db.provision :refer [row-specs]]
            [yahont.modules.dictionaries.register.provision.profile :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all]]
            [yahont.db.sql.row.delete :refer [delete-by-id]]))

(defn create-table []
  (migrate/create provision/profile
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/name)
                  (row-specs provision/start-date)
                  (row-specs provision/end-date)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/profile row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/profile id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/profile id))

(defn delete-row-by-id [id]
  (delete-by-id provision/profile id))

(defn select-all-rows []
  (select-all provision/profile))
