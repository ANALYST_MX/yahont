(ns yahont.modules.dictionaries.register.models.department
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id]]))

(defn create-table []
  (migrate/create provision/department
                  (row-specs provision/pk)
                  (row-specs provision/code)
                  (row-specs provision/subdivision)
                  (row-specs provision/profile)
                  (row-specs provision/name)
                  (row-specs provision/paid)
                  (row-specs provision/start-date)
                  (row-specs provision/end-date)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/department row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/department id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/department id))

(defn delete-row-by-id [id]
  (delete-by-id provision/department id))

(defn select-all-rows []
  (select-all provision/department))

(defn select-row [sql]
  (select (sql/format sql)))
