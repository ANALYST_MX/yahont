(ns yahont.modules.dictionaries.register.models)

(def ^:const department "department")
(def ^:const institution "institution")
(def ^:const payer "payer")
(def ^:const profile "profile")
(def ^:const subdivision "subdivision")
(def ^:const mode "mode")
