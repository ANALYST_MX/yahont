(ns yahont.modules.dictionaries.register.provision.institution
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.register.models :as models]))

(def ^:const key-entry (keyword-entry models/institution))

(def ^:const key-entries (keyword-entries models/institution))

(def ^:const institution (naming/table modules/dictionaries submodules/register models/institution))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key institution))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
