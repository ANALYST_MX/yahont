(ns yahont.modules.dictionaries.register.provision.mode
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.register.models :as models]))

(def ^:const key-entry (keyword-entry models/mode))

(def ^:const key-entries (keyword-entries models/mode))

(def ^:const mode (naming/table modules/dictionaries submodules/register models/mode))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key mode))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
