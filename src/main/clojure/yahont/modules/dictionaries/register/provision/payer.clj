(ns yahont.modules.dictionaries.register.provision.payer
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.register.models :as models]))

(def ^:const key-entry (keyword-entry models/payer))

(def ^:const key-entries (keyword-entries models/payer))

(def ^:const payer (naming/table modules/dictionaries submodules/register models/payer))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key payer))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
