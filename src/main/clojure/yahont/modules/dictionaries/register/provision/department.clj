(ns yahont.modules.dictionaries.register.provision.department
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.register.models :as models]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision-subdivision]
            [yahont.modules.dictionaries.register.provision.profile :as provision-profile]
            [yahont.modules.dictionaries.common.provision.yes_no :as provision-yes-no]))

(def ^:const key-entry (keyword-entry models/department))

(def ^:const key-entries (keyword-entries models/department))

(def ^:const department (naming/table modules/dictionaries submodules/register models/department))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key department))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} subdivision (naming/foreign-key provision-subdivision/subdivision))
(def ^{:const true, :specs [:integer]} profile (naming/foreign-key provision-profile/profile))
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:integer "NOT NULL"]} paid (naming/foreign-key provision-yes-no/yes-no))
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
