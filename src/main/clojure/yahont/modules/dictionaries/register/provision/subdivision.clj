(ns yahont.modules.dictionaries.register.provision.subdivision
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.dictionaries.submodules :as submodules]
            [yahont.modules.dictionaries.register.models :as models]
            [yahont.modules.dictionaries.register.provision.institution :as provision-institution]))

(def ^:const key-entry (keyword-entry models/subdivision))

(def ^:const key-entries (keyword-entries models/subdivision))

(def ^:const subdivision (naming/table modules/dictionaries submodules/register models/subdivision))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key subdivision))
(def ^{:const true, :specs [:varchar "NOT NULL"]} code :code)
(def ^{:const true, :specs [:integer "NOT NULL"]} institution (naming/foreign-key provision-institution/institution))
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
