(ns yahont.modules.dictionaries.register.controllers.payer.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.payer.create :as view]
            [yahont.modules.dictionaries.register.provision.payer :as provision]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (payer/create params)))]
      (redirect (string/replace url/dictionaries-register-payer-show #":id" (str id))))
    (catch Exception e (view/error))))
