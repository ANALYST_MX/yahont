(ns yahont.modules.dictionaries.register.controllers.subdivision.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.subdivision.edit :as view]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [yahont.modules.dictionaries.register.provision.institution :as provision-institution]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (subdivision/select-by-id id)) (provision-institution/key-entries (institution/select-all)))
    (catch Exception e (do ((println e) (view/error))))))
