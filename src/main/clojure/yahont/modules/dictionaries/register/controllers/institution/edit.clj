(ns yahont.modules.dictionaries.register.controllers.institution.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.edit :as view]
            [yahont.modules.dictionaries.register.provision.institution :as provision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [yahont.modules.versions.register.persistence.register :as version]
            [yahont.modules.versions.register.provision.register :as provision-version]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (institution/select-by-id id)))
    (catch Exception e (view/error))))
