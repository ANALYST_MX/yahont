(ns yahont.modules.dictionaries.register.controllers.department.show
  (:require [yahont.modules.dictionaries.register.views.department.show :as view]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.register.persistence.department :as department]))

(defn index [id]
  (try
    (view/index (provision/key-entry (department/select-by-id id)))
    (catch Exception e (view/error))))
