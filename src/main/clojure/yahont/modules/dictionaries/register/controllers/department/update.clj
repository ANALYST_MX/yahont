(ns yahont.modules.dictionaries.register.controllers.department.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.update :as view]
            [yahont.modules.dictionaries.register.persistence.department :as department]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (department/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-department-show #":id" id))
    (catch Exception e (view/error))))
