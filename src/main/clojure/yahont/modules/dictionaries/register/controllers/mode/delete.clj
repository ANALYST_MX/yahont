(ns yahont.modules.dictionaries.register.controllers.mode.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.delete :as view]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (mode/delete-by-id id)
    (redirect url/dictionaries-register-modes)
    (catch Exception e (view/error))))
