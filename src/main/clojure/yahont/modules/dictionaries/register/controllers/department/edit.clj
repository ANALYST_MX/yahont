(ns yahont.modules.dictionaries.register.controllers.department.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.edit :as view]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.register.persistence.department :as department]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [yahont.modules.dictionaries.register.provision.profile :as provision-profile]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision-subdivision]
            [yahont.modules.versions.register.persistence.register :as version]
            [yahont.modules.versions.register.provision.register :as provision-version]))

(defn index [id]
  (try
    (view/index (provision/key-entry (department/select-by-id id)) (provision-profile/key-entries (profile/select-all)) (provision-subdivision/key-entries (subdivision/select-all)))
    (catch Exception e (view/error))))
