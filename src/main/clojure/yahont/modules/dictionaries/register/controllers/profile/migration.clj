(ns yahont.modules.dictionaries.register.controllers.profile.migration
  (:require [yahont.modules.dictionaries.register.views.profile.migration :as view]
            [yahont.modules.dictionaries.register.models.profile :as profile]))

(defn migration-table []
  (try
    (profile/create-table)
    (view/ok)
    (catch Exception e (view/error))))
