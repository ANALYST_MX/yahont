(ns yahont.modules.dictionaries.register.controllers.department.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.delete :as view]
            [yahont.modules.dictionaries.register.persistence.department :as department]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (department/delete-by-id id)
    (redirect url/dictionaries-register-departments)
    (catch Exception e (view/error))))
