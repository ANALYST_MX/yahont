(ns yahont.modules.dictionaries.register.controllers.payer.show
  (:require [yahont.modules.dictionaries.register.views.payer.show :as view]
            [yahont.modules.dictionaries.register.provision.payer :as provision]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]))

(defn index [id]
  (try
    (view/index (provision/key-entry (payer/select-by-id id)))
    (catch Exception e (view/error))))
