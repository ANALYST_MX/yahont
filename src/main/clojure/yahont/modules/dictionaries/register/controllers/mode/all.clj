(ns yahont.modules.dictionaries.register.controllers.mode.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.all :as view]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]))

(defn index []
  (try
    (view/index (provision/key-entries (mode/select-all)))
    (catch Exception e (view/error))))
