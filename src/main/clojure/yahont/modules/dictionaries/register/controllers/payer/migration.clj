(ns yahont.modules.dictionaries.register.controllers.payer.migration
  (:require [yahont.modules.dictionaries.register.views.payer.migration :as view]
            [yahont.modules.dictionaries.register.models.payer :as payer]))

(defn migration-table []
  (try
    (payer/create-table)
    (view/ok)
    (catch Exception e (view/error))))
