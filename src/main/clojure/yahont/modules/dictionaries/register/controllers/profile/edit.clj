(ns yahont.modules.dictionaries.register.controllers.profile.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.profile.edit :as view]
            [yahont.modules.dictionaries.register.provision.profile :as provision]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [yahont.modules.versions.register.persistence.register :as version]
            [yahont.modules.versions.register.provision.register :as provision-version]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (profile/select-by-id id)))
    (catch Exception e (view/error))))
