(ns yahont.modules.dictionaries.register.controllers.department.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.create :as view]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.register.persistence.department :as department]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [yahont.modules.dictionaries.register.provision.profile :as provision-profile]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision-subdivision]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index (provision-profile/key-entries (profile/select-all)) (provision-subdivision/key-entries (subdivision/select-all))))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (department/create params)))]
      (redirect (string/replace url/dictionaries-register-department-show #":id" (str id))))
    (catch Exception e (view/error))))
