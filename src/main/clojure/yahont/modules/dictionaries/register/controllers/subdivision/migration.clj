(ns yahont.modules.dictionaries.register.controllers.subdivision.migration
  (:require [yahont.modules.dictionaries.register.views.subdivision.migration :as view]
            [yahont.modules.dictionaries.register.models.subdivision :as subdivision]))

(defn migration-table []
  (try
    (subdivision/create-table)
    (view/ok)
    (catch Exception e (view/error))))
