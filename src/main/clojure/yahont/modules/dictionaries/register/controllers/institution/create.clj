(ns yahont.modules.dictionaries.register.controllers.institution.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.create :as view]
            [yahont.modules.dictionaries.register.provision.institution :as provision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (institution/create params)))]
      (redirect (string/replace url/dictionaries-register-institution-show #":id" (str id))))
    (catch Exception e (view/error))))
