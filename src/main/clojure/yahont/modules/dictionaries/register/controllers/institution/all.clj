(ns yahont.modules.dictionaries.register.controllers.institution.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.all :as view]
            [yahont.modules.dictionaries.register.provision.institution :as provision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]))

(defn index []
  (try
    (view/index (provision/key-entries (institution/select-all)))
    (catch Exception e (view/error))))
