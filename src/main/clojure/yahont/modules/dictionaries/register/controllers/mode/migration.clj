(ns yahont.modules.dictionaries.register.controllers.mode.migration
  (:require [yahont.modules.dictionaries.register.views.mode.migration :as view]
            [yahont.modules.dictionaries.register.models.mode :as mode]))

(defn migration-table []
  (try
    (mode/create-table)
    (view/ok)
    (catch Exception e (view/error))))
