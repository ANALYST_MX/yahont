(ns yahont.modules.dictionaries.register.controllers.subdivision.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.subdivision.delete :as view]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (subdivision/delete-by-id id)
    (redirect url/dictionaries-register-subdivisions)
    (catch Exception e (view/error))))
