(ns yahont.modules.dictionaries.register.controllers.profile.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.profile.all :as view]
            [yahont.modules.dictionaries.register.provision.profile :as provision]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]))

(defn index []
  (try
    (view/index (provision/key-entries (profile/select-all)))
    (catch Exception e (view/error))))
