(ns yahont.modules.dictionaries.register.controllers.subdivision.create
  (:require [clojure.string :as string]
            [clojure.set :refer [rename-keys]]
            [yahont.url :as url]
            [yahont.db.convention.name :as naming]
            [yahont.modules.dictionaries.register.views.subdivision.create :as view]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [yahont.modules.dictionaries.register.provision.institution :as provision-institution]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index (provision-institution/key-entries (institution/select-all))))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (subdivision/create params)))]
      (redirect (string/replace url/dictionaries-register-subdivision-show #":id" (str id))))
    (catch Exception e (view/error))))
