(ns yahont.modules.dictionaries.register.controllers.institution.show
  (:require [yahont.modules.dictionaries.register.views.institution.show :as view]
            [yahont.modules.dictionaries.register.provision.institution :as provision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]))

(defn index [id]
  (try
    (view/index (provision/key-entry (institution/select-by-id id)))
    (catch Exception e (view/error))))
