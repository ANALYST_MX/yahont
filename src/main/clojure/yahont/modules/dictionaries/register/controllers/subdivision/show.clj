(ns yahont.modules.dictionaries.register.controllers.subdivision.show
  (:require [yahont.modules.dictionaries.register.views.subdivision.show :as view]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]))

(defn index [id]
  (try
    (view/index (provision/key-entry (subdivision/select-by-id id)))
    (catch Exception e (view/error))))
