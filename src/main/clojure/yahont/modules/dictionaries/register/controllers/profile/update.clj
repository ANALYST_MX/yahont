(ns yahont.modules.dictionaries.register.controllers.profile.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.profile.update :as view]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (profile/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-profile-show #":id" id))
    (catch Exception e (view/error))))
