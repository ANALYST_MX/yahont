(ns yahont.modules.dictionaries.register.controllers.profile.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.profile.delete :as view]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (profile/delete-by-id id)
    (redirect url/dictionaries-register-profiles)
    (catch Exception e (view/error))))
