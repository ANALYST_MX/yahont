(ns yahont.modules.dictionaries.register.controllers.payer.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.payer.all :as view]
            [yahont.modules.dictionaries.register.provision.payer :as provision]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]))

(defn index []
  (try
    (view/index (provision/key-entries (payer/select-all)))
    (catch Exception e (view/error))))
