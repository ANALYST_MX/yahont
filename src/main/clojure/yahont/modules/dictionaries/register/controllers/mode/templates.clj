(ns yahont.modules.dictionaries.register.controllers.mode.templates
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.all :as view]
            [yahont.modules.dictionaries.register.views.mode.templates.select :as select]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]))

(defn select-options
  ([] (select-options nil))
  ([selected]
   (try
     (select/options (provision/key-entries (mode/select-all)) selected)
     (catch Exception e (view/error)))))
