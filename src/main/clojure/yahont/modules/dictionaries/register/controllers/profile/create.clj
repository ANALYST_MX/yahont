(ns yahont.modules.dictionaries.register.controllers.profile.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.profile.create :as view]
            [yahont.modules.dictionaries.register.provision.profile :as provision]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (profile/create params)))]
      (redirect (string/replace url/dictionaries-register-profile-show #":id" (str id))))
    (catch Exception e (view/error))))
