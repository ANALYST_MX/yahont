(ns yahont.modules.dictionaries.register.controllers.mode.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.update :as view]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (mode/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-mode-show #":id" id))
    (catch Exception e (view/error))))
