(ns yahont.modules.dictionaries.register.controllers.mode.show
  (:require [yahont.modules.dictionaries.register.views.mode.show :as view]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]))

(defn index [id]
  (try
    (view/index (provision/key-entry (mode/select-by-id id)))
    (catch Exception e (view/error))))
