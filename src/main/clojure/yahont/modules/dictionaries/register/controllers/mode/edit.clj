(ns yahont.modules.dictionaries.register.controllers.mode.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.edit :as view]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (mode/select-by-id id)))
    (catch Exception e (view/error))))
