(ns yahont.modules.dictionaries.register.controllers.mode.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.mode.create :as view]
            [yahont.modules.dictionaries.register.provision.mode :as provision]
            [yahont.modules.dictionaries.register.persistence.mode :as mode]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (mode/create params)))]
      (redirect (string/replace url/dictionaries-register-mode-show #":id" (str id))))
    (catch Exception e (do (println e) (view/error)))))
