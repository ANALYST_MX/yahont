(ns yahont.modules.dictionaries.register.controllers.institution.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.delete :as view]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (institution/delete-by-id id)
    (redirect url/dictionaries-register-institutions)
    (catch Exception e (view/error))))
