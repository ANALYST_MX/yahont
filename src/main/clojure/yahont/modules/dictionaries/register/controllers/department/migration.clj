(ns yahont.modules.dictionaries.register.controllers.department.migration
  (:require [yahont.modules.dictionaries.register.views.department.migration :as view]
            [yahont.modules.dictionaries.register.models.department :as department]))

(defn migration-table []
  (try
    (department/create-table)
    (view/ok)
    (catch Exception e (view/error))))
