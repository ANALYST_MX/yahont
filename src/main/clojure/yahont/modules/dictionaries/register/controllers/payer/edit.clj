(ns yahont.modules.dictionaries.register.controllers.payer.edit
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.payer.edit :as view]
            [yahont.modules.dictionaries.register.provision.payer :as provision]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]
            [yahont.modules.versions.register.persistence.register :as version]
            [yahont.modules.versions.register.provision.register :as provision-version]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (payer/select-by-id id)))
    (catch Exception e (view/error))))
