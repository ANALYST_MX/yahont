(ns yahont.modules.dictionaries.register.controllers.profile.show
  (:require [yahont.modules.dictionaries.register.views.profile.show :as view]
            [yahont.modules.dictionaries.register.provision.profile :as provision]
            [yahont.modules.dictionaries.register.persistence.profile :as profile]))

(defn index [id]
  (try
    (view/index (provision/key-entry (profile/select-by-id id)))
    (catch Exception e (view/error))))
