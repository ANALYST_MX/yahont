(ns yahont.modules.dictionaries.register.controllers.department.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.all :as view]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.register.persistence.department :as department]))

(defn index []
  (try
    (view/index (provision/key-entries (department/select-all)))
    (catch Exception e (view/error))))
