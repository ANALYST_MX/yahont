(ns yahont.modules.dictionaries.register.controllers.institution.migration
  (:require [yahont.modules.dictionaries.register.views.institution.migration :as view]
            [yahont.modules.dictionaries.register.models.institution :as institution]))

(defn migration-table []
  (try
    (institution/create-table)
    (view/ok)
    (catch Exception e (view/error))))
