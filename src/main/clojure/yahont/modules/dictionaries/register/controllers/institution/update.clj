(ns yahont.modules.dictionaries.register.controllers.institution.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.update :as view]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (institution/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-institution-show #":id" id))
    (catch Exception e (view/error))))
