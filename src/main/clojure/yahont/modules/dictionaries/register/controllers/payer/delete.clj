(ns yahont.modules.dictionaries.register.controllers.payer.delete
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.payer.delete :as view]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (payer/delete-by-id id)
    (redirect url/dictionaries-register-payers)
    (catch Exception e (view/error))))
