(ns yahont.modules.dictionaries.register.controllers.institution.templates
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.institution.all :as view]
            [yahont.modules.dictionaries.register.views.institution.templates.select :as select]
            [yahont.modules.dictionaries.register.provision.institution :as provision]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]))

(defn select-options
  ([] (select-options nil))
  ([selected]
   (try
     (select/options (provision/key-entries (institution/select-all)) selected)
     (catch Exception e (view/error)))))
