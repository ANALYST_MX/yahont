(ns yahont.modules.dictionaries.register.controllers.subdivision.all
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.subdivision.all :as view]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]))

(defn index []
  (try
    (view/index (provision/key-entries (subdivision/select-all)))
    (catch Exception e (view/error))))
