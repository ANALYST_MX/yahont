(ns yahont.modules.dictionaries.register.controllers.subdivision.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.subdivision.update :as view]
            [yahont.modules.dictionaries.register.persistence.subdivision :as subdivision]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (subdivision/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-subdivision-show #":id" id))
    (catch Exception e (view/error))))
