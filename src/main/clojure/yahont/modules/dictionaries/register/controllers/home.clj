(ns yahont.modules.dictionaries.register.controllers.home
  (:require [yahont.modules.dictionaries.register.views.home :as view]))

(defn index []
  (view/index))
