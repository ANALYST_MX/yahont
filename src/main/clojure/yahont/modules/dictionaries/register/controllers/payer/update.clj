(ns yahont.modules.dictionaries.register.controllers.payer.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.payer.update :as view]
            [yahont.modules.dictionaries.register.persistence.payer :as payer]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (payer/update-by-id id params)
    (redirect (string/replace url/dictionaries-register-payer-show #":id" id))
    (catch Exception e (view/error))))
