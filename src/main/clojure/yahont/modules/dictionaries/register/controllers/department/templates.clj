(ns yahont.modules.dictionaries.register.controllers.department.templates
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.views.department.all :as view]
            [yahont.modules.dictionaries.register.views.department.templates.select :as select]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.register.persistence.department :as department]))

(defn select-options
  ([] (select-options nil))
  ([selected]
   (try
     (select/options (provision/key-entries (department/select-all)) selected)
     (catch Exception e (view/error)))))
