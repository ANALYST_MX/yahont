(ns yahont.modules.dictionaries.register.persistence.profile
  (:require [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.register.db.schema.profile :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.register.models.profile :as profile]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/profile (profile/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/profile (profile/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/profile (profile/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (profile/delete-row-by-id id)))

(defn select-all []
  (entries->map models/profile (profile/select-all-rows)))
