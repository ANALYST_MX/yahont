(ns yahont.modules.dictionaries.register.persistence.mode
  (:require [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.register.db.schema.mode :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.register.models.mode :as mode]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/mode (mode/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/mode (mode/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/mode (mode/select-row-by-id id))))

(defn select-production []
  (select-by-id 1))

(defn select-development []
  (select-by-id 2))

(defn select-test []
  (select-by-id 3))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (mode/delete-row-by-id id)))

(defn select-all []
  (entries->map models/mode (mode/select-all-rows)))
