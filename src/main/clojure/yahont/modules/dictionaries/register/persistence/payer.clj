(ns yahont.modules.dictionaries.register.persistence.payer
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.register.db.schema.payer :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.register.models.payer :as payer]
            [yahont.modules.dictionaries.register.provision.payer :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/payer (payer/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/payer (payer/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/payer (payer/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (payer/delete-row-by-id id)))

(defn select-all []
  (entries->map models/payer (payer/select-all-rows)))

(defn select-by-code [code]
  (entry->map models/payer (-> (select :*)
                               (from (keyword provision/payer))
                               (where [:= provision/code code])
                               payer/select-row)))
