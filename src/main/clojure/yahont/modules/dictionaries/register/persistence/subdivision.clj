(ns yahont.modules.dictionaries.register.persistence.subdivision
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.register.db.schema.subdivision :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.register.models.subdivision :as subdivision]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.register.models :as models]
            [yahont.modules.dictionaries.register.persistence.institution :as institution]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/subdivision (subdivision/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/subdivision (subdivision/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/subdivision (subdivision/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (subdivision/delete-row-by-id id)))

(defn select-all []
  (entries->map models/subdivision (subdivision/select-all-rows)))

(defn select-institution [subdivision]
  (let [subdivision (provision/key-entry subdivision)]
    (institution/select-by-id (provision/institution subdivision))))

(defn select-by-code [code]
  (entry->map models/subdivision (-> (select :*)
                                     (from (keyword provision/subdivision))
                                     (where [:= provision/code code])
                                     subdivision/select-row)))
