(ns yahont.modules.dictionaries.register.persistence.department
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.dictionaries.register.db.schema.department :refer [prepare prepare-pk]]
            [yahont.modules.dictionaries.register.models.department :as department]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.dictionaries.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/department (department/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/department (department/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/department (department/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (department/delete-row-by-id id)))

(defn select-all []
  (entries->map models/department (department/select-all-rows)))

(defn select-by-code [code]
  (entry->map models/department (-> (select :*)
                                    (from (keyword provision/department))
                                    (where [:= provision/code code])
                                    department/select-row)))
