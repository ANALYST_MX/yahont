(ns yahont.modules.dictionaries.register.views.institution.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.institution :as provision]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))

(defn index [institution]
  (common :title (t :title/institution)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-institution (t :label/institution))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-register-institution-update #":id" (str (provision/pk institution)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code institution))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name institution))
                           [:br]
                           (label "date_start" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date) (unparse default-formatter (provision/start-date institution)))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date) (unparse default-formatter (provision/end-date institution)))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
