(ns yahont.modules.dictionaries.register.views.subdivision.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn subdivision-summary [subdivision]
  [:tr
   [:td (provision/pk subdivision)]
   [:td (provision/code subdivision)]
   [:td (provision/name subdivision)]
   [:td [:a {:href (string/replace url/dictionaries-register-subdivision-edit #":id" (str (provision/pk subdivision)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-subdivision-delete #":id" (str (provision/pk subdivision)))} (t :label/delete)]]])

(defn list-subdivisions [subdivisions]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(subdivision-summary (provision/key-entry %)) subdivisions))])

(defn index [subdivisions]
  (common :title (t :title/subdivision)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-subdivision (t :label/subdivision))]
                   ]]
                 [:div.all
                  (list-subdivisions subdivisions)]]))

(defn error [] (server-error))
