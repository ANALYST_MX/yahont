(ns yahont.modules.dictionaries.register.views.department.create
  (:require [yahont.url :as url]
            [yahont.syntax.relational :refer [select-values]]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field select-options submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.common.controllers.yes_no.templates :as render-yes-no]))

(defn- generate-profiles-option-list [profiles] (map (fn [{:keys [profile]}] (select-values profile [provision/pk provision/name])) profiles))

(defn- generate-subdivisions-option-list [subdivisions] (map (fn [{:keys [subdivision]}] (select-values subdivision [provision/pk provision/name])) subdivisions))

(defn index [profiles subdivisions]
  (common :title (t :title/department)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-department (t :label/department))]
                   ]]
                 [:div.create
                  (form-to [:post url/dictionaries-register-department-create]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (label "profile" (t :label/profile))
                           [:br]
                           [:select {provision/pk provision/profile provision/name provision/profile}
                            (select-options (generate-profiles-option-list profiles))
                            ]
                           [:br]
                           (label "subdivision" (t :label/subdivision))
                           [:br]
                           [:select {provision/pk provision/subdivision provision/name provision/subdivision}
                            (select-options (generate-subdivisions-option-list subdivisions))
                            ]
                           [:br]
                           (label "paid" (t :label/paid))
                           [:br]
                           [:select {provision/pk provision/paid provision/name provision/paid}
                            (render-yes-no/select-options)
                            ]
                           [:br]
                           (label "start_date" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
