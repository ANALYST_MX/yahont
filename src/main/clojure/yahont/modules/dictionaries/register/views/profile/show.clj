(ns yahont.modules.dictionaries.register.views.profile.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.profile :as provision]))

(defn index [profile]
  (common :title (t :title/profile)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-profile (t :label/profile))]
                   [:li (link-to url/dictionaries-register-profiles (t :label/profiles))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code profile)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name profile)
                  [:br]]]))

(defn error [] (server-error))
