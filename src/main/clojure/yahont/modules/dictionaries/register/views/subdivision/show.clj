(ns yahont.modules.dictionaries.register.views.subdivision.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]))

(defn index [subdivision]
  (common :title (t :title/subdivision)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-subdivision (t :label/subdivision))]
                   [:li (link-to url/dictionaries-register-subdivisions (t :label/subdivisions))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code subdivision)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name subdivision)
                  [:br]]]))

(defn error [] (server-error))
