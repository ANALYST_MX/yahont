(ns yahont.modules.dictionaries.register.views.department.update
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
