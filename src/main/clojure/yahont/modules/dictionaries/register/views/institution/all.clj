(ns yahont.modules.dictionaries.register.views.institution.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.institution :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn institution-summary [institution]
  [:tr
   [:td (provision/pk institution)]
   [:td (provision/code institution)]
   [:td (provision/name institution)]
   [:td [:a {:href (string/replace url/dictionaries-register-institution-edit #":id" (str (provision/pk institution)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-institution-delete #":id" (str (provision/pk institution)))} (t :label/delete)]]])

(defn list-institutions [institutions]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(institution-summary (provision/key-entry %)) institutions))])

(defn index [institutions]
  (common :title (t :title/institution)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-institution (t :label/institution))]
                   ]]
                 [:div.all
                  (list-institutions institutions)]]))

(defn error [] (server-error))
