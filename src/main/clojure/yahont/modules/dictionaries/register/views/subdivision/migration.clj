(ns yahont.modules.dictionaries.register.views.subdivision.migration
  (:require [yahont.modules.application.views.layout :refer [success server-error]]))

(defn ok [] (success))
(defn error [] (server-error))
