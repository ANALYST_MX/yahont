(ns yahont.modules.dictionaries.register.views.department.templates.select
  (:require [yahont.syntax.relational :refer [select-values]]
            [hiccup.form :refer [select-options]]
            [yahont.modules.dictionaries.register.provision.department :as provision]))

(defmacro options [department-all selected]
  (let [entry (symbol (name provision/key-entry))]
    `(select-options
      (map (fn [{:keys [~entry]}] (select-values ~entry [~provision/pk ~provision/name])) ~department-all) ~selected)))
