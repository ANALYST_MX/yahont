(ns yahont.modules.dictionaries.register.views.mode.edit
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]))

(defn index [mode]
  (common :title (t :title/mode)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-mode (t :label/mode))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-register-mode-update #":id" (str (provision/pk mode)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code mode))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name mode))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
