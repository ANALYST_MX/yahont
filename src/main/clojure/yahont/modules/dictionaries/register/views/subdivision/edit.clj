(ns yahont.modules.dictionaries.register.views.subdivision.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.syntax.relational :refer [select-values]]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field select-options submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))

(defn- generate-institutions-option-list [institutions] (map (fn [{:keys [institution]}] (select-values institution [provision/pk provision/name])) institutions))

(defn index [subdivision institutions]
  (common :title (t :title/subdivision)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-subdivision (t :label/subdivision))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-register-subdivision-update #":id" (str (provision/pk subdivision)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code subdivision))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           [:select {provision/pk provision/institution provision/name provision/institution}
                            (select-options (generate-institutions-option-list institutions) (provision/institution subdivision))
                            ]
                           [:br]
                           (label "date_start" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date) (unparse default-formatter (provision/start-date subdivision)))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date) (unparse default-formatter (provision/end-date subdivision)))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
