(ns yahont.modules.dictionaries.register.views.subdivision
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/subdivision)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-subdivisions (t :label/all))]
                   [:li (link-to url/dictionaries-register-subdivision-create (t :label/create))]]]]))
