(ns yahont.modules.dictionaries.register.views.department.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.department :as provision]))

(defn index [department]
  (common :title (t :title/department)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-department (t :label/department))]
                   [:li (link-to url/dictionaries-register-departments (t :label/departments))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code department)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name department)
                  [:br]]]))

(defn error [] (server-error))
