(ns yahont.modules.dictionaries.register.views.mode.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]))

(defn index [mode]
  (common :title (t :title/mode)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-mode (t :label/mode))]
                   [:li (link-to url/dictionaries-register-modes (t :label/modes))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code mode)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name mode)
                  [:br]]]))

(defn error [] (server-error))
