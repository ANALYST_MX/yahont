(ns yahont.modules.dictionaries.register.views.payer.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.payer :as provision]))

(defn index [payer]
  (common :title (t :title/payer)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-payer (t :label/payer))]
                   [:li (link-to url/dictionaries-register-payers (t :label/payers))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code payer)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name payer)
                  [:br]]]))

(defn error [] (server-error))
