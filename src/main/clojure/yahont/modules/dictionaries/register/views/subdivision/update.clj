(ns yahont.modules.dictionaries.register.views.subdivision.update
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
