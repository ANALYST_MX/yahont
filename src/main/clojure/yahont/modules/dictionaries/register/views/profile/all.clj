(ns yahont.modules.dictionaries.register.views.profile.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.profile :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn profile-summary [profile]
  [:tr
   [:td (provision/pk profile)]
   [:td (provision/code profile)]
   [:td (provision/name profile)]
   [:td [:a {:href (string/replace url/dictionaries-register-profile-edit #":id" (str (provision/pk profile)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-profile-delete #":id" (str (provision/pk profile)))} (t :label/delete)]]])

(defn list-profiles [profiles]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(profile-summary (provision/key-entry %)) profiles))])

(defn index [profiles]
  (common :title (t :title/profile)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-profile (t :label/profile))]
                   ]]
                 [:div.all
                  (list-profiles profiles)]]))

(defn error [] (server-error))
