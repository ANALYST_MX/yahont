(ns yahont.modules.dictionaries.register.views.mode.create
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]))

(defn index []
  (common :title (t :title/mode)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-mode (t :label/mode))]
                   ]]
                 [:div.create
                  (form-to [:post url/dictionaries-register-mode-create]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
