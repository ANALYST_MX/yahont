(ns yahont.modules.dictionaries.register.views.subdivision.create
  (:require [yahont.url :as url]
            [yahont.syntax.relational :refer [select-values]]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field select-options submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.dictionaries.register.provision.subdivision :as provision]))

(defn- generate-institutions-option-list [institutions] (map (fn [{:keys [institution]}] (select-values institution [provision/pk provision/name])) institutions))

(defn index [institutions]
  (common :title (t :title/subdivision)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-subdivision (t :label/subdivision))]
                   ]]
                 [:div.create
                  (form-to [:post url/dictionaries-register-subdivision-create]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (label "institution" (t :label/name))
                           [:br]
                           [:select {provision/pk provision/institution provision/name provision/institution}
                            (select-options (generate-institutions-option-list institutions))
                            ]
                           [:br]
                           (label "start_date" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
