(ns yahont.modules.dictionaries.register.views.mode.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn mode-summary [mode]
  [:tr
   [:td (provision/pk mode)]
   [:td (provision/code mode)]
   [:td (provision/name mode)]
   [:td [:a {:href (string/replace url/dictionaries-register-mode-edit #":id" (str (provision/pk mode)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-mode-delete #":id" (str (provision/pk mode)))} (t :label/delete)]]])

(defn list-modes [modes]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(mode-summary (provision/key-entry %)) modes))])

(defn index [modes]
  (common :title (t :title/mode)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-mode (t :label/mode))]
                   ]]
                 [:div.all
                  (list-modes modes)]]))

(defn error [] (server-error))
