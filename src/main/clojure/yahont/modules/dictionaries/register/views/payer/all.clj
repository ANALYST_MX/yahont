(ns yahont.modules.dictionaries.register.views.payer.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.payer :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn payer-summary [payer]
  [:tr
   [:td (provision/pk payer)]
   [:td (provision/code payer)]
   [:td (provision/name payer)]
   [:td [:a {:href (string/replace url/dictionaries-register-payer-edit #":id" (str (provision/pk payer)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-payer-delete #":id" (str (provision/pk payer)))} (t :label/delete)]]])

(defn list-payers [payers]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(payer-summary (provision/key-entry %)) payers))])

(defn index [payers]
  (common :title (t :title/payer)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-payer (t :label/payer))]
                   ]]
                 [:div.all
                  (list-payers payers)]]))

(defn error [] (server-error))
