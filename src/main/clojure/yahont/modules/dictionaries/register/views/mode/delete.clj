(ns yahont.modules.dictionaries.register.views.mode.delete
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
