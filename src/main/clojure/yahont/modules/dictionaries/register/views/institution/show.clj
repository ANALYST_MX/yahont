(ns yahont.modules.dictionaries.register.views.institution.show
  (:require [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.institution :as provision]))

(defn index [institution]
  (common :title (t :title/institution)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-institution (t :label/institution))]
                   [:li (link-to url/dictionaries-register-institutions (t :label/institutions))]
                   ]]
                 [:div.show
                  (label "code" (t :label/code))
                  [:br]
                  (provision/code institution)
                  [:br]
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name institution)
                  [:br]]]))

(defn error [] (server-error))
