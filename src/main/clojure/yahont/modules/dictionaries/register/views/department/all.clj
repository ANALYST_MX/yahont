(ns yahont.modules.dictionaries.register.views.department.all
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.department :as provision]))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn department-summary [department]
  [:tr
   [:td (provision/pk department)]
   [:td (provision/code department)]
   [:td (provision/name department)]
   [:td [:a {:href (string/replace url/dictionaries-register-department-edit #":id" (str (provision/pk department)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/dictionaries-register-department-delete #":id" (str (provision/pk department)))} (t :label/delete)]]])

(defn list-departments [departments]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "code" "name"])
     ]]
   (into [:tbody]
         (map #(department-summary (provision/key-entry %)) departments))])

(defn index [departments]
  (common :title (t :title/department)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-department (t :label/department))]
                   ]]
                 [:div.all
                  (list-departments departments)]]))

(defn error [] (server-error))
