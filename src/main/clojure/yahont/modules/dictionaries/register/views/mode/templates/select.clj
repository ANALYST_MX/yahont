(ns yahont.modules.dictionaries.register.views.mode.templates.select
  (:require [yahont.syntax.relational :refer [select-values]]
            [hiccup.form :refer [select-options]]
            [yahont.modules.dictionaries.register.provision.mode :as provision]))

(defmacro options [mode-all selected]
  (let [entry (symbol (name provision/key-entry))]
    `(select-options
      (map (fn [{:keys [~entry]}] (select-values ~entry [~provision/pk ~provision/name])) ~mode-all) ~selected)))
