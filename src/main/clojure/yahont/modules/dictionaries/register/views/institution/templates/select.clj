(ns yahont.modules.dictionaries.register.views.institution.templates.select
  (:require [yahont.syntax.relational :refer [select-values]]
            [hiccup.form :refer [select-options]]
            [yahont.modules.dictionaries.register.provision.institution :as provision]))

(defmacro options [institution-all selected]
  (let [entry (symbol (name provision/key-entry))]
    `(select-options
      (map (fn [{:keys [~entry]}] (select-values ~entry [~provision/pk ~provision/name])) ~institution-all) ~selected)))
