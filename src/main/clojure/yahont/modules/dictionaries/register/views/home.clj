(ns yahont.modules.dictionaries.register.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-institution (t :label/institution))]
                   [:li (link-to url/dictionaries-register-subdivision (t :label/subdivision))]
                   [:li (link-to url/dictionaries-register-payer (t :label/payer))]
                   [:li (link-to url/dictionaries-register-department (t :label/department))]
                   [:li (link-to url/dictionaries-register-profile (t :label/profile))]
                   [:li (link-to url/dictionaries-register-mode (t :label/mode))]]]]))
