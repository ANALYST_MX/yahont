(ns yahont.modules.dictionaries.register.views.department.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.syntax.relational :refer [select-values]]
            [yahont.url :as url]
            [yahont.modules.dictionaries.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field select-options submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.dictionaries.register.provision.department :as provision]
            [yahont.modules.dictionaries.common.controllers.yes_no.templates :as render-yes-no]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))

(defn- generate-profiles-option-list [profiles] (map (fn [{:keys [profile]}] (select-values profile [provision/pk provision/name])) profiles))

(defn- generate-subdivisions-option-list [subdivisions] (map (fn [{:keys [subdivision]}] (select-values subdivision [provision/pk provision/name])) subdivisions))

(defn index [department profiles subdivisions]
  (common :title (t :title/department)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/dictionaries-register-home (t :label/home))]
                   [:li (link-to url/dictionaries-register-department (t :label/department))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/dictionaries-register-department-update #":id" (str (provision/pk department)))]
                           (label "code" (t :label/code))
                           [:br]
                           (text-field (name provision/code) (provision/code department))
                           [:br]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name department))
                           [:br]
                           (label "profile" (t :label/profile))
                           [:br]
                           [:select {:id provision/profile :name provision/profile}
                            (select-options (generate-profiles-option-list profiles) (provision/profile department))
                            ]
                           [:br]
                           (label "subdivision" (t :label/subdivision))
                           [:br]
                           [:select {:id provision/subdivision :name provision/subdivision}
                            (select-options (generate-subdivisions-option-list subdivisions) (provision/subdivision department))
                            ]
                           [:br]
                           (label "paid" (t :label/paid))
                           [:br]
                           [:select {:id provision/paid :name provision/paid}
                            (render-yes-no/select-options (provision/paid department))
                            ]
                           [:br]
                           (label "date_start" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date) (unparse default-formatter (provision/start-date department)))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date) (unparse default-formatter (provision/end-date department)))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
