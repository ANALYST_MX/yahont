(ns yahont.modules.dictionaries.register.views.payer.delete
  (:require
   [yahont.modules.application.views.layout :refer [server-error]]))

(defn error [] (server-error))
