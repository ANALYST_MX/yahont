(ns yahont.modules.dictionaries.register.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :institution "Учреждение"
      :subdivision "Подразделение"
      :department "Отделение"
      :profile "Профиль"
      :payer "Платильщик"
      :mode "Режим"
      }
     :label
     {:home "Главная"
      :institution "Учреждение"
      :institutions "Учреждения"
      :subdivision "Подразделение"
      :subdivisions "Подразделения"
      :department "Отделение"
      :departments "Отделения"
      :profile "Профиль"
      :profiles "Профили"
      :payer "Платильщик"
      :payers "Платильщики"
      :create "Создать"
      :mode "Режим"
      :modes "Режимы"
      :paid "Оплачивается?"
      :start-date "С"
      :end-date "До"
      :code "Код"
      :name "Наименование"
      :edit "Редактировать"
      :all "Все"
      :delete "Удалить"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
