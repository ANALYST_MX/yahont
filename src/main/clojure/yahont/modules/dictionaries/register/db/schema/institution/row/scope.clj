(ns yahont.modules.dictionaries.register.db.schema.institution.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.dictionaries.register.provision.institution])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code name start-date end-date])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   name convert/->str
   start-date convert/->str->timestamp
   end-date convert/->str->timestamp})

(def mapping
  {})
