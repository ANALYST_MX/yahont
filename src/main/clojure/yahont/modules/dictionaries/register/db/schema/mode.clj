(ns yahont.modules.dictionaries.register.db.schema.mode
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.dictionaries.register.db.schema.mode.row.scope :refer [scope conversion-pk]]
            [yahont.modules.dictionaries.register.db.schema.mode.row.conversion :refer [conversion]]
            [yahont.modules.dictionaries.register.db.schema.mode.row.mapping :refer [mapping]]
            [yahont.syntax.conditional :refer [if-let*]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (conversion-pk id))
