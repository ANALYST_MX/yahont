(ns yahont.modules.dictionaries.register.db.schema.payer.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.dictionaries.register.db.schema.payer.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
