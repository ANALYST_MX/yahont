(ns yahont.modules.dictionaries.register.db.schema.subdivision.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.dictionaries.register.db.schema.subdivision.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
