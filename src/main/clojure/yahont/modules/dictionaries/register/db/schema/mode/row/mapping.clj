(ns yahont.modules.dictionaries.register.db.schema.mode.row.mapping
  (:use [clojure.set])
  (:require [yahont.modules.dictionaries.register.db.schema.mode.row.scope :as scope]))

(defn mapping [row-map]
  (rename-keys row-map scope/mapping))
