(ns yahont.modules.dictionaries.register.db.schema.mode.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.dictionaries.register.provision.mode])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code name])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   name convert/->str})

(def mapping
  {})
