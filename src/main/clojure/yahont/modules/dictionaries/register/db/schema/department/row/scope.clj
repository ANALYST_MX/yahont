(ns yahont.modules.dictionaries.register.db.schema.department.row.scope
  (:refer-clojure :exclude [name])
  (:use [yahont.modules.dictionaries.register.provision.department])
  (:require [yahont.utils.convert :as convert]))

(def scope [pk code subdivision profile name paid start-date end-date])

(def conversion-pk convert/->str->int)

(def conversion
  {pk conversion-pk
   code convert/->str
   subdivision convert/->str->int
   profile convert/->str->int
   name convert/->str
   paid convert/->str->int 
   start-date convert/->str->timestamp
   end-date convert/->str->timestamp})

(def mapping
  {})
