(ns yahont.modules.dictionaries.register.db.schema.payer
  (:require [yahont.db.row :refer [select-columns]]
            [yahont.modules.dictionaries.register.db.schema.payer.row.scope :refer [scope conversion-pk]]
            [yahont.modules.dictionaries.register.db.schema.payer.row.conversion :refer [conversion]]
            [yahont.modules.dictionaries.register.db.schema.payer.row.mapping :refer [mapping]]
            [yahont.syntax.conditional :refer [if-let*]]))

(defn prepare
  ([row-map] (prepare row-map scope))
  ([row-map columnseq]
     (-> row-map
         mapping
         ((select-columns columnseq))
         conversion)))

(defn prepare-pk [id]
  (conversion-pk id))
