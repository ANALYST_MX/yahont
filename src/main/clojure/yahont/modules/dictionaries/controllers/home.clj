(ns yahont.modules.dictionaries.controllers.home
  (:require [yahont.modules.dictionaries.views.home :as view]))

(defn index []
  (view/index))
