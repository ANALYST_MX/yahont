(ns yahont.modules.status_code.controllers.four_oh_four
  (:require [yahont.modules.status_code.views.four_oh_four :as view]))

(defn index []
  (view/index))
