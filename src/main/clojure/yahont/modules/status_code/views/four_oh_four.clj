(ns yahont.modules.status_code.views.four_oh_four
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.status_code.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/page-not-found)
          :body [:div
                 [:h1 {:class "info-worning"} (t :info/page-not-found)]
                 [:p "There's no requested page. "]
                 (link-to {:class "btn btn-primary"} url/home (t :label/home))]))
