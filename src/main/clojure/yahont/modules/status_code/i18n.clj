(ns yahont.modules.status_code.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:page-not-found "Page Not Found"
      }
     :info
     {:page-not-found "The page you requested could not be found"
      }
     :label
     {:home "Главная"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
