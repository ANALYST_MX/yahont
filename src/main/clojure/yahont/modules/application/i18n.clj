(ns yahont.modules.application.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      }
     :label
     {:home "Главная"
      :register "Реестр"
      :dictionaries "Спрaвочники"
      :subdivisions "Подразделения"
      :versions "Версии"
      :services "Услуги"
      :info "О программе"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
