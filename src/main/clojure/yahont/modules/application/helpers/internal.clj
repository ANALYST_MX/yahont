(ns yahont.modules.application.helpers.internal)

(def default-pagination-count 20)

(defn offset-for
  [page num-per-page]
  (* num-per-page
     (- (max page 1) 1)))

(defmacro paginate
  [query & {:keys [page num-per-page] :or {page 0 num-per-page default-pagination-count}}]
  `(conj ~query
         (honeysql.helpers/limit ~num-per-page)
         (honeysql.helpers/offset (offset-for ~page ~num-per-page))))
