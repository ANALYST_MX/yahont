(ns yahont.modules.application.controllers.info
  (:require [yahont.modules.application.views.info :as view]))

(defn index []
  (view/index))
