(ns yahont.modules.application.controllers.home
  (:require [yahont.modules.application.views.home :as view]))

(defn index []
  (view/index))
