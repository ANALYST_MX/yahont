(ns yahont.modules.application.views.layout
  (:require [yahont.modules.application.i18n :refer [t]]
            [hiccup.page :as page]
            [yahont.support.http.response.status :as status]))

(defn common [& {:keys [title body] :or {title (t :title/home)}}]
  (page/html5
   [:head
    [:meta {:charset "utf-8"}]
    [:title title]
    (page/include-js "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js")]
   [:body
    [:div {:id "header"}]
    body
    [:div {:id "footer"}]]))

(defn server-error []
  status/server-error)

(defn success []
  status/success)
