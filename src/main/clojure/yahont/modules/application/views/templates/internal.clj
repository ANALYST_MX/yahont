(ns yahont.modules.application.views.templates.internal
  (:refer-clojure :exclude [next])
  (:require [hiccup.element :refer [link-to]]))

(defn- current [n]
  [:span (str n)])

(defn- previous [n]
  [:li (if (> n 1) (link-to (str "?page=" (dec n)) "&laquo;"))])

(defn- next [n total]
  [:li (if (> total n) (link-to (str "?page=" (inc n)) "&raquo;"))])

(defn paginate [page page-count per-page]
  (conj [:ul] (previous page) (current page) (next page (Math/ceil (/ page-count per-page)))))
