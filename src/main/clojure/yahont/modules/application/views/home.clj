(ns yahont.modules.application.views.home
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.application.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/register-home (t :label/register))]
                   [:li (link-to url/dictionaries-home (t :label/dictionaries))]
                   [:li (link-to url/versions-home (t :label/versions))]
                   [:li (link-to url/subdivisions-home (t :label/subdivisions))]
                   [:li (link-to url/services-home (t :label/services))]
                   [:li (link-to url/info (t :label/info))]]]]))
