(ns yahont.modules.versions.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      }
     :label
     {:home "Главная"
      :register "Реестр"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
