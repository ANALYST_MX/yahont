(ns yahont.modules.versions.register.persistence.register
  (:require [honeysql.helpers :refer :all]
            [yahont.support.time :refer [now]]
            [yahont.modules.versions.register.db.schema.register :refer [prepare prepare-pk]]
            [yahont.modules.versions.register.models.register :as register]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.db.marshal :refer [entry->map entries->map]]
            [yahont.modules.versions.register.models :as models]))

(defn create [row-map]
  (let [row-map (prepare row-map)]
    (entry->map models/register (register/create-row row-map))))

(defn update-by-id [id row-map]
  (let [id (prepare-pk id)
        row-map (merge-with #(or % %2) (prepare row-map) {:updated_at (now)})]
    (entry->map models/register (register/update-row-by-id id row-map))))

(defn select-by-id [id]
  (let [id (prepare-pk id)]
    (entry->map models/register (register/select-row-by-id id))))

(defn delete-by-id [id]
  (let [id (prepare-pk id)]
    (register/delete-row-by-id id)))

(defn select-all []
  (entries->map models/register (register/select-all-rows)))

(defn select-current []
  (let [datetime (now)]
    (entry->map models/register (-> (select :*)
                                    (from (keyword provision/register))
                                    (where [:and
                                            [:<= provision/start-date datetime]
                                            [:>= provision/end-date datetime]])
                                    register/select-current))))
