(ns yahont.modules.versions.register.models.register
  (:require [honeysql.core :as sql]
            [yahont.db.provision :refer [row-specs]]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.db.sql.table.create :as migrate]
            [yahont.db.sql.row.create :refer [create]]
            [yahont.db.sql.row.update :refer [update-by-id]]
            [yahont.db.sql.row.select :refer [select-by-id select-all select]]
            [yahont.db.sql.row.delete :refer [delete-by-id]]))

(defn create-table []
  (migrate/create provision/register
                  (row-specs provision/pk)
                  (row-specs provision/name)
                  (row-specs provision/start-date)
                  (row-specs provision/end-date)
                  (row-specs provision/description)
                  [:created_at :timestamp "NOT NULL" "DEFAULT CURRENT_TIMESTAMP"]
                  [:updated_at :timestamp]))

(defn create-row [row-map]
  (create provision/register row-map))

(defn update-row-by-id [id row-map]
  (update-by-id provision/register id row-map))

(defn select-row-by-id [id]
  (select-by-id provision/register id))

(defn delete-row-by-id [id]
  (delete-by-id provision/register id))

(defn select-all-rows []
  (select-all provision/register))

(defn select-current [sql]
  (select (sql/format sql)))
