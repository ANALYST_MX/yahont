(ns yahont.modules.versions.register.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      :register "Реестр"
      }
     :label
     {:home "Главная"
      :register "Реестр"
      :registers "Реестры"
      :create "Создать"
      :start-date "С"
      :end-date "До"
      :name "Наименование"
      :description "Описание"
      :edit "Редактировать"
      :all "Все"
      :delete "Удалить"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
