(ns yahont.modules.versions.register.provision.register
  (:refer-clojure :exclude [name])
  (:require [yahont.map.convention.name :refer [keyword-entry keyword-entries]]
            [yahont.db.convention.name :as naming]
            [yahont.modules :as modules]
            [yahont.modules.versions.submodules :as submodules]
            [yahont.modules.versions.register.models :as models]))

(def ^:const key-entry (keyword-entry models/register))

(def ^:const key-entries (keyword-entries models/register))

(def ^:const register (naming/table modules/versions submodules/register models/register))

(def ^{:const true, :specs [:serial "PRIMARY KEY"]} pk (naming/primary-key register))
(def ^{:const true, :specs [:varchar "NOT NULL"]} name :name)
(def ^{:const true, :specs [:timestamp "NOT NULL"]} start-date :start_date)
(def ^{:const true, :specs [:timestamp]} end-date :end_date)
(def ^{:const true, :specs [:varchar]} description :description)
