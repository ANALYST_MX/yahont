(ns yahont.modules.versions.register.controllers.register.update
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.versions.register.views.register.update :as view]
            [yahont.modules.versions.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn update [id params]
  (try
    (register/update-by-id id params)
    (redirect (string/replace url/versions-register-show #":id" id))
    (catch Exception e (view/error))))
