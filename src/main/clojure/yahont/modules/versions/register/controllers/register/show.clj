(ns yahont.modules.versions.register.controllers.register.show
  (:require [yahont.modules.versions.register.views.register.show :as view]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.modules.versions.register.persistence.register :as register]))

(defn index [id]
  (try
    (view/index (provision/key-entry (register/select-by-id id)))
    (catch Exception e (view/error))))
