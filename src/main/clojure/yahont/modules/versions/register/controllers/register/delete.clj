(ns yahont.modules.versions.register.controllers.register.delete
  (:require [yahont.url :as url]
            [yahont.modules.versions.register.views.register.delete :as view]
            [yahont.modules.versions.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn delete [id]
  (try
    (register/delete-by-id id)
    (redirect url/versions-register)
    (catch Exception e (view/error))))
