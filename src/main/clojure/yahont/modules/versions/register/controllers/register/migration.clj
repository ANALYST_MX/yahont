(ns yahont.modules.versions.register.controllers.register.migration
  (:require [yahont.modules.versions.register.views.register.migration :as view]
            [yahont.modules.versions.register.models.register :as register]))

(defn migration-table []
  (try
    (register/create-table)
    (view/ok)
    (catch Exception e (view/error))))
