(ns yahont.modules.versions.register.controllers.register.all
  (:require [yahont.url :as url]
            [yahont.modules.versions.register.views.register.all :as view]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.modules.versions.register.persistence.register :as register]))

(defn index []
  (try
    (view/index (provision/key-entries (register/select-all)))
    (catch Exception e (view/error))))
