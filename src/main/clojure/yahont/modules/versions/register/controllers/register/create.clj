(ns yahont.modules.versions.register.controllers.register.create
  (:require [clojure.string :as string]
            [yahont.url :as url]
            [yahont.modules.versions.register.views.register.create :as view]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.modules.versions.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn index []
  (view/index))

(defn create [params]
  (try
    (let [id (provision/pk (provision/key-entry (register/create params)))]
      (redirect (string/replace url/versions-register-show #":id" (str id))))
    (catch Exception e (view/error))))
