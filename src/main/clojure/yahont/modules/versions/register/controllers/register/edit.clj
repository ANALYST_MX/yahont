(ns yahont.modules.versions.register.controllers.register.edit
  (:require [yahont.url :as url]
            [yahont.modules.versions.register.views.register.edit :as view]
            [yahont.modules.versions.register.provision.register :as provision]
            [yahont.modules.versions.register.persistence.register :as register]
            [ring.util.response :refer [redirect]]))

(defn index [id]
  (try
    (view/index (provision/key-entry (register/select-by-id id)))
    (catch Exception e (view/error))))
