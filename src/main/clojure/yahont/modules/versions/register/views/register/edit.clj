(ns yahont.modules.versions.register.views.register.edit
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.versions.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field submit-button]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.versions.register.provision.register :as provision]))

(def default-formatter (formatter "yyyy-MM-dd HH:mm:ss"))
  
(defn index [register]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/versions-register (t :label/home))]
                   ]]
                 [:div.edit
                  (form-to [:post (string/replace url/versions-register-update #":id" (str (:id register)))]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name) (provision/name register))
                           [:br]
                           (label "date_start" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date) (unparse default-formatter (provision/start-date register)))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date) (unparse default-formatter (provision/end-date register)))
                           [:br]
                           (label "description" (t :label/description))
                           [:br]
                           (text-field (name provision/description) (provision/description register))
                           [:br]
                           (submit-button (t :label/edit)))]]))

(defn error [] (server-error))
