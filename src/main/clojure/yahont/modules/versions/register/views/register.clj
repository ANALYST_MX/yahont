(ns yahont.modules.versions.register.views.register
  (:require [yahont.url :as url]
            [yahont.modules.application.views.layout :refer [common]]
            [yahont.modules.versions.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]))

(defn index []
  (common :title (t :title/home)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/versions-home (t :label/home))]
                   [:li (link-to url/versions-registers (t :label/all))]
                   [:li (link-to url/versions-register-create (t :label/create))]]]]))
