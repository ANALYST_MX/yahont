(ns yahont.modules.versions.register.views.register.show
  (:require [yahont.url :as url]
            [yahont.modules.versions.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [label]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.versions.register.provision.register :as provision]))

(defn index [register]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/versions-register (t :label/register))]
                   [:li (link-to url/versions-registers (t :label/registers))]
                   ]]
                 [:div.show
                  (label "name" (t :label/name))
                  [:br]
                  (provision/name register)
                  [:br]
                  (label "description" (t :label/description))
                  [:br]
                  (provision/description register)
                  [:br]]]))

(defn error [] (server-error))
