(ns yahont.modules.versions.register.views.register.create
  (:require [yahont.url :as url]
            [yahont.modules.versions.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [hiccup.form :refer [form-to label text-field text-area submit-button]]
            [yahont.modules.application.views.layout :refer [common success server-error]]
            [yahont.modules.versions.register.provision.register :as provision]))

(defn index []
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/versions-register (t :label/home))]
                    ]]
                 [:div.create
                  (form-to [:post url/versions-register-create]
                           (label "name" (t :label/name))
                           [:br]
                           (text-field (name provision/name))
                           [:br]
                           (label "start_date" (t :label/start-date))
                           [:br]
                           (text-field (name provision/start-date))
                           [:br]
                           (label "date_end" (t :label/end-date))
                           [:br]
                           (text-field (name provision/end-date))
                           [:br]
                           (label "description" (t :label/description))
                           [:br]
                           (text-area (name provision/description))
                           [:br]
                           (submit-button (t :label/create)))]]))

(defn ok [] (success))
(defn error [] (server-error))
