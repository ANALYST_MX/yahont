(ns yahont.modules.versions.register.views.register.all
  (:require [clojure.string :as string]
            [yahont.support.time.format :refer [formatter unparse]]
            [yahont.url :as url]
            [yahont.modules.versions.register.i18n :refer [t]]
            [hiccup.element :refer [link-to]]
            [yahont.modules.application.views.layout :refer [common server-error]]
            [yahont.modules.versions.register.persistence.register]
            [yahont.modules.versions.register.provision.register :as provision]))

(def default-formatter (formatter "yyyy-MM-dd"))

(defn map-tag [tag xs]
  (map (fn [x] [tag x]) xs))

(defn register-summary [register]
  [:tr
   [:td (provision/pk register)]
   [:td (provision/name register)]
   [:td (unparse default-formatter (provision/start-date register))]
   [:td (unparse default-formatter (provision/end-date register))]
   [:td [:a {:href (string/replace url/versions-register-edit #":id" (str (provision/pk register)))} (t :label/edit)]]
   [:td [:a {:href (string/replace url/versions-register-delete #":id" (str (provision/pk register)))} (t :label/delete)]]])

(defn list-registers [registers]
  [:table
   [:thead
    [:tr
     (map-tag :th ["id" "name" "start_date" "end_date"])
     ]]
   (into [:tbody]
         (map #(register-summary (provision/key-entry %)) registers))])

(defn index [registers]
  (common :title (t :title/register)
          :body [:div {:id "section"}
                 [:div.menu
                  [:ul
                   [:li (link-to url/versions-register (t :label/home))]
                   ]]
                 [:div.all
                  (list-registers registers)]]))

(defn error [] (server-error))
