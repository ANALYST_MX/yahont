(ns yahont.modules.versions.controllers.home
  (:require [yahont.modules.versions.views.home :as view]))

(defn index []
  (view/index))
