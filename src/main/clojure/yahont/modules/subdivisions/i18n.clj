(ns yahont.modules.subdivisions.i18n
  (:use [yahont.config])
  (:require [taoensso.tower :as tower]))

(def my-tconfig
  {:dictionary
   {:ru
    {:title
     {:home "Главная"
      }
     :label
     {:home "Главная"
      :subdivisions "Подразделения"
      }
     }
    }
   :dev-mode? true
   :fallback-locale :ru
   })

(def t (partial (tower/make-t my-tconfig) locale))
