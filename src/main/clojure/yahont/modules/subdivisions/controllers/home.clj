(ns yahont.modules.subdivisions.controllers.home
  (:require [yahont.modules.subdivisions.views.home :as view]))

(defn index []
  (view/index))
